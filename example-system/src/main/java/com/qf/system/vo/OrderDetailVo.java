package com.qf.system.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class OrderDetailVo {
    /**
     * 订单号
     */
    @TableField(value = "order_number")
    private String orderNumber;
    /**
     * 订单总额
     */
    @TableField(value = "order_meney")
    private Double orderMeney;
    /**
     * 订单创建时间
     */
    @TableField(value = "creation_time")
    private Date creationTime;
    /**
     * 患者姓名
     */
    @TableField(value = "patient_name")
    private String patientName;
    /**
     * 出生日期
     */
    @TableField(value = "birthday")
    private Date birthday;
    /**
     * 性别
     */
    @TableField(value = "gender")
    private Integer gender;
    /**
     * 手机号码
     */
    @TableField(value = "phone")
    private String phone;
    /**
     * 会员等级
     */
    @TableField(value = "vip_class")
    private String vipClass;
    /**
     * 储值余额
     */
    @TableField(value = "balance")
    private Double balance;
    /**
     * 会员积分
     */
    @TableField(value = "vip_points")
    private Double vipPoints;
    /**
     * 科室名字
     */
    @TableField(value = "department_name")
    private String departmentName;
    /**
     * 医生名字
     */
    @TableField(value = "username")
    private String username;
    /**
     * 接诊类型
     */
    @TableField(value = "clinical_type")
    private Integer clinicalType;
    /**
     * 挂号时间
     */
    @TableField(value = "registration_time")
    private Date registrationTime;

    private List<OrderChemicalVo> list;
    /**
     * 附加费用名称
     */
    @TableField(value = "cost_name")
    private String cost_name;
    /**
     * 附加费用金额
     */
    @TableField(value = "money")
    private String money;
}
