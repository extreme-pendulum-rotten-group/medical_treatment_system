package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

/**
 * 患者学历表
 */
@Data
@TableName(value = "hhf_patient_education_background")
public class PatientEducationBackground {
    /**
     * 主键
     */
    @TableId(value = "patient_education_background_id", type = IdType.INPUT)
    private Integer patientEducationBackgroundId;

    /**
     * 患者学历名称
     */
    @TableField(value = "patient_education_background_name")
    private String patientEducationBackgroundName;

    /**
     * 创建人
     */
    @TableField(value = "creator")
    private String creator;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 是否删除：1已删除，0未删除
     */
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    public static final String COL_PATIENT_EDUCATION_BACKGROUND_ID = "patient_education_background_id";

    public static final String COL_PATIENT_EDUCATION_BACKGROUND_NAME = "patient_education_background_name";

    public static final String COL_CREATOR = "creator";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_IS_DELETED = "is_deleted";
}