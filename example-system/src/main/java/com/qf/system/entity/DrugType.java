package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "lhr_drug_type")
public class DrugType {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 处方分类
     */
    @TableField(value = "prescription_type")
    private Integer prescriptionType;

    /**
     * 药品分类名称
     */
    @TableField(value = "drug_type_name")
    private String drugTypeName;

    /**
     * 创建时间
     */
    @TableField(value = "creation_time")
    private Date creationTime;

    /**
     * 创建人ID
     */
    @TableField(value = "`member`")
    private Long member;

    /**
     * 状态
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 删除
     */
    @TableField(value = "is_del")
    private Integer isDel;

    public static final String COL_ID = "id";

    public static final String COL_PRESCRIPTION_TYPE = "prescription_type";

    public static final String COL_DRUG_TYPE_NAME = "drug_type_name";

    public static final String COL_CREATION_TIME = "creation_time";

    public static final String COL_MEMBER = "member";

    public static final String COL_STATUS = "status";

    public static final String COL_IS_DEL = "is_del";
}