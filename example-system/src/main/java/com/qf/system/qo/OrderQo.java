package com.qf.system.qo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class OrderQo {
    /**
     * 患者id
     */
    private Integer patientId;
    /**
     * 订单号
     */
    private String orderNumber;
    /**
     * 零售（处方）（挂号）单号
     */
    private String retailNumber;
    /**
     * 科室id
     */
    private Integer departmentId;
    /**
     * 医生id
     */
    private Integer memberId;
    /**
     * 挂号id
     */
    private Integer registrationId;
    /**
     * 订单状态 1-未付款 2-已付款 0-已退款
     */
    private Integer orderStatue;
    /**
     * 订单类型 1-挂号 2-零售 3-处方
     */
    private Integer orderType;
}
