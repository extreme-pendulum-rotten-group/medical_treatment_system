package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Department;
import com.qf.system.reqeust.DeptParam;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DepartmentMapper extends BaseMapper<Department> {
    List<Department> viewDept(DeptParam param);

    Integer deleteList(List<Integer> list);

    Integer changeDept(DeptParam param);

    Integer addDept(DeptParam param);

}