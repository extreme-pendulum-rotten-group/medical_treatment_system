package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.PointsChangeRecord;
import com.qf.system.reqeust.VipRequestParam;

import java.util.List;

public interface PointsChangeRecordMapper extends BaseMapper<PointsChangeRecord> {
    default List<PointsChangeRecord> selectPointsChangeRecord(VipRequestParam vipRequestParam) {
        QueryWrapper<PointsChangeRecord> qw = new QueryWrapper<>();
        qw.lambda().eq(PointsChangeRecord::getPatientId, vipRequestParam.getPatientId());
        return this.selectList(qw);
    }
}