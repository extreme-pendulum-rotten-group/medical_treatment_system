package com.qf.system.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.PointsChangeRecord;
import com.qf.system.mapper.PointsChangeRecordMapper;
import com.qf.system.reqeust.VipRequestParam;
import com.qf.system.service.PointsChangeRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: hhf
 */
@Service
public class PointsChangeRecordServiceImpl implements PointsChangeRecordService {
    private static final String DEPOSIT_POINTS = "储值积分";
    private static final String ADD_POINTS = "积分增加";
    private static final String REDUCE_POINTS = "积分扣减";
    private static final String RESET_POINTS = "积分清零";

    @Resource
    PointsChangeRecordMapper pointsChangeRecordMapper;

    @Override
    public List<PointsChangeRecord> selectPointsChangeRecord(VipRequestParam vipRequestParam) {
        return pointsChangeRecordMapper.selectPointsChangeRecord(vipRequestParam);
    }

    @Override
    public int addDepositPointRecord(VipRequestParam vipRequestParam) {
        PointsChangeRecord pointsChangeRecord = new PointsChangeRecord();
        pointsChangeRecord.setChangeType(DEPOSIT_POINTS);
        pointsChangeRecord.setAmount(vipRequestParam.getChangePoints());
        pointsChangeRecord.setPatientId(vipRequestParam.getPatientId());
        int count = pointsChangeRecordMapper.insert(pointsChangeRecord);
        if (count < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return count;
    }

    @Override
    public int addAddPointRecord(VipRequestParam vipRequestParam) {
        PointsChangeRecord pointsChangeRecord = new PointsChangeRecord();
        pointsChangeRecord.setChangeType(ADD_POINTS);
        pointsChangeRecord.setAmount(vipRequestParam.getChangePoints());
        pointsChangeRecord.setPatientId(vipRequestParam.getPatientId());
        int count = pointsChangeRecordMapper.insert(pointsChangeRecord);
        if (count < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return count;
    }

    @Override
    public int addReducePointRecord(VipRequestParam vipRequestParam) {
        PointsChangeRecord pointsChangeRecord = new PointsChangeRecord();
        pointsChangeRecord.setChangeType(REDUCE_POINTS);
        pointsChangeRecord.setAmount(vipRequestParam.getChangePoints());
        pointsChangeRecord.setPatientId(vipRequestParam.getPatientId());
        int count = pointsChangeRecordMapper.insert(pointsChangeRecord);
        if (count < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return count;
    }

    @Override
    public int resetReducePointRecord(VipRequestParam vipRequestParam) {
        PointsChangeRecord pointsChangeRecord = new PointsChangeRecord();
        pointsChangeRecord.setChangeType(RESET_POINTS);
        pointsChangeRecord.setAmount(vipRequestParam.getChangePoints());
        pointsChangeRecord.setPatientId(vipRequestParam.getPatientId());
        int count = pointsChangeRecordMapper.insert(pointsChangeRecord);
        if (count < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return count;
    }
}
