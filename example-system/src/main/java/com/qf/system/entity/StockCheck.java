package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * @Project      : medical_treatment_system
 * @Package      : com.qf.system.entity
 * @ClassName    : StockCheck.java
 * @createTime   : 2022/3/31 14:59
 * @version      : 1.0
 * @author       : 菜可夫斯基
 * @Email        : 3293477562@qq.com
 * @公众号        : 菜可夫斯基
 * @CSDN         : weixin_44106059
 * @Description  : 
 */

@Data
@TableName(value = "nf_stock")
public class StockCheck {
    /**
     * 库存ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 盘点单号
     */
    @TableField(value = "`number`")
    private String number;

    /**
     * 当前库存数
     */
    @TableField(value = "stock_current")
    private Double stockCurrent;

    /**
     * 盘点库存数
     */
    @TableField(value = "stock_check")
    private Double stockCheck;

    /**
     * 盘盈盘亏
     */
    @TableField(value = "profit_and_loss")
    private Integer profitAndLoss;

    /**
     * 盘点人
     */
    @TableField(value = "check_people")
    private String checkPeople;

    /**
     * 创建时间
     */
    @TableField(value = "created_date")
    private Date createdDate;

    /**
     * 备注
     */
    @TableField(value = "remarks")
    private String remarks;

    /**
     * 盘点药品ID
     */
    @TableField(value = "chemical_id")
    private Integer chemicalId;

    /**
     * 是否完成盘点
     */
    @TableField(value = "`enable`")
    private Byte enable;

    /**
     * 调价次数
     */
    @TableField(value = "`operator_count`")
    private Integer operatorCount;

    private Chemical chemical;

    public static final String COL_ID = "id";

    public static final String COL_OPERATOR_COUNT = "operator_count";

    public static final String COL_NUMBER = "number";

    public static final String COL_STOCK_CURRENT = "stock_current";

    public static final String COL_STOCK_CHECK = "stock_check";

    public static final String COL_PROFIT_AND_LOSS = "profit_and_loss";

    public static final String COL_CHECK_PEOPLE = "check_people";

    public static final String COL_CREATED_DATE = "created_date";

    public static final String COL_REMARKS = "remarks";

    public static final String COL_CHEMICAL_ID = "chemical_id";

    public static final String COL_ENABLE = "enable";
}