package com.qf.system.service.impl;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.mapper.CostMapper;
import com.qf.system.reqeust.CostParam;
import com.qf.system.service.CostService;
import com.qf.system.vo.CostVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class CostServiceImpl implements CostService {

    @Resource
    CostMapper costMapper;

    /**
     * 查询
     * @param param
     * @return
     */
    @Override
    public ResponseResult selectCost(CostParam param) {
        List<CostVo> costVos = costMapper.selectCost(param);
        if (costVos.size() != 0){
            return ResponseResult.success(ResultCode.SUCCESS,costVos);
        }else {
            return ResponseResult.error(ResultCode.ERROR);
        }
    }

    /**
     * 批量删除
     * @param list
     * @return
     */
    @Override
    public ResponseResult deleteList(List<Integer> list) {
        Integer number = costMapper.deleteList(list);
        if (number == null){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 编辑修改
     * @param param
     * @return
     */
    @Override
    public ResponseResult updateById(CostParam param) {
        Integer number = costMapper.updateById(param);
        if (number == null){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 新增费用
     * @param param
     * @return
     */
    @Override
    public ResponseResult addCost(CostParam param) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //获取当前时间String类型
        String time = sdf.format(date);
        param.setTime(time);
        Integer number = costMapper.addCost(param);
        if (number == null){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }
}
