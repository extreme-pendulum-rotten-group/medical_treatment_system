package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.entity.Physique;
import org.springframework.stereotype.Service;

@Service
public interface PhysiqueService {
    ResponseResult addPhysique(Physique physique);

    ResponseResult updatePhysique(Physique physique);
}
