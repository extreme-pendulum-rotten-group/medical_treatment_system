package com.qf.system.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.VipType;
import com.qf.system.mapper.VipTypeMapper;
import com.qf.system.service.VipTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: Rock
 */
@Service
public class VipTypeServiceImpl implements VipTypeService {
    @Resource
    VipTypeMapper vipTypeMapper;

    @Override
    public Integer addVipType(VipType vipType) {
        int count = vipTypeMapper.insert(vipType);
        if (count < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return count;
    }

    @Override
    public Integer updateVipType(VipType vipType) {
        int count = vipTypeMapper.updateById(vipType);
        if (count < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return count;
    }

    @Override
    public List<VipType> selectVipType() {
        return vipTypeMapper.selectList(null);
    }

    @Override
    public String selectVipTypeNameById(Integer vipTypeId) {
        String vipTypeName = vipTypeMapper.selectVipTypeNameById(vipTypeId);
        if (vipTypeName == null) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return vipTypeName;
    }
}
