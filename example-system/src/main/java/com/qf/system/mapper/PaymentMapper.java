package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Payment;
import com.qf.system.reqeust.PaymentParam;
import com.qf.system.vo.PaymentVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PaymentMapper extends BaseMapper<Payment> {
    /**
     * 展示
     * @param param
     * @return
     */
    List<PaymentVo> selectPayment(PaymentParam param);

    /**
     * 更改状态
     * @param param
     * @return
     */
    Integer changeById(PaymentParam param);

    /**
     * 新增付费方式
     * @param param
     * @return
     */
    Integer addPayment(PaymentParam param);
}