package com.qf.system.service;

import com.qf.system.qo.DrugRetailChemicalQo;
import com.qf.system.qo.DrugRetailCostQo;
import com.qf.system.qo.DrugRetailQo;
import com.qf.system.vo.DrugRetailChemicalVo;
import com.qf.system.vo.DrugRetailPatientVo;
import com.qf.system.vo.DrugRetailSurchargeVo;
import com.qf.system.vo.NumberVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DrugRetailService {

    NumberVo insert(DrugRetailQo drugRetailQo);

    List<String> findChemicalName();

    DrugRetailPatientVo findPatient(String str);

    List<DrugRetailChemicalVo> findChemical(DrugRetailChemicalQo drugRetailChemicalQo);

    List<DrugRetailSurchargeVo> findSurcharge();

    Integer addOrder(DrugRetailCostQo drugRetailCostQo);
}
