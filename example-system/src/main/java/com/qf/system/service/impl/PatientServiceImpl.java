package com.qf.system.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.mapper.PatientMapper;
import com.qf.system.qo.PatientQo;
import com.qf.system.reqeust.PatientOnReceptionQo;
import com.qf.system.reqeust.PatientOnTreatmentQo;
import com.qf.system.service.PatientService;
import com.qf.system.vo.PatientOnReceptionVo;
import com.qf.system.vo.PatientOnTreatmentVo;
import org.springframework.beans.BeanUtils;
import com.qf.system.utils.MyUtils;
import com.qf.system.vo.PatientVo;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ZJW
 * @version 1.0.0
 * @Description TODO
 * @ClassName PatientServiceImpl.java
 * @createTime 2022年03月29日 11:28:00
 */
@Service
public class PatientServiceImpl implements PatientService {
    @Resource
    PatientMapper patientMapper;

    @Override
    public Integer addPatient(PatientQo patientQo) {
        if (patientQo == null) {
            throw new ServiceException(ResultCode.PARAMS_NOT_IS_BLANK);
        }
        Integer insert = patientMapper.insert(patientQo);
        if (insert == 0 || insert == null) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return insert;
    }

    @Override
    public ResponseResult selectPatientOnReception() {
        List<PatientOnReceptionVo> list= patientMapper.selectPatientOnReception();
        if (!ObjectUtils.isEmpty(list)){
            return ResponseResult.success(list);
        }
        return ResponseResult.error(ResultCode.ERROR);
    }

    @Override
    public List<PatientVo> findPatient() {
        List<PatientVo> list = patientMapper.select();
        if (list.size() == 0) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return list;
    }

    @Override
    public ResponseResult selectPatientOnReceptionBySomething(PatientOnReceptionQo patientOnReceptionQo) {
        List<PatientOnReceptionVo> list= patientMapper.selectPatientOnReceptionBySomething(patientOnReceptionQo);
        if (!ObjectUtils.isEmpty(list)){
            return ResponseResult.success(list);
        }
        return ResponseResult.error(ResultCode.ERROR);
    }

    @Override
    public ResponseResult selectPatientOnTreatmentById(PatientOnTreatmentQo patientOnTreatmentQo) {
        PatientOnTreatmentVo patient = patientMapper.selectPatientOnTreatmentById(patientOnTreatmentQo);
        if (ObjectUtils.isEmpty(patient)){
            return ResponseResult.error();
        }
        return ResponseResult.success(patient);
    }

}
