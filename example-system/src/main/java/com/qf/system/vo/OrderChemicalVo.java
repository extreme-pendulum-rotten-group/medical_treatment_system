package com.qf.system.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.math.BigDecimal;
@Data
public class OrderChemicalVo {
    /**
     * 通用名
     */
    @TableField(value = "name")
    private String name;
    /**
     * 零售价
     */
    @TableField(value = "retail_price")
    private BigDecimal retailPrice;
    /**
     * 包装单位
     */
    @TableField(value = "packing_unit")
    private String packingUnit;
}
