package com.qf.system.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.system.mapper.OrderMapper;
import com.qf.system.qo.DrugRetailCostQo;
import com.qf.system.qo.OrderHomeQo;
import com.qf.system.qo.OrderQo;
import com.qf.system.qo.OrderSurchargeQo;
import com.qf.system.service.OrderService;
import com.qf.system.vo.DrugRetailPatientVo;
import com.qf.system.vo.OrderDetailVo;
import com.qf.system.vo.OrderVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Resource
    OrderMapper orderMapper;

    @Override
    public Integer addOrder(DrugRetailCostQo drugRetailCostQo) {
        if (drugRetailCostQo == null) {
            throw new ServiceException(ResultCode.PARAMS_IS_INVALID);
        }
        Integer order = orderMapper.insertOrder(drugRetailCostQo);
        if (order <= 0) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return order;
    }

    /**
     * 查询订单首页
     * @param orderHomeQo
     * @return
     */
    @Override
    public List<OrderVo> findOrderHome(OrderHomeQo orderHomeQo) {
        if (orderHomeQo == null) {
            throw new ServiceException(ResultCode.PARAMS_IS_INVALID);
        }
        List<OrderVo> vos = orderMapper.selectOrder(orderHomeQo);
        if (vos.size() <= 0) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return vos;
    }

    /**
     * 修改付款状态
     * @param orderNumber
     * @param onset
     * @param end
     * @return
     */
    @Override
    public Integer update(String orderNumber, Integer onset, Integer end) {
        if ("".equals(orderNumber)  || onset == 0 || end == 0) {
            throw new ServiceException(ResultCode.PARAMS_IS_INVALID);
        }
        Integer update = orderMapper.update(orderNumber, onset, end);
        if (update <= 0) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return update;
    }

    /**
     * 删除订单（假）
     * @param orderNumber
     * @return
     */
    @Override
    public Integer deleteOrder(String orderNumber) {
        if ("".equals(orderNumber)) {
            throw new ServiceException(ResultCode.PARAMS_IS_INVALID);
        }
        Integer order = orderMapper.deleteOrder(orderNumber);
        if (order <= 0) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return order;
    }

    /**
     * 订单详情
     * @param orderNumber
     * @return
     */
    @Override
    public OrderDetailVo selectOrderDetail(String orderNumber) {
        if ("".equals(orderNumber)) {
            throw new ServiceException(ResultCode.PARAMS_IS_INVALID);
        }
        OrderDetailVo vo = orderMapper.selectOrderDetail(orderNumber);
        vo.setList(orderMapper.selectOrderChemicalVo(orderNumber));
        if (vo == null) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return vo;
    }
}
