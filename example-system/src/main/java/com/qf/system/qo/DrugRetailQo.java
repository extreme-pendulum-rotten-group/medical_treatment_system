package com.qf.system.qo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.List;

@Data
public class DrugRetailQo {
    /**
     * 患者信息
     */
    private Integer patientId;
    /**
     * 零售单号
     */
    private String retailNumber;
    /**
     * 药物id
     */
    private List<Integer> chemicalId;
    /**
     * 药品单价
     */
    private List<Double> chemicalMoney;
    /**
     * 附加费用表id
     */
    private Integer surchargeId;
    /**
     * 操作人
     */
    private Integer member;

}
