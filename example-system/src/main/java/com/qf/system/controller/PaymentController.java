package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.PaymentParam;
import com.qf.system.service.PaymentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Resource
    PaymentService paymentService;

    /**
     * 查找和展示
     * @param param
     * @return
     */
    @GetMapping("/view/payment")
    public ResponseResult selectPayment(PaymentParam param){
        return paymentService.selectPayment(param);
    }

    /**
     * 更改状态
     * @param param
     * @return
     */
    @PostMapping("/update/id")
    public ResponseResult changeById(PaymentParam param){
        return paymentService.changeById(param);
    }

    /**
     * 新增付费方式
     * @param param
     * @return
     */
    @PostMapping("/add/payment")
    public ResponseResult addPayment(PaymentParam param){
        return paymentService.addPayment(param);
    }
}
