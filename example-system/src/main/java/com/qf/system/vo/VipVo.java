package com.qf.system.vo;

import com.qf.system.entity.*;
import lombok.Data;

/**
 * @author: hhf
 */
@Data
public class VipVo extends Vip {
    private PatientEducationBackground patientEducationBackground;
    private PatientOccupation patientOccupation;
    private PatientSource patientSource;
    private VipType vipType;
}
