package com.qf.system.service;

import com.qf.system.reqeust.PrescriptionSurchargeQo;
import org.springframework.stereotype.Service;

@Service
public interface PrescriptionSurchargeService {
    Integer insertPrescriptionAndSurcharge(PrescriptionSurchargeQo psq);
}
