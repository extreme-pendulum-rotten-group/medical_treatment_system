package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.InStock;
import com.qf.system.entity.OutStock;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * @Project      : medical_treatment_system
 * @Package      : com.qf.system.mapper
 * @ClassName    : InStockMapper.java
 * @createTime   : 2022/3/29 10:57
 * @version      : 1.0
 * @author       : 菜可夫斯基
 * @Email        : 3293477562@qq.com
 * @公众号        : 菜可夫斯基
 * @CSDN         : weixin_44106059
 * @Description  : 
 */
@Mapper
public interface InStockMapper extends BaseMapper<OutStock> {
   Integer addStock(@Param("inStock") InStock inStock);

   Integer updateStock(@Param("inStock") InStock inStock);

   Integer reviewEnable(@Param("inEnable") Integer inEnable, @Param("inNumber") String inNumber);

    List<InStock> inPaging(@Param("page") Integer page, @Param("size") Integer size, @Param("enable") Boolean enable, @Param("type") String type, @Param("number") String number, @Param("supplier") String supplier);

    Integer deleteInById(@Param("id") Integer id);
}