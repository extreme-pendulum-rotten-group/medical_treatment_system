package com.qf.system.reqeust;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class ChemicalOnReceptionQo {

    private Integer id;
    /**
     * 通用名
     */
    private String name;
    /**
     * 药品类型
     */
    private String type;
    /**
     * 药品规格
     */
    private String specifications;
    /**
     * 零售价
     */
    private BigDecimal retailPrice;

    /**
     * 当前库存数
     */
    private Double stockCurrent;






}