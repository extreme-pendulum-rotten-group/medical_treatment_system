package com.qf.system.controller;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.qo.DrugRetailCostQo;
import com.qf.system.qo.OrderHomeQo;
import com.qf.system.service.OrderService;
import com.qf.system.vo.OrderDetailVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController("/order")
public class OrderController {
    @Resource
    OrderService orderService;

    @PostMapping("/add/cost")
    public ResponseResult addOrder(DrugRetailCostQo drugRetailCostQo) {
        return ResponseResult.success(orderService.addOrder(drugRetailCostQo));
    }


    /**
     * 查询订单首页
     * @param orderHomeQo
     * @return
     */
    @PostMapping("/home")
    public ResponseResult findOrderHome(OrderHomeQo orderHomeQo) {
        return ResponseResult.success(orderService.findOrderHome(orderHomeQo));
    }

    /**
     * 修改付款状态
     * @param orderNumber
     * @param onset
     * @param end
     * @return
     */
    @PostMapping("/update")
    public ResponseResult update(String orderNumber, Integer onset, Integer end) {
        return ResponseResult.success(orderService.update(orderNumber, onset, end));
    }

    /**
     * 删除订单
     * @param orderNumber
     * @return
     */
    @PostMapping("/delete")
    public ResponseResult deleteOrder(String orderNumber) {
        return ResponseResult.success(orderService.deleteOrder(orderNumber));
    }

    /**
     * 订单详情
     * @param orderNumber
     * @return
     */
    @GetMapping("/detail")
    public ResponseResult selectOrderDetail(String orderNumber) {
        return ResponseResult.success(orderService.selectOrderDetail(orderNumber));
    }
}
