package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Registration;
import com.qf.system.qo.ConditionRegistrationQo;
import com.qf.system.qo.RegistrationQo;
import com.qf.system.vo.RegistrationDetailsVo;
import com.qf.system.vo.RegistrationVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Property;

import java.util.List;

public interface RegistrationMapper extends BaseMapper<Registration> {

    /**
     * 新增挂号
     */
    Integer addRegistration(@Param("qo") RegistrationQo registrationQo);

    /**
     * 查询挂号首页
     */
    List<RegistrationVo> selectRegistration(@Param("status") Integer status);

    /**
     * 条件查询挂号首页
     */
    List<RegistrationVo> conditionSelectRegistration(@Param("qo")ConditionRegistrationQo conditionRegistrationQo);

    /**
     * 同个挂号单号查询挂号详情
     * @param number
     * @return
     */
    RegistrationDetailsVo selectRegistrationById(@Param("number") String number);

    /**
     * 修改状态值
     * @param number
     * @return
     */
    Integer updateRegistrationStatus(@Param("number") String number,@Param("modification")Integer modification);
}