package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.reqeust.MemberParam;

public interface MsgService {
    ResponseResult sendMsg(String phone);
}
