package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.StockCheck;
import com.qf.system.service.impl.StockCheckService;
import com.qf.system.vo.CheckPagingVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.controller
 * @ClassName : StockCheckController.java
 * @createTime : 2022/3/30 10:53
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */

@RestController
public class StockCheckController {
    @Resource
    StockCheckService stockCheckService;

    /**
     * 新增盘点
     * @return
     */
    @PostMapping("/stock/check/add")
    ResponseResult addStockCheck(@RequestBody StockCheck stockCheck) {
        return ResponseResult.success(ResultCode.SUCCESS, stockCheckService.addStockCheck(stockCheck));
    }

    /**
     * 完成盘查
     * @param number
     * @return
     */
    @PutMapping("/stock/check/finish/{number}")
    ResponseResult finishCheck(@PathVariable("number") String number) {
        return ResponseResult.success(ResultCode.SUCCESS, stockCheckService.finishCheck(number));
    }

    /**
     * 盘查分页
     *
     * @return
     */
    @GetMapping("/stock/check/paging/")
    ResponseResult checkPaging(@RequestBody CheckPagingVo pagingVo) {
        return ResponseResult.success(ResultCode.SUCCESS, stockCheckService.checkPaging(pagingVo.getPage(), pagingVo.getSize(), pagingVo.getParam1(), pagingVo.getParam2()));
    }

    /**
     * 盘查删除
     * @param id
     * @return
     */
    @DeleteMapping("/stock/check/delete/{id}")
    ResponseResult deleteCheckById(@PathVariable Integer id) {
        return ResponseResult.success(ResultCode.SUCCESS,stockCheckService.deleteCheckById(id));
    }

}
