package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Position;
import com.qf.system.reqeust.MemberParam;
import com.qf.system.reqeust.PositionParam;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PositionMapper extends BaseMapper<Position> {
    List<Position> selectPositin(PositionParam param);

    Integer deleteLisst(List<Integer> list);

    Integer changeMember(PositionParam param);

    Integer addMember(PositionParam param);

}