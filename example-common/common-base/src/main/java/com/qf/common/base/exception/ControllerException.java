package com.qf.common.base.exception;

import com.qf.common.base.result.ResultCode;

public class ControllerException extends BaseException {
    public ControllerException(ResultCode resultCode) {
        super(resultCode);
    }

    public static void main(String[] args) {
        throw new ControllerException(ResultCode.SYSTEM_ERROR);
    }
}
