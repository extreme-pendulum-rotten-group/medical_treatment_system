package com.qf.system.vo;

import com.qf.system.entity.Permission;
import com.qf.system.entity.Role;
import lombok.Data;

import java.util.List;

@Data
public class RoleVo extends Role {
    private List<Permission> permissions;
}
