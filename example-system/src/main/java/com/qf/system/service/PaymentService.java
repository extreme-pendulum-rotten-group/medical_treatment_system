package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.PaymentParam;

public interface PaymentService {
    ResponseResult selectPayment(PaymentParam param);

    ResponseResult changeById(PaymentParam param);

    ResponseResult addPayment(PaymentParam param);
}
