package com.qf.system.service.impl;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.Position;
import com.qf.system.mapper.PositionMapper;
import com.qf.system.reqeust.MemberParam;
import com.qf.system.reqeust.PositionParam;
import com.qf.system.service.PositionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PositionServiceImpl implements PositionService {

    @Resource
    PositionMapper positionMapper;

    /**
     * 查询与展示
     * @param param
     * @return
     */
    @Override
    public ResponseResult selectPosition(PositionParam param) {
        List<Position> positions = positionMapper.selectPositin(param);
        if (positions.size() == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,positions);
    }

    /**
     * 删除
     * @param list
     * @return
     */
    @Override
    public ResponseResult deleteList(List<Integer> list) {
        Integer number = positionMapper.deleteLisst(list);
        if (number == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 编辑与修改
     * @param param
     * @return
     */
    @Override
    public ResponseResult changePosition(PositionParam param) {
        Integer number = positionMapper.changeMember(param);
        if (number == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 新增
     * @param param
     * @return
     */
    @Override
    public ResponseResult addPosition(PositionParam param) {
        Integer number = positionMapper.addMember(param);
        if (number == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }
}
