package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.entity.PointsChangeRecord;
import com.qf.system.entity.TransactionRecord;
import com.qf.system.entity.Vip;
import com.qf.system.entity.VipType;
import com.qf.system.reqeust.VipRequestParam;
import com.qf.system.service.PointsChangeRecordService;
import com.qf.system.service.TransactionRecordService;
import com.qf.system.service.VipService;
import com.qf.system.service.VipTypeService;
import com.qf.system.vo.VipVo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: hhf
 */
@RestController
@RequestMapping("/vip")
public class VipController {
    @Resource
    VipService vipService;

    @Resource
    TransactionRecordService transactionRecordService;

    @Resource
    VipTypeService vipTypeService;

    @Resource
    PointsChangeRecordService pointsChangeRecordService;

    /**
     * 查找所有会员
     * @return
     */
    @GetMapping("/select/vips")
    public ResponseResult<List<VipVo>> selectVips(){
        List<VipVo> vips = vipService.selectVips();
        return ResponseResult.success(vips);
    }

    /**
     * 通过id查找会员
     */
    @PostMapping("/select/vip/by/id")
    public ResponseResult<VipVo> selectVipById(@RequestBody VipRequestParam vipRequestParam){
        VipVo vip = vipService.selectVipById(vipRequestParam);
        return ResponseResult.success(vip);
    }

    /**
     * 按条件搜索会员
     */
    @PostMapping("/search/vip")
    public ResponseResult<List<VipVo>> searchVip(@RequestBody VipRequestParam vipRequestParam){
        List<VipVo> vips = vipService.searchVip(vipRequestParam);
        return ResponseResult.success(vips);
    }

    /**
     * 修改会员信息
     */
    @PostMapping("/update/vip")
    public ResponseResult<Integer> updateVip(@RequestBody Vip vip){
        Integer integer = vipService.updateVip(vip);
        return ResponseResult.success(integer);
    }

    /**
     * 查找所有会员类型
     */
    @GetMapping("/select/vip/type")
    public ResponseResult<List<VipType>> selectVipType(){
        List<VipType> vipTypes = vipTypeService.selectVipType();
        return ResponseResult.success(vipTypes);
    }

    /**
     * 新增会员类型
     */
    @PostMapping("/add/vip/type")
    public ResponseResult<Integer> addVipType(@RequestBody VipType vipType){
        Integer integer = vipTypeService.addVipType(vipType);
        return ResponseResult.success(integer);
    }

    /**
     * 修改会员类型信息
     */
    @PostMapping("/update/vip/type")
    public ResponseResult<Integer> updateVipType(@RequestBody VipType vipType){
        Integer integer = vipTypeService.updateVipType(vipType);
        return ResponseResult.success(integer);
    }

    /**
     * 充值
     */
    @PostMapping("/add/deposit")
    public ResponseResult<Integer> addDeposit(@Validated(Vip.Deposit.class) @RequestBody VipRequestParam vipRequestParam){
        Integer integer = vipService.addDeposit(vipRequestParam);
        return ResponseResult.success(integer);
    }

    /**
     * 退款
     */
    @PostMapping("/reduce/deposit")
    public ResponseResult<Integer> reduceDeposit(@RequestBody VipRequestParam vipRequestParam){
        Integer integer = vipService.reduceDeposit(vipRequestParam);
        return ResponseResult.success(integer);
    }

    /**
     * 根据会员id查找充值记录
     */
    @GetMapping("/select/transaction/record/by/id")
    public ResponseResult<List<TransactionRecord>> selectTransactionRecord(@RequestBody VipRequestParam vipRequestParam){
        List<TransactionRecord> transactionRecords = transactionRecordService.selectTransactionRecord(vipRequestParam);
        return ResponseResult.success(transactionRecords);
    }

    /**
     * 根据会员id查找积分变动记录
     */
    @GetMapping("/select/points/change/record/by/id")
    public ResponseResult<List<PointsChangeRecord>> selectPointsChangeRecord(@RequestBody VipRequestParam vipRequestParam){
        List<PointsChangeRecord> pointsChangeRecords = pointsChangeRecordService.selectPointsChangeRecord(vipRequestParam);
        return ResponseResult.success(pointsChangeRecords);
    }

    /**
     * 积分增加
     */
    @PostMapping("/add/points")
    public ResponseResult<Integer> addPoints(@RequestBody VipRequestParam vipRequestParam){
        Integer integer = vipService.addPoints(vipRequestParam);
        return ResponseResult.success(integer);
    }

    /**
     * 积分扣减
     */
    @PostMapping("/reduce/points")
    public ResponseResult<Integer> reducePoints(@RequestBody VipRequestParam vipRequestParam){
        Integer integer = vipService.reducePoints(vipRequestParam);
        return ResponseResult.success(integer);
    }

    /**
     * 积分清零
     */
    @PostMapping("/reset/points")
    public ResponseResult<Integer> resetPoints(@RequestBody VipRequestParam vipRequestParam){
        Integer integer = vipService.resetPoints(vipRequestParam);
        return ResponseResult.success(integer);
    }
}
