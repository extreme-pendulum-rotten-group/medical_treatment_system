package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "lhr_manufacturer")
public class Manufacturer {
    /**
     * 生产厂家ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 厂家名称
     */
    @TableField(value = "manufacturer_name")
    private String manufacturerName;

    /**
     * 创建时间
     */
    @TableField(value = "creation_time")
    private Date creationTime;

    /**
     * 创建人ID
     */
    @TableField(value = "`member`")
    private Long member;

    /**
     * 删除
     */
    @TableField(value = "is_del")
    private Integer isDel;

    public static final String COL_ID = "id";

    public static final String COL_MANUFACTURER_NAME = "manufacturer_name";

    public static final String COL_CREATION_TIME = "creation_time";

    public static final String COL_MEMBER = "member";

    public static final String COL_IS_DEL = "is_del";
}