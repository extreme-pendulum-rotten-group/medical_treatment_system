package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.MemberParam;
import com.qf.system.reqeust.PositionParam;

import java.util.List;

public interface PositionService {
    ResponseResult selectPosition(PositionParam param);

    ResponseResult deleteList(List<Integer> list);

    ResponseResult changePosition(PositionParam param);

    ResponseResult addPosition(PositionParam param);
}
