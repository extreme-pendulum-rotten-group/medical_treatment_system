package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Dossier;
import com.qf.system.reqeust.DossierAndPhysiqueQo;
import com.qf.system.reqeust.DossierQo;
import com.qf.system.vo.DossierVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 78458
 */
@Mapper
public interface DossierMapper extends BaseMapper<Dossier> {

    int addDossier(DossierQo dossierQo);

    List<DossierVo> selectDossierByCardNumber(String cardNumber);

    int updateDossierById(Dossier dossier);

    DossierAndPhysiqueQo selectDossierAndPhysique(Long id);
}