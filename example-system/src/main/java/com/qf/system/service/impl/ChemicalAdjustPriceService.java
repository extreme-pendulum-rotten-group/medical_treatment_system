package com.qf.system.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.ChemicalAdjustPrice;
import com.qf.system.mapper.ChemicalAdjustPriceMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.service.impl
 * @ClassName : ChemicalAdjustPriceService.java
 * @createTime : 2022/3/30 18:37
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */

@Service
public class ChemicalAdjustPriceService {
    @Resource
    ChemicalAdjustPriceMapper chemicalAdjustPriceMapper;
    @Resource
    ChemicalService chemicalService;


    public String adjustPrice(BigDecimal currentPrice, String remarks, Integer chemicalId) {
        Integer result = chemicalAdjustPriceMapper.adjustPrice(currentPrice, remarks, chemicalId);
        if (result == 0) {
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return "完成调价";
    }

    public List<ChemicalAdjustPrice> adjustPaging(Integer page, Integer size, Integer type, String param1, String param2, String param3) {
        List<ChemicalAdjustPrice> adjustPrices = chemicalAdjustPriceMapper.adjustPaging((page-1)*size,size,type,param1,param2,param3);
        if (adjustPrices==null){
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return adjustPrices;
    }
}
