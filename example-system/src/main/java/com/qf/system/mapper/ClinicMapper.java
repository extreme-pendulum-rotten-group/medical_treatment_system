package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Clinic;
import com.qf.system.reqeust.ClinicParam;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ClinicMapper extends BaseMapper<Clinic> {
    Integer deleteList(List<Integer> list);

    Integer changeClc(ClinicParam param);

    Integer addClc(ClinicParam param);

    List<Clinic> viewDept(ClinicParam param);
}