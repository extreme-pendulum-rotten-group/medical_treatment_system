package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Supplier;
import com.qf.system.reqeust.SupplierParam;import com.qf.system.vo.SupplierVo;import org.apache.ibatis.annotations.Mapper;import java.util.List;

@Mapper
public interface SupplierMapper extends BaseMapper<Supplier> {
    List<SupplierVo> selectSupplier(SupplierParam param);

    Integer deleteByList(List<Integer> list);

    Integer addSupplier(SupplierParam param);
}