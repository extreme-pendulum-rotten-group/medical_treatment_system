package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.DrugMsgParam;

public interface DrugMsgService {
    ResponseResult viewDrugType(DrugMsgParam param);

    ResponseResult viewDrugDosage(DrugMsgParam param);

    ResponseResult viewDrugUsage(DrugMsgParam param);

    ResponseResult viewPack(DrugMsgParam param);

    ResponseResult viewInvoice(DrugMsgParam param);

    ResponseResult viewManufacturer(DrugMsgParam param);

    ResponseResult viewWarehouse(DrugMsgParam param);
}
