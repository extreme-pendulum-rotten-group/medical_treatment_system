package com.qf.system.vo;

import com.qf.system.entity.Payment;
import lombok.Data;

@Data
public class PaymentVo extends Payment {
}
