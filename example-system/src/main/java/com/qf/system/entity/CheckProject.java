package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "lhr_check_project")
public class CheckProject {
    /**
     * 项目id
     */
    @TableId(value = "project_id", type = IdType.INPUT)
    private Long projectId;

    /**
     * 项目名称
     */
    @TableField(value = "project_name")
    private String projectName;

    /**
     * 成本价
     */
    @TableField(value = "cost_price")
    private Double costPrice;

    /**
     * 零售价
     */
    @TableField(value = "retail_price")
    private Double retailPrice;

    /**
     * 单位 - 1：次；2：支；3：盒 ...
     */
    @TableField(value = "unit_of_measurement")
    private String unitOfMeasurement;

    /**
     * 项目分类 - 1：脑电图；2：针灸；3：材料费 ...
     */
    @TableField(value = "project_type")
    private String projectType;

    /**
     * 发票项目 - 1：诊疗费；2：检查费；3：西药费
     */
    @TableField(value = "invoice")
    private String invoice;

    /**
     * 检查部位
     */
    @TableField(value = "check_point")
    private String checkPoint;

    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;

    /**
     * 创建时间
     */
    @TableField(value = "creation_time")
    private Date creationTime;

    /**
     * 会员折扣 - 1：启用；2：禁用
     */
    @TableField(value = "discount")
    private Integer discount;

    /**
     * 状态 - 1：启用；2：禁用
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 删除
     */
    @TableField(value = "is_del")
    private Integer isDel;

    public static final String COL_PROJECT_ID = "project_id";

    public static final String COL_PROJECT_NAME = "project_name";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_RETAIL_PRICE = "retail_price";

    public static final String COL_UNIT_OF_MEASUREMENT = "unit_of_measurement";

    public static final String COL_PROJECT_TYPE = "project_type";

    public static final String COL_INVOICE = "invoice";

    public static final String COL_CHECK_POINT = "check_point";

    public static final String COL_REMARK = "remark";

    public static final String COL_CREATION_TIME = "creation_time";

    public static final String COL_DISCOUNT = "discount";

    public static final String COL_STATUS = "status";

    public static final String COL_IS_DEL = "is_del";
}