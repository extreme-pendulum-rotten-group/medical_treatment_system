package com.qf.system.service;

import ch.qos.logback.core.util.InvocationGate;
import com.qf.system.qo.ConditionRegistrationQo;
import com.qf.system.qo.RegistrationQo;
import com.qf.system.vo.RegistrationDetailsVo;
import com.qf.system.vo.RegistrationVo;

import java.util.List;

public interface RegistrationService {
    /**
     * 新增挂号
     * @param registrationQo
     * @return
     */
    Integer setRegistration(RegistrationQo registrationQo);

    /**
     * 挂号首页
     * @return
     */
    List<RegistrationVo> findListByRegistration(Integer status);

    /**
     * 挂号首页条件查询
     * @param conditionRegistrationQo
     * @return
     */
    List<RegistrationVo> conditionFindRegistration(ConditionRegistrationQo conditionRegistrationQo);

    /**
     * 挂号详情
     * @param number
     * @return
     */
    RegistrationDetailsVo selectRegistrationById(String number);

    Integer updateByNumber(String number,Integer modification);
}
