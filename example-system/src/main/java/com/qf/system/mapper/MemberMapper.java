package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Member;
import com.qf.system.reqeust.MemberParam;
import com.qf.system.vo.HomePatientVo;
import com.qf.system.vo.MemberVo;import org.apache.ibatis.annotations.Mapper;import java.util.List;

@Mapper
public interface MemberMapper extends BaseMapper<Member> {
    default Member findByUsername(String username) {
        QueryWrapper<Member> qw = new QueryWrapper<>();
        qw.lambda().eq(Member::getUsername, username);
        return this.selectOne(qw);
    }

    /**
     * 通过 username 和 password 来进行登录与验证
     * @param param
     * @return
     */
    List<MemberVo> selectUser(MemberParam param);

    /**
     * 更改密码
     * @param param
     * @return
     */
    Integer changePassword(MemberParam param);

    /**
     * 查询 phone 存不存在
     * @param param
     * @return
     */
    List<MemberVo> selectByPhone(MemberParam param);

    /**
     * 首页展示患者挂号情况
     * @return
     */
    List<HomePatientVo> selectPatient();

    /**
     * 查询和展示医生信息
     * @param param
     * @return
     */
    List<MemberVo> selectMember(MemberParam param);

    /**
     * 删除
     * @param list
     * @return
     */
    Integer deleteLisst(List<Integer> list);

    /**
     * 编辑用户信息
     * @param param
     * @return
     */
    Integer changeMember(MemberParam param);

    /**
     * 新增用户
     * @param param
     * @return
     */
    Integer addMember(MemberParam param);
}