package com.qf.system.service;

import com.qf.system.entity.Vip;
import com.qf.system.entity.VipType;
import com.qf.system.reqeust.VipRequestParam;
import com.qf.system.vo.VipVo;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author hhf
 */
public interface VipService {
    List<VipVo> selectVips();

    VipVo selectVipById(VipRequestParam vipRequestParam);

    List<VipVo> searchVip(VipRequestParam vipRequestParam);

    Integer updateVip(Vip vip);

    Integer addDeposit(VipRequestParam vipRequestParam);

    Integer reduceDeposit(VipRequestParam vipRequestParam);

    Integer addPoints(VipRequestParam vipRequestParam);

    Integer reducePoints(VipRequestParam vipRequestParam);

    Integer resetPoints(VipRequestParam vipRequestParam);
}
