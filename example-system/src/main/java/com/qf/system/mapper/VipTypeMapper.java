package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.VipType;

public interface VipTypeMapper extends BaseMapper<VipType> {
    String selectVipTypeNameById(Integer vipTypeId);
}