package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.DeptParam;
import com.qf.system.service.DepartmentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/department")
public class DepartmentController {

    @Resource
    DepartmentService deptService;

    /**
     * 查找科室
     * @param param
     * @return
     */
    @GetMapping("/view/dept")
    public ResponseResult selectDept(DeptParam param){
        return deptService.selectDept(param);
    }

    /**
     * 删除
     * @param array
     * @return
     */
    @PostMapping("/delete/list")
    public ResponseResult deleteList(@RequestParam(value="array")Integer[] array) {
        List<Integer> list = Arrays.asList(array);
        return deptService.deleteList(list);
    }

    /**
     * 编辑科室
     * @param param
     * @return
     */
    @PostMapping("/change/dept")
    public ResponseResult changeDept(DeptParam param){
        return deptService.changeDept(param);
    }

    /**
     * 新增
     * @param param
     * @return
     */
    @PostMapping("/add/dept")
    public ResponseResult addDept(DeptParam param){
        return deptService.addDept(param);
    }
}
