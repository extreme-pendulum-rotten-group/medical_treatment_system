package com.qf.system.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data

public class CheckProjectOnPrescriptionVo {
    /**
     * 项目id
     */
    private Long projectId;

    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 零售价
     */
    private Double retailPrice;

    /**
     * 单位 - 1：次；2：支；3：盒 ...
     */
    private String unitOfMeasurement;

    /**
     * 项目分类 - 1：脑电图；2：针灸；3：材料费 ...
     */
    private String projectType;

    /**
     * 发票项目 - 1：诊疗费；2：检查费；3：西药费
     */
    private String invoice;

    /**
     * 状态 - 1：启用；2：禁用
     */
    private Integer status;
    /**
     * 删除
     */
    private Integer isDel;


}