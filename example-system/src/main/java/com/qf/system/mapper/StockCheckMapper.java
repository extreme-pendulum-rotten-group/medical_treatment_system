package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.StockCheck;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * @Project      : medical_treatment_system
 * @Package      : com.qf.system.mapper
 * @ClassName    : StockCheckMapper.java
 * @createTime   : 2022/3/30 0:48
 * @version      : 1.0
 * @author       : 菜可夫斯基
 * @Email        : 3293477562@qq.com
 * @公众号        : 菜可夫斯基
 * @CSDN         : weixin_44106059
 * @Description  :
 */
@Mapper
public interface StockCheckMapper extends BaseMapper<StockCheck> {
    Integer addStockCheck(@Param("stock") StockCheck stock);

    Integer finishCheck(String number);

    List<StockCheck> checkPaging(@Param("offset") Integer offset, @Param("size") Integer size, @Param("type") String chemicalType, @Param("name") String chemicalName);

    Integer deleteCheckById(@Param("id") Integer id);
}