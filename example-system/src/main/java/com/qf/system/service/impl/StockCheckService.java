package com.qf.system.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.StockCheck;
import com.qf.system.mapper.StockCheckMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.service.impl
 * @ClassName : StockCheckService.java
 * @createTime : 2022/3/30 10:59
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */
@Service
public class StockCheckService {
    @Resource
    StockCheckMapper stockCheckMapper;

    public String addStockCheck(StockCheck stockCheck) {
        Integer result = stockCheckMapper.addStockCheck(stockCheck);
        if (result != 1) {
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return "新增盘点成功";
    }

    public String finishCheck(String number) {
        Integer result = stockCheckMapper.finishCheck(number);
        if (result == 0) {
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return "完成盘点";
    }

    public List<StockCheck> checkPaging(Integer page, Integer size, String chemicalType, String chemicalName) {

        List<StockCheck> stocks = stockCheckMapper.checkPaging((page - 1) * size, size, chemicalType, chemicalName);
        stocks.forEach(stockCheck -> {
            System.out.println("stockCheck = " + stockCheck);
        });
        if (stocks == null) {
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return stocks;
    }

    public String deleteCheckById(Integer id) {
        Integer result = stockCheckMapper.deleteCheckById(id);
        if (result!=1){
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return "删除成功";
    }
}
