package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.service.CartService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author ZJW
 * @version 1.0.0
 * @Description TODO
 * @ClassName PayController.java
 * @createTime 2022年03月31日 19:36:00
 */
@RestController
@RequestMapping("/pay")
public class PayController {

    @Resource
    CartService cartService;

    @PostMapping("/show")
    public ResponseResult addToCart(Integer orderNumber, Integer chemicalId, Integer num){
        cartService.addToCart(orderNumber,chemicalId,num);
        return null;
    }
}
