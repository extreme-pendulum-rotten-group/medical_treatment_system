package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "zjw_physique")
public class Physique {
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @TableField(value = "temperature")
    private String temperature;

    @TableField(value = "breathe")
    private String breathe;

    @TableField(value = "sphygmus")
    private String sphygmus;

    @TableField(value = "blood_pressure")
    private String bloodPressure;

    @TableField(value = "height")
    private String height;

    @TableField(value = "weight")
    private String weight;

    @TableField(value = "blood_glucose")
    private String bloodGlucose;

    @TableField(value = "`blood fat`")
    private String bloodFat;

    @TableField(value = "card_number")
    private String cardNumber;

    @TableField(value = "dossier_id")
    private String dossierId;

    public static final String COL_ID = "id";

    public static final String COL_TEMPERATURE = "temperature";

    public static final String COL_BREATHE = "breathe";

    public static final String COL_SPHYGMUS = "sphygmus";

    public static final String COL_BLOOD_PRESSURE = "blood_pressure";

    public static final String COL_HEIGHT = "height";

    public static final String COL_WEIGHT = "weight";

    public static final String COL_BLOOD_GLUCOSE = "blood_glucose";

    public static final String COL_BLOOD_FAT = "blood_fat";

    public static final String COL_CARD_NUMBER = "card_number";
    public static final String COL_DOSSIER_ID = "dossier_id";
}