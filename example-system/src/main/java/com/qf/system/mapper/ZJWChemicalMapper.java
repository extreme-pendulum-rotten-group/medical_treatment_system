package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Chemical;
import com.qf.system.reqeust.ChemicalOnReceptionQo;
import com.qf.system.vo.ChemicalOnReceptionVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ZJWChemicalMapper extends BaseMapper<Chemical> {
    List<ChemicalOnReceptionVo> selectAllChemicalAndStock();

    List<ChemicalOnReceptionVo> selectChemicalBySomething(ChemicalOnReceptionQo chemicalQo);

    ChemicalOnReceptionVo selectChemicalById(ChemicalOnReceptionQo chemicalOnReceptionQo);
}