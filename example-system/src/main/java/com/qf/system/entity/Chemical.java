package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.dao
 * @ClassName : Chemical.java
 * @createTime : 2022/3/28 19:59
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */

@Data
@TableName(value = "nf_chemical")
public class Chemical {
    /**
     * 药品ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 条形码
     */
    @TableField(value = "bar_code")
    private String barCode;

    /**
     * 通用名
     */
    @TableField(value = "`name`")
    private String name;

    /**
     * 拼音码
     */
    @TableField(value = "pinyin")
    private String pinyin;

    /**
     * 药品类型
     */
    @TableField(value = "`type`")
    private String type;

    /**
     * 药品规格
     */
    @TableField(value = "specifications")
    private String specifications;

    /**
     * 药品剂型
     */
    @TableField(value = "form")
    private String form;

    /**
     * 是否OTC药品
     */
    @TableField(value = "otc")
    private Boolean otc;

    /**
     * 发票项目
     */
    @TableField(value = "Invoice")
    private String invoice;

    /**
     * 批准文号
     */
    @TableField(value = "approval_number")
    private String approvalNumber;

    /**
     * 生产厂家
     */
    @TableField(value = "producer")
    private String producer;

    /**
     * 药品状态（是否可用）
     */
    @TableField(value = "`enable`")
    private Boolean enable;

    /**
     * 包装单位
     */
    @TableField(value = "packing_unit")
    private String packingUnit;

    /**
     * 基本系数
     */
    @TableField(value = "basic_param")
    private String basicParam;

    /**
     * 基本单位
     */
    @TableField(value = "basic_uint")
    private String basicUint;

    /**
     * 剂量系数
     */
    @TableField(value = "dose_coefficient")
    private String doseCoefficient;

    /**
     * 剂量单位
     */
    @TableField(value = "dose_unit")
    private String doseUnit;

    /**
     * 采购价
     */
    @TableField(value = "purchase_price")
    private BigDecimal purchasePrice;

    /**
     * 用法
     */
    @TableField(value = "`usage`")
    private String usage;

    /**
     * 零售价
     */
    @TableField(value = "retail_price")
    private BigDecimal retailPrice;

    /**
     * 是否允许拆零
     */
    @TableField(value = "dismantle")
    private Boolean dismantle;

    /**
     * 单次用量
     */
    @TableField(value = "single_dosage")
    private String singleDosage;

    /**
     * 频度
     */
    @TableField(value = "frequency")
    private String frequency;

    /**
     * 天数
     */
    @TableField(value = "day_number")
    private Integer dayNumber;

    /**
     * 总量
     */
    @TableField(value = "total")
    private Integer total;

    /**
     * 库存下限
     */
    @TableField(value = "lower_limit_of_inventory")
    private Double lowerLimitOfInventory;

    /**
     * 库存上限
     */
    @TableField(value = "superior_limit_of_inventory")
    private Double superiorLimitOfInventory;

    /**
     * 货位号
     */
    @TableField(value = "location_number")
    private String locationNumber;

    /**
     * 有效期预警
     */
    @TableField(value = "validity")
    private Integer validity;

    /**
     * 药品说明
     */
    @TableField(value = "`explain`")
    private String explain;

    /**
     * 备注
     */
    @TableField(value = "remarks")
    private String remarks;

    /**
     * 药品编码
     */
    @TableField(value = "`number`")
    private String number;

    /**
     * 是否允许会员折扣
     */
    @TableField(value = "discount")
    private Boolean discount;

    /**
     * 当前零售价
     */
    @TableField(value = "current_retail_price")
    private BigDecimal currentRetailPrice;

    /**
     * 采购金额
     */
    @TableField(value = "purchase_amount")
    private BigDecimal purchaseAmount;

    /**
     * 零售金额
     */
    @TableField(value = "sales_amount")
    private BigDecimal salesAmount;

    /**
     * 有效期
     */
    @TableField(value = "term_of_validity")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date termOfValidity;

    @TableField(value = "create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    public static final String COL_TERM_OF_VALIDITY = "term_of_validity";

    public static final String COL_ID = "id";

    public static final String COL_BAR_CODE = "bar_code";

    public static final String COL_NAME = "name";

    public static final String COL_PINYIN = "pinyin";

    public static final String COL_TYPE = "type";

    public static final String COL_SPECIFICATIONS = "specifications";

    public static final String COL_FORM = "form";

    public static final String COL_OTC = "otc";

    public static final String COL_INVOICE = "Invoice";

    public static final String COL_APPROVAL_NUMBER = "approval_number";

    public static final String COL_PRODUCER = "Producer";

    public static final String COL_ENABLE = "enable";

    public static final String COL_PACKING_UNIT = "packing_unit";

    public static final String COL_BASIC_PARAM = "basic_param";

    public static final String COL_BASIC_UINT = "basic_uint";

    public static final String COL_DOSE_COEFFICIENT = "dose_coefficient";

    public static final String COL_DOSE_UNIT = "dose_unit";

    public static final String COL_PURCHASE_PRICE = "purchase_price";

    public static final String COL_USAGE = "usage";

    public static final String COL_RETAIL_PRICE = "retail_price";

    public static final String COL_DISMANTLE = "dismantle";

    public static final String COL_SINGLE_DOSAGE = "single_dosage";

    public static final String COL_FREQUENCY = "frequency";

    public static final String COL_DAY_NUMBER = "day_number";

    public static final String COL_TOTAL = "total";

    public static final String COL_LOWER_LIMIT_OF_INVENTORY = "lower_limit_of_inventory";

    public static final String COL_SUPERIOR_LIMIT_OF_INVENTORY = "superior_limit_of_inventory";

    public static final String COL_LOCATION_NUMBER = "location_number";

    public static final String COL_VALIDITY = "validity";

    public static final String COL_EXPLAIN = "explain";

    public static final String COL_REMARKS = "remarks";

    public static final String COL_NUMBER = "number";

    public static final String COL_DISCOUNT = "discount";

    public static final String COL_CURRENT_RETAIL_PRICE = "current_retail_price";

    public static final String COL_PURCHASE_AMOUNT = "purchase_amount";

    public static final String COL_SALES_AMOUNT = "sales_amount";

    public static final String TERM_OF_VALIDITY = "term_of_validity";
}