package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 78458
 */
@Data
@TableName(value = "dossier")
public class Dossier {
    /**
     * 病历id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 病患主要诉求
     */
    @TableField(value = "appeal")
    private String appeal;

    /**
     * 现病史
     */
    @TableField(value = "present_history")
    private String presentHistory;

    /**
     * 既往史
     */
    @TableField(value = "previous_history")
    private String previousHistory;

    /**
     * 过敏史
     */
    @TableField(value = "allergy")
    private String allergy;

    /**
     * 个人史
     */
    @TableField(value = "person")
    private String person;

    /**
     * 家族史
     */
    @TableField(value = "family")
    private String family;

    /**
     * 辅助检查
     */
    @TableField(value = "accessory_check")
    private String accessoryCheck;

    /**
     * 治疗意见
     */
    @TableField(value = "treatment_options")
    private String treatmentOptions;

    /**
     * 备注
     */
    @TableField(value = "remarks")
    private String remarks;

    /**
     * 备注
     */
    @TableField(value = "card_number")
    private String cardNumber;

    /**
     * 诊断信息
     */
    @TableField(value = "diagnose")
    private String diagnose;

    /**
     * 医嘱建议
     */
    @TableField(value = "advice")
    private String advice;

    public static final String COL_ID = "id";

    public static final String COL_APPEAL = "appeal";

    public static final String COL_PRESENT_HISTORY = "present_history";

    public static final String COL_PREVIOUS_HISTORY = "previous_history";

    public static final String COL_ALLERGY = "allergy";

    public static final String COL_PERSON = "person";

    public static final String COL_FAMILY = "family";

    public static final String COL_ACCESSORY_CHECK = "accessory_check";

    public static final String COL_TREATMENT_OPTIONS = "treatment_options";

    public static final String COL_REMARKS = "remarks";

    public static final String COL_CARD_NUMBER = "cardNumber";

    public static final String COL_DIAGNOSE = "diagnose";

    public static final String COL_ADVICE = "advice";

}