package com.qf.system.service.impl;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.mapper.CheckProjectMapper;
import com.qf.system.reqeust.CheckParam;
import com.qf.system.service.CheckService;
import com.qf.system.vo.CheckVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class CheckServiceImpl implements CheckService {
    @Resource
    CheckProjectMapper checkProjectMapper;
    /**
     * 查询和展示
     * @param param
     * @return
     */
    @Override
    public ResponseResult selectCheck(CheckParam param) {
        List<CheckVo> checkVos = checkProjectMapper.selectCheck(param);
        if (checkVos.size() == 0){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,checkVos);
    }

    /**
     * 删除
     * @param list
     * @return
     */
    @Override
    public ResponseResult deleteByList(List<Integer> list) {
        Integer number = checkProjectMapper.deleteByList(list);
        if (number == null){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 编辑和删除
     * @param param
     * @return
     */
    @Override
    public ResponseResult updateById(CheckParam param) {
        Integer number = checkProjectMapper.changeById(param);
        if (number == null){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 新增
     * @param param
     * @return
     */
    @Override
    public ResponseResult addCheck(CheckParam param) {
        //Date date = new Date();
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ////获取当前时间String类型
        //String time = sdf.format(date);
        //param.setCreationTime(time);
        Integer number = checkProjectMapper.addCheck(param);
        if (number == null){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }
}
