package com.qf.system.config;

import com.qf.system.utils.ShiroUtils;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.crazycake.shiro.RedisCacheManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.annotation.Resource;

@Configuration
public class ShiroConfig {

    @Resource
    RedisSessionDAO redisSessionDAO;

    @Resource
    RedisCacheManager redisCacheManager;

    /**
     * 基于数据数据用户表验证
     */
    @Bean
    //public UserRealm realm(){
    //    return new UserRealm();
    //}
    public UserRealm realm(HashedCredentialsMatcher hashedCredentialsMatcher) {
        UserRealm userRealm = new UserRealm();
        // 注册到realm中
        userRealm.setCredentialsMatcher(hashedCredentialsMatcher);
        return userRealm;
    }

    /**
     * 路径过滤器
     * anon 不需要认证
     * authc 需要认证
     */
    @Bean("filterChainDefinition")
    public ShiroFilterChainDefinition filterChainDefinition(){
        DefaultShiroFilterChainDefinition shiroFilterChainDefinition = new DefaultShiroFilterChainDefinition();
        //// 不需要认证
        //shiroFilterChainDefinition.addPathDefinition("/login","anon");
        //shiroFilterChainDefinition.addPathDefinition("/setMsg","anon");
        //shiroFilterChainDefinition.addPathDefinition("/register","anon");
        //// 其他所有都需要认证
        shiroFilterChainDefinition.addPathDefinition("/**","anon");
        return shiroFilterChainDefinition;
    }

    /**
     * ioc
     * 自动注入
     * @param userRealm
     * @param webSessionManager
     * @return
     */
    @Bean("securityManager")
    @Primary
    public DefaultWebSecurityManager securityManager(UserRealm userRealm, CustomSessionManager webSessionManager){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        /**
         * 设置自定义realm对象
         */
        securityManager.setRealm(userRealm);
        securityManager.setSessionManager(webSessionManager);
        /**
         * 设置权限缓存
         */
        securityManager.setCacheManager(redisCacheManager);
        return securityManager;
    }

    /**
     * 配置密码加密算法  注意一定要跟 注册的时候一致
     * MD5
     * SHA256
     * SHA512
     * @return
     */
    @Bean("hashedCredentialsMatcher")
    @Primary
    public HashedCredentialsMatcher hashedCredentialsMatcher(){
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher(Md5Hash.ALGORITHM_NAME);
        hashedCredentialsMatcher.setHashIterations(ShiroUtils.HASH_ITERATIONS);
        hashedCredentialsMatcher.setStoredCredentialsHexEncoded(true);
        return hashedCredentialsMatcher;
    }

    /**
     * 请求头中
     * @return
     */
    @Bean
    public CustomSessionManager webSessionManager(){
        CustomSessionManager sessionManager = new CustomSessionManager();
        /**
         * 设置 sessinDao
         */
        sessionManager.setSessionDAO(redisSessionDAO);
        /**
         * 设置缓存管理器
         */
        sessionManager.setCacheManager(redisCacheManager);
        return sessionManager;
    }
}
