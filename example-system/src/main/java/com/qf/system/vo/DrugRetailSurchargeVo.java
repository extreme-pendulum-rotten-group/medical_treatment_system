package com.qf.system.vo;

import lombok.Data;

@Data
public class DrugRetailSurchargeVo {
    /**
     * id
     */
    private Integer surchargeId;

    /**
     * 附加费名称
     */
    private String surcharge;

    /**
     * 金额
     */
    private Double momey;
}
