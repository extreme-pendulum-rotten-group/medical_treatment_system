package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

/**
 * 交易记录表
 */
@Data
@TableName(value = "hhf_transaction_record")
public class TransactionRecord {
    /**
     * 主键
     */
    @TableId(value = "transaction_record_id", type = IdType.INPUT)
    private Integer transactionRecordId;

    /**
     * 交易时间
     */
    @TableField(value = "transaction_time")
    private Date transactionTime;

    /**
     * 交易类型
     */
    @TableField(value = "transaction_type")
    private String transactionType;

    /**
     * 交易金额
     */
    @TableField(value = "transaction_money")
    private Double transactionMoney;

    /**
     * 赠送金额
     */
    @TableField(value = "present_money")
    private Double presentMoney;

    /**
     * 合计金额
     */
    @TableField(value = "total_money")
    private Double totalMoney;

    /**
     * 是否删除：1已删除，0未删除
     */
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    /**
     * 会员id
     */
    @TableField(value = "patient_id")
    private Long patientId;

    public static final String COL_TRANSACTION_RECORD_ID = "transaction_record_id";

    public static final String COL_TRANSACTION_TIME = "transaction_time";

    public static final String COL_TRANSACTION_TYPE = "transaction_type";

    public static final String COL_TRANSACTION_MONEY = "transaction_money";

    public static final String COL_PRESENT_MONEY = "present_money";

    public static final String COL_TOTAL_MONEY = "total_money";

    public static final String COL_IS_DELETED = "is_deleted";

    public static final String COL_PATIENT_ID = "patient_id";
}