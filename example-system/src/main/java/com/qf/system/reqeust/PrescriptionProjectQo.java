package com.qf.system.reqeust;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

/**
 * @author 78458
 */
@Data

public class PrescriptionProjectQo {

    private Long prescriptionId;

    private List<Long> projectIdList;


}