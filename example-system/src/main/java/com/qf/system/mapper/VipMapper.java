package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Vip;import com.qf.system.entity.VipType;import com.qf.system.reqeust.VipRequestParam;import com.qf.system.vo.VipVo;import org.apache.ibatis.annotations.Param;import java.util.List;

public interface VipMapper extends BaseMapper<Vip> {
    List<VipVo> selectVips();

    VipVo selectVipById(VipRequestParam vipRequestParam);

    List<VipVo> searchVip(VipRequestParam vipRequestParam);

    Integer reduceDeposit(VipRequestParam vipRequestParam);
}