package com.qf.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.qf.system.service.UserService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    ValueOperations<String, Object> valueOperations;

    @Resource
    RedisTemplate<String, Object> redisTemplate;

    @Override
    public String login(String phone, String code) {
        /**
         * 强转为 string 获取手机号码和验证码
         */
        String tempCode = (String) valueOperations.get(MsgServiceImpl.PREFIX_REDIS_KEY + phone);
        /**
         * 判断验证码与 redis 中保存的是否一致
         */
        if (!ObjectUtil.isEmpty(tempCode) && tempCode.equals(code)){
            //删除键
            redisTemplate.delete(MsgServiceImpl.PREFIX_REDIS_KEY + phone);
            return "验证成功";
        }
        return null;
    }
}
