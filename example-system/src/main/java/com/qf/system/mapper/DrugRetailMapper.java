package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.DrugRetail;
import com.qf.system.qo.DrugRetailChemicalQo;
import com.qf.system.qo.DrugRetailCostQo;
import com.qf.system.qo.OrderSurchargeQo;
import com.qf.system.vo.DrugRetailChemicalVo;
import com.qf.system.vo.DrugRetailPatientVo;
import com.qf.system.vo.DrugRetailSurchargeVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DrugRetailMapper extends BaseMapper<DrugRetail> {

    /**
     * 添加零售信息
     * @return
     */
    Integer insertDrugRetail(@Param("qo") DrugRetail drugRetail);

    /**
     * 查询药品名称
     * @return
     */
    List<String> selectChemicalName();

    /**
     * 查询患者
     * @return
     */
    DrugRetailPatientVo selectPatient(@Param("str") String str);

    /**
     * 查询药品信息
     * @return
     */
    List<DrugRetailChemicalVo> selectChemical(@Param("qo")DrugRetailChemicalQo drugRetailChemicalQo);

    /**
     * 查询附加费用
     * @return
     */
    List<DrugRetailSurchargeVo> selectSurcharge();

    /**
     * 添加附加费用
     * @param orderSurchargeQo
     * @return
     */
    Integer insertSurcharge(@Param("qo")OrderSurchargeQo orderSurchargeQo);

}