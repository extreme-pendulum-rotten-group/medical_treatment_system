package com.qf.system.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
@Data
public class RegistrationVo {

    /**
     * 挂号单号
     */
    private String registrationNumber;

    /**
     * 患者姓名
     */
    private String patientName;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 出生日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birthday;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 科室名
     */
    private String departmentName;

    /**
     * 医生姓名
     */
    private String username;

    /**
     * 接诊时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date registrationTime;

    /**
     * 挂号状态 1-未就诊 2-已就诊 3-已退号
     */
    private Integer registrationStatus;
}
