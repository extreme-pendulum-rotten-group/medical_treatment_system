package com.qf.system.qo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class OrderHomeQo {

    /**
     * 订单状态 1-未付款 2-已付款 0-已退款
     */
    private Integer orderStatue;

    /**
     * 订单类型 1-挂号 2-零售 3-处方
     */
    private Integer orderType;

    /**
     * 订单号
     */
    private String orderNumber;

    /**
     * 患者姓名
     */
    private String patientName;

    /**
     * 患者卡号
     */
    private String cardNumber;

}
