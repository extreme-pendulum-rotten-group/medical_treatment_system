package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.OutStock;
import com.qf.system.service.impl.OutStockServer;
import com.qf.system.vo.InPagingVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.controller
 * @ClassName : InStockController.java
 * @createTime : 2022/3/29 11:15
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */
@RestController
public class OutStockController {

    @Resource
    OutStockServer outStockServer;

    /**
     * 新增入库信息
     *
     * @param outStock
     * @return
     */
    @PostMapping("/stock/out/add")
    ResponseResult addOutStock(@RequestBody OutStock outStock) {
        return ResponseResult.success(ResultCode.SUCCESS, outStockServer.addOutStock(outStock));
    }

    /**
     * 编辑出库信息
     * @param outStock
     * @return
     */
    @PutMapping("/stock/out/update")
    ResponseResult updateOutStock(@RequestBody OutStock outStock) {
        return ResponseResult.success(ResultCode.SUCCESS, outStockServer.updateOutStock(outStock));
    }

    /**
     * 出库审核
     *
     * @param
     * @return
     */
    @PutMapping("/stock/out/review/update/{outEnable}/{outNumber}")
    ResponseResult outReview(@PathVariable("outEnable") Integer outEnable, @PathVariable("outNumber") String outNumber) {
        return ResponseResult.success(ResultCode.SUCCESS, outStockServer.outReviewEnable(outEnable, outNumber));
    }

    /**
     * 出库管理分页
     * @param inPagingVo
     * @return
     */
    @GetMapping("/out/paging")
    ResponseResult inPaging(@RequestBody InPagingVo inPagingVo) {
        return ResponseResult.success(ResultCode.SUCCESS,outStockServer.outPaging(
                inPagingVo.getPage(),inPagingVo.getSize(),inPagingVo.getParam1(),inPagingVo.getParam2(),inPagingVo.getParam3(),inPagingVo.getParam4()
        ));
    }

    /**
     * 出库删除
     * @param id
     * @return
     */
    @DeleteMapping("/out/delete/{id}")
    ResponseResult deleteById(@PathVariable Integer id){
        return ResponseResult.success(ResultCode.SUCCESS,outStockServer.deleteOutById(id));
    }


}
