package com.qf.system.service;

import com.qf.system.qo.PatientQo;
import com.qf.system.vo.PatientVo;
import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.PatientOnReceptionQo;
import com.qf.system.reqeust.PatientOnTreatmentQo;
import com.qf.system.vo.PatientOnReceptionVo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ZJW
 * @version 1.0.0
 * @Description TODO
 * @ClassName PatientService.java
 * @createTime 2022年03月29日 11:27:00
 */
@Service
public interface PatientService {

    Integer addPatient(PatientQo patientQo);

    List<PatientVo> findPatient();

    ResponseResult selectPatientOnReception();

    ResponseResult selectPatientOnReceptionBySomething(PatientOnReceptionQo patientOnReceptionQo);

    ResponseResult selectPatientOnTreatmentById(PatientOnTreatmentQo patientOnTreatmentQo);
}
