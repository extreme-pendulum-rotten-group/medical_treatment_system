package com.qf.system.service.impl;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.mapper.SupplierMapper;
import com.qf.system.reqeust.SupplierParam;
import com.qf.system.service.SupplierService;
import com.qf.system.vo.SupplierVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Resource
    SupplierMapper supplierMapper;

    /**
     * 查找与展示
     * @param param
     * @return
     */
    @Override
    public ResponseResult selectSupplier(SupplierParam param) {
        List<SupplierVo> supplierVos = supplierMapper.selectSupplier(param);
        if(supplierVos.size() == 0) {
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,supplierVos);
    }

    /**
     * 删除
     * @param list
     * @return
     */
    @Override
    public ResponseResult deleteByList(List<Integer> list) {
        Integer number = supplierMapper.deleteByList(list);
        if (number == null){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 编辑和修改
     * @param param
     * @return
     */
    @Override
    public ResponseResult changeById(SupplierParam param) {
        Integer number = supplierMapper.updateById(param);
        if (number == null){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 新增
     * @param param
     * @return
     */
    @Override
    public ResponseResult addSupplier(SupplierParam param) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //获取当前时间String类型
        String time = sdf.format(date);
        param.setEstablish(time);
        Integer number = supplierMapper.addSupplier(param);
        if (number == null){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }


}
