package com.qf.system.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Random;

/**
 * @author 86180
 */
public class MyUtils {

    /**
     * 截取用户id后四位
     *
     * @param id id
     * @return id后四位
     */
    private static String getId(Long id) {
        String ids = id.toString();
        return ids.substring(ids.length() - 4);
    }



    /**
     * 生成随机编号
     *
     * @param type 平台类型
     * @return 随机编号
     */
    public static String getNumber(String type) {
        String date = DateUtil.format(new Date(), "yyyyMMdd");
        String ranNum = RandomUtil.randomNumbers(2);
        //String time = RandomUtil.randomNumbers(2);
        //String ids = getId(id);
        return type + date + ranNum ;
    }

    /**
     * 生成随机手机号
     */
    public static String getPhone() {
        //前3位:网络识别号
        int i = RandomUtil.randomInt(3, 10);
        //第4～7位:地区编码
        String r = RandomUtil.randomNumbers(9);
        //第8～11位:用户号码
        return 1 + "" + i + r;
    }








}
