package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Physique;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PhysiqueMapper extends BaseMapper<Physique> {
    int addPhysique(Physique physique);

    int updatePhysique(Physique physique);
}