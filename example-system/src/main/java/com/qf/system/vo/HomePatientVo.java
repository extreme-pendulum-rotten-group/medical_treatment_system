package com.qf.system.vo;

import lombok.Data;

import java.util.Date;

@Data
public class HomePatientVo {
    /**
     * 挂号状态
     */
    private Integer registrationStatus;

    /**
     *  挂号单号
     */
    private String registrationNumber;

    /**
     * 患者名称
     */
    private String patientName;

    /**
     * 患者性别
     */
    private Integer gender;

    /**
     * 患者出生日期
     */
    private Date birthday;

    /**
     * 患者性别
     */
    private String phone;

    /**
     * 接诊类型
     */
    private Integer acceptsType;

    /**
     * 科室名称
     */
    private String departmentName;

    /**
     * 医生姓名
     */
    private String username;

    /**
     * 挂号时间
     */
    private Date registrationTime;
}
