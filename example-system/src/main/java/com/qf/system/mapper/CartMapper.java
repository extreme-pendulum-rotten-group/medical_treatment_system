package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Cart;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CartMapper extends BaseMapper<Cart> {
    Integer insertCart(Cart cart);

    Integer updateNumByCid(Integer cartId, Integer num);

    Cart findByOrderNumberAndChemicalId(Integer orderNumber, Integer chemicalId);
}