package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.CostParam;

import java.util.List;

public interface CostService {
    /**
     * 模糊搜索
     * @param param
     * @return
     */
    ResponseResult selectCost (CostParam param);

    /**
     * 批量删除
     * @param list
     * @return
     */
    ResponseResult deleteList(List<Integer> list);

    /**
     * 编辑修改
     * @param param
     * @return
     */
    ResponseResult updateById(CostParam param);

    /**
     * 新增费用
     * @param param
     * @return
     */
    ResponseResult addCost(CostParam param);
}
