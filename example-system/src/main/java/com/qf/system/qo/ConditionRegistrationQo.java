package com.qf.system.qo;

import lombok.Data;

@Data
public class ConditionRegistrationQo {
    /**
     * 科室名
     */
    private String departmentName;

    /**
     * 医生姓名
     */
    private String username;

    /**
     * 挂号状态 1-未就诊 2-已就诊 3-已退号
     */
    private Integer registrationStatus;
}
