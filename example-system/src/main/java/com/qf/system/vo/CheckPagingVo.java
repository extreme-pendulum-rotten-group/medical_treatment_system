package com.qf.system.vo;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.vo
 * @ClassName : CheckPagingVo.java
 * @createTime : 2022/3/31 9:53
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */
@Data
public class CheckPagingVo {
    private Integer page;
    private Integer size;
    private String param1;
    private String param2;
}
