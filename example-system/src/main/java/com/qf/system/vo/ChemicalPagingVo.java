package com.qf.system.vo;

import lombok.Data;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.vo
 * @ClassName : ChemicalPagingVo.java
 * @createTime : 2022/3/31 10:09
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */
@Data
public class ChemicalPagingVo {
    private Integer page;
    private Integer size;
    private String param1;
    private Boolean param2;
    private Date param3;
    private String param4;
    private String param5;
    private String param6;
}
