package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 权限表
 */
@Data
@TableName(value = "lhr_permission")
public class Permission {
    /**
     * 主键
     */
    @TableId(value = "per_id", type = IdType.INPUT)
    private Integer perId;

    /**
     * 权限名
     */
    @TableField(value = "`name`")
    private String name;

    /**
     * 权限说明
     */
    @TableField(value = "decl")
    private String decl;

    /**
     * 是否启用 1 启用 0 禁用
     */
    @TableField(value = "`status`")
    private Integer status;

    public static final String COL_PER_ID = "per_id";

    public static final String COL_NAME = "name";

    public static final String COL_DECL = "decl";

    public static final String COL_STATUS = "status";
}