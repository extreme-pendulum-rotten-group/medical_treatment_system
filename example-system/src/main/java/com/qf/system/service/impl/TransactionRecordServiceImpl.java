package com.qf.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.TransactionRecord;
import com.qf.system.mapper.TransactionRecordMapper;
import com.qf.system.reqeust.VipRequestParam;
import com.qf.system.service.TransactionRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: Rock
 */
@Service
public class TransactionRecordServiceImpl implements TransactionRecordService {
    private static final String ADD_DEPOSIT = "充值";
    private static final String REDUCE_DEPOSIT = "退款";

    @Resource
    TransactionRecordMapper transactionRecordMapper;

    @Override
    public List<TransactionRecord> selectTransactionRecord(VipRequestParam vipRequestParam) {
        return transactionRecordMapper.selectTransactionRecord(vipRequestParam);
    }

    @Override
    public int addAddDepositRecord(VipRequestParam vipRequestParam) {
        Double deposit = vipRequestParam.getDeposit();
        Double present = vipRequestParam.getPresent();
        TransactionRecord transactionRecord = new TransactionRecord();
        transactionRecord.setTransactionType(ADD_DEPOSIT);
        transactionRecord.setTransactionMoney(deposit);
        transactionRecord.setPresentMoney(present);
        transactionRecord.setTotalMoney(deposit + present);
        transactionRecord.setPatientId(vipRequestParam.getPatientId());
        int count = transactionRecordMapper.insert(transactionRecord);
        if (count < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return count;
    }

    @Override
    public int addReduceDepositRecord(VipRequestParam vipRequestParam) {
        TransactionRecord transactionRecord = new TransactionRecord();
        transactionRecord.setTransactionType(REDUCE_DEPOSIT);
        transactionRecord.setTransactionMoney(vipRequestParam.getDeposit());
        transactionRecord.setTotalMoney(vipRequestParam.getDeposit());
        transactionRecord.setPatientId(vipRequestParam.getPatientId());
        int count = transactionRecordMapper.insert(transactionRecord);
        if (count < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return count;
    }
}
