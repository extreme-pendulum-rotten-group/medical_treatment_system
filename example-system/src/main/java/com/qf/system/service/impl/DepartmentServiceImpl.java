package com.qf.system.service.impl;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.Department;
import com.qf.system.mapper.DepartmentMapper;
import com.qf.system.reqeust.DeptParam;
import com.qf.system.service.DepartmentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Resource
    DepartmentMapper deptMapper;

    /**
     * 查找和展示
     * @param param
     * @return
     */
    @Override
    public ResponseResult selectDept(DeptParam param) {
        List<Department> depts = deptMapper.viewDept(param);
        if (depts.size() == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,depts);
    }

    /**
     * 删除
     * @param list
     * @return
     */
    @Override
    public ResponseResult deleteList(List<Integer> list) {
        Integer number = deptMapper.deleteList(list);
        if (number == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 编辑 dept
     * @param param
     * @return
     */
    @Override
    public ResponseResult changeDept(DeptParam param) {
        Integer number = deptMapper.changeDept(param);
        if (number == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 新增 dept
     * @param param
     * @return
     */
    @Override
    public ResponseResult addDept(DeptParam param) {
        Integer number = deptMapper.addDept(param);
        if (number == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }
}
