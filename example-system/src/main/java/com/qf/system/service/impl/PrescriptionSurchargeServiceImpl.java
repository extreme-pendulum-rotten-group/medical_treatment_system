package com.qf.system.service.impl;

import com.qf.system.entity.PrescriptionSurcharge;
import com.qf.system.mapper.PrescriptionSurchargeMapper;
import com.qf.system.reqeust.PrescriptionSurchargeQo;
import com.qf.system.service.PrescriptionSurchargeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author ZJW
 * @version 1.0.0
 * @Description TODO
 * @ClassName PrescriptionSurchargeServiceImpl.java
 * @createTime 2022年03月31日 20:27:00
 */
@Service
public class PrescriptionSurchargeServiceImpl implements PrescriptionSurchargeService {
    @Resource
    PrescriptionSurchargeMapper prescriptionSurchargeMapper;

    @Override
    public Integer insertPrescriptionAndSurcharge(PrescriptionSurchargeQo psq) {
        Integer count =0;
        PrescriptionSurcharge ps = new PrescriptionSurcharge();
        ps.setPrescriptionId(psq.getPrescriptionId());
        for (int i = 0; i < psq.getSurchargeIdList().size(); i++) {
            ps.setSurchargeId(psq.getSurchargeIdList().get(i));
            int j = prescriptionSurchargeMapper.insertPrescriptionAndSurcharge(ps);
            if (j>0){
                count++;
            }
        }
        return count;
    }
}
