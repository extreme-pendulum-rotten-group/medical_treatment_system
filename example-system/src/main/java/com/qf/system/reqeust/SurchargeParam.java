package com.qf.system.reqeust;

import com.qf.system.entity.Surcharge;
import lombok.Data;

@Data
public class SurchargeParam extends Surcharge {
    private String username;
}
