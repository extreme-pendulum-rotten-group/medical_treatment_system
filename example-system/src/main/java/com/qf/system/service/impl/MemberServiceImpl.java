package com.qf.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.Member;
import com.qf.system.mapper.MemberMapper;
import com.qf.system.reqeust.MemberParam;
import com.qf.system.service.MemberService;
import com.qf.system.vo.HomePatientVo;
import com.qf.system.vo.MemberVo;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;
import javax.annotation.Resource;


@Service
 public class MemberServiceImpl implements MemberService {

    @Resource
    MemberMapper memberMapper;

    @Resource
    ValueOperations<String, Object> valueOperations;

    @Resource
    RedisTemplate<String, Object> redisTemplate;

    /**
     * 验证密码与短信
     *
     * @param param
     * @return
     */
    @Override
    public ResponseResult login(MemberParam param) {
        String password = param.getPassword();
        if (password != null) {
            List<MemberVo> memberVos = memberMapper.selectByPhone(param);
            if (memberVos.size() == 0) {
                return ResponseResult.success(ResultCode.SUCCESS, memberVos);
            }
        } else {
            String tempCode = (String) valueOperations.get(MsgServiceImpl.PREFIX_REDIS_KEY + param.getPhone());
            /**
             * 判断验证码与 redis 中保存的是否一致
             */
            if (!ObjectUtil.isEmpty(tempCode) && tempCode.equals(param.getCode())) {
                //删除键
                redisTemplate.delete(MsgServiceImpl.PREFIX_REDIS_KEY + param.getPhone());
                return ResponseResult.success(ResultCode.SUCCESS);
            } else {
                return ResponseResult.error(ResultCode.AUTH_ERROR);
            }
        }
        return ResponseResult.error(ResultCode.AUTH_ERROR);
    }

    /**
     * 更改密码
     *
     * @param param
     * @return
     */
    @Override
    public ResponseResult changePassword(MemberParam param) {
        Integer number = memberMapper.changePassword(param);
        if (number == null) {
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 展示病人挂号记录
     * @return
     */
    @Override
    public ResponseResult selectPatient() {
        List<HomePatientVo> homePatientVos = memberMapper.selectPatient();
        if (homePatientVos.size() == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,homePatientVos);
    }

    /**
     * 查询和展示
     * @param param
     * @return
     */
    @Override
    public ResponseResult selectMember(MemberParam param) {
        List<MemberVo> memberVos = memberMapper.selectMember(param);
        if (memberVos.size() == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,memberVos);
    }

    /**
     * 删除
     * @param list
     * @return
     */
    @Override
    public ResponseResult deleteList(List<Integer> list) {
        Integer number = memberMapper.deleteLisst(list);
        if (number == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 编辑用户信息
     * @param param
     * @return
     */
    @Override
    public ResponseResult changeMember(MemberParam param) {
        Integer number = memberMapper.changeMember(param);
        if (number == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 新增
     * @param param
     * @return
     */
    @Override
    public ResponseResult addMember(MemberParam param) {
        Integer number = memberMapper.addMember(param);
        if (number == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }
}
