package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.PrescriptionProject;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PrescriptionProjectMapper extends BaseMapper<PrescriptionProject> {
    int insertPrescriptionAndProject(PrescriptionProject pp);
}