package com.qf.system.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.InStock;
import com.qf.system.mapper.InStockMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.service.impl
 * @ClassName : InStockServer.java
 * @createTime : 2022/3/29 11:15
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */
@Service
public class InStockServer {

    @Resource
    InStockMapper inStockMapper;


    public String addInStock(InStock stock) {
        Integer result = inStockMapper.addStock(stock);
        if (result != 1) {
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return "新增入库成功";
    }

    public String updateInStock(InStock inStock) {
        Integer result = inStockMapper.updateStock(inStock);
        if (result != 1) {
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return "编辑入库信息成功";
    }

    public String inReviewEnable(Integer inEnable, String inNumber) {
        Integer result = inStockMapper.reviewEnable(inEnable, inNumber);
        if (result == 0) {
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return "入库审核通过";
    }

    public List<InStock> inPaging(Integer page, Integer size, Boolean enable, String type, String number, String supplier) {
        List<InStock> inStocks = inStockMapper.inPaging((page-1)*size, size, enable, type, number, supplier);
        return inStocks;
    }

    public String deleteInById(Integer id) {
        Integer result=inStockMapper.deleteInById(id);
        if(result!=1){
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return "删除成功";
    }
}
