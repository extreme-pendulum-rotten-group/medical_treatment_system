package com.qf.system.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "dossier")
public class DossierVo {

    private String appeal;
    private String presentHistory;
    private String previousHistory;
    private String allergy;
    private String person;
    private String family;
    private String accessoryCheck;
    private String treatmentOptions;
    private String remarks;
    private String cardNumber;


    public static final String COL_ID = "id";

    public static final String COL_APPEAL = "appeal";

    public static final String COL_PRESENT_HISTORY = "present_history";

    public static final String COL_PREVIOUS_HISTORY = "previous_history";

    public static final String COL_ALLERGY = "allergy";

    public static final String COL_PERSON = "person";

    public static final String COL_FAMILY = "family";

    public static final String COL_ACCESSORY_CHECK = "accessory_check";

    public static final String COL_TREATMENT_OPTIONS = "treatment_options";

    public static final String COL_REMARKS = "remarks";

    public static final String COL_CARD_NUMBER = "cardNumber";
}