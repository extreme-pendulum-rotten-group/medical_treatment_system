package com.qf.system.service.impl;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.mapper.PaymentMapper;
import com.qf.system.reqeust.PaymentParam;
import com.qf.system.service.PaymentService;
import com.qf.system.vo.PaymentVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Resource
    PaymentMapper paymentMapper;

    /**
     * 查找与展示
     * @param param
     * @return
     */
    @Override
    public ResponseResult selectPayment(PaymentParam param) {
        List<PaymentVo> paymentVos = paymentMapper.selectPayment(param);
        if (paymentVos.size() == 0){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,paymentVos);
    }

    /**
     * 更改状态
     * @param param
     * @return
     */
    @Override
    public ResponseResult changeById(PaymentParam param) {
        Integer number = paymentMapper.changeById(param);
        if (number == null){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 新增付费方式
     * @param param
     * @return
     */
    @Override
    public ResponseResult addPayment(PaymentParam param) {
        Integer number = paymentMapper.addPayment(param);
        if (number == null){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }
}
