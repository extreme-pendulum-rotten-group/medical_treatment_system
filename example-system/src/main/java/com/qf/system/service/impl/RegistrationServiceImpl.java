package com.qf.system.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.system.mapper.RegistrationMapper;
import com.qf.system.qo.ConditionRegistrationQo;
import com.qf.system.qo.RegistrationQo;
import com.qf.system.service.RegistrationService;
import com.qf.system.utils.MyUtils;
import com.qf.system.vo.RegistrationDetailsVo;
import com.qf.system.vo.RegistrationVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RegistrationServiceImpl implements RegistrationService {
    @Resource
    RegistrationMapper registrationMapper;


    @Override
    public Integer setRegistration(RegistrationQo registrationQo) throws ServiceException {
        if (registrationQo == null) {
            throw new ServiceException(ResultCode.PARAMS_NOT_IS_BLANK);
        }
        registrationQo.setRegistrationNumber(MyUtils.getNumber("GH"));
        Integer integer = registrationMapper.addRegistration(registrationQo);
        if (integer == 0 || integer == null) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return integer;
    }

    @Override
    public List<RegistrationVo> findListByRegistration(Integer status) {
        if (status == null || status == 0) {
            throw new ServiceException(ResultCode.PARAMS_NOT_IS_BLANK);
        }
        List<RegistrationVo> list = registrationMapper.selectRegistration(status);
        if (list.size() == 0) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return list;
    }

    @Override
    public List<RegistrationVo> conditionFindRegistration(ConditionRegistrationQo conditionRegistrationQo) {
        if (conditionRegistrationQo == null) {
            throw new ServiceException(ResultCode.PARAMS_NOT_IS_BLANK);
        }
        List<RegistrationVo> list = registrationMapper.conditionSelectRegistration(conditionRegistrationQo);
        if (list.size() == 0) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return list;
    }

    @Override
    public RegistrationDetailsVo selectRegistrationById(String number) {
        if (number.equals("")) {
            throw new ServiceException(ResultCode.PARAMS_NOT_IS_BLANK);
        }
        RegistrationDetailsVo vo = registrationMapper.selectRegistrationById(number);

        if (vo == null) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return vo;
    }


    @Override
    public Integer updateByNumber(String number,Integer modification) {
        Integer integer = registrationMapper.updateRegistrationStatus(number,modification);
        return integer;
    }
}
