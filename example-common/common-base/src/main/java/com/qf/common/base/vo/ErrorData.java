package com.qf.common.base.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorData {
    private String msg;
    private String filedName;
}
