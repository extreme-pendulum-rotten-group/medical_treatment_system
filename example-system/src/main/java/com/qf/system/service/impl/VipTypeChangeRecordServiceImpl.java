package com.qf.system.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.VipTypeChangeRecord;
import com.qf.system.mapper.VipTypeChangeRecordMapper;
import com.qf.system.reqeust.VipRequestParam;
import com.qf.system.service.VipTypeChangeRecordService;
import com.qf.system.service.VipTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author: hhf
 */
@Service
public class VipTypeChangeRecordServiceImpl implements VipTypeChangeRecordService {
    @Resource
    VipTypeService vipTypeService;

    @Resource
    VipTypeChangeRecordMapper vipTypeChangeRecordMapper;

    @Override
    public int addUpgradeRecord(VipRequestParam vipRequestParam) {
        VipTypeChangeRecord vipTypeChangeRecord = new VipTypeChangeRecord();
        String vipTypeName = vipTypeService.selectVipTypeNameById(vipRequestParam.getVipTypeId());
        vipTypeChangeRecord.setVipTypeName(vipTypeName);
        vipTypeChangeRecord.setChangeType("升级");
        vipTypeChangeRecord.setPatientId(vipRequestParam.getPatientId());
        int count = vipTypeChangeRecordMapper.insert(vipTypeChangeRecord);
        if (count < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return count;
    }
}
