package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "zjw_prescription_project")
public class PrescriptionProject {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 处方id
     */
    @TableField(value = "prescription_id")
    private Long prescriptionId;

    /**
     * 项目id
     */
    @TableField(value = "project_id")
    private Long projectId;

    public static final String COL_ID = "id";

    public static final String COL_PRESCRIPTION_ID = "prescription_id";

    public static final String COL_PROJECT_ID = "project_id";
}