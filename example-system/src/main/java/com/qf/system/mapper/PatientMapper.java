package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Patient;
import com.qf.system.qo.PatientQo;
import com.qf.system.vo.PatientVo;
import org.apache.ibatis.annotations.Param;
import com.qf.system.reqeust.PatientOnReceptionQo;
import com.qf.system.reqeust.PatientOnTreatmentQo;
import com.qf.system.vo.PatientOnReceptionVo;
import com.qf.system.vo.PatientOnTreatmentVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PatientMapper extends BaseMapper<Patient> {

    Integer insert(@Param("qo") PatientQo patientQo);
//    PatientVo selectPatientAndDossier(String cardNumber);

    List<PatientVo> select();

    Integer update(@Param("id") Integer id);

    Patient selectPatientCordNumber(String cardNumber);

    List<PatientOnReceptionVo> selectPatientOnReception();

    List<PatientOnReceptionVo> selectPatientOnReceptionBySomething(PatientOnReceptionQo patientOnReceptionQo);

    PatientOnTreatmentVo selectPatientOnTreatmentById(PatientOnTreatmentQo patientOnTreatmentQo);
}