package com.qf.system.service.impl;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.Clinic;
import com.qf.system.entity.Department;
import com.qf.system.mapper.ClinicMapper;
import com.qf.system.reqeust.ClinicParam;
import com.qf.system.service.ClinicService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ClinicServiceImpl implements ClinicService {

    @Resource
    ClinicMapper clinicMapper;


    @Override
    public ResponseResult selectClc(ClinicParam param) {
        List<Clinic> clinics = clinicMapper.viewDept(param);
        if (clinics.size() == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,clinics);
    }

    /**
     * 删除
     * @param list
     * @return
     */
    @Override
    public ResponseResult deleteList(List<Integer> list) {
        Integer number = clinicMapper.deleteList(list);
        if (number == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 编辑
     * @param param
     * @return
     */
    @Override
    public ResponseResult changeClc(ClinicParam param) {
        Integer number = clinicMapper.changeClc(param);
        if (number == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 新增
     * @param param
     * @return
     */
    @Override
    public ResponseResult addClc(ClinicParam param) {
        Integer number = clinicMapper.addClc(param);
        if (number == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }
}
