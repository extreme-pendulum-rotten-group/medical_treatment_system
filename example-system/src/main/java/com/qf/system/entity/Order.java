package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "order_info")
public class Order {
    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 订单号
     */
    @TableField(value = "order_number")
    private String orderNumber;

    /**
     * 患者id
     */
    @TableField(value = "patient_id")
    private Integer patientId;

    /**
     * 零售（处方）（挂号）单号
     */
    @TableField(value = "retail_number")
    private String retailNumber;

    /**
     * 科室id
     */
    @TableField(value = "department_id")
    private Integer departmentId;

    /**
     * 医生id
     */
    @TableField(value = "member_id")
    private Integer memberId;

    /**
     * 挂号id
     */
    @TableField(value = "registration_id")
    private Integer registrationId;

    /**
     * 是否删除 1-删除 0-不删除
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    /**
     * 订单创建时间
     */
    @TableField(value = "creation_time")
    private Date creationTime;

    /**
     * 订单总额
     */
    @TableField(value = "order_meney")
    private Double orderMeney;

    /**
     * 订单状态 1-未付款 2-已付款 0-已退款
     */
    @TableField(value = "order_statue")
    private Integer orderStatue;

    /**
     * 订单类型 1-挂号 2-零售 3-处方
     */
    @TableField(value = "order_type")
    private Integer orderType;

    public static final String COL_ID = "id";

    public static final String COL_ORDER_NUMBER = "order_number";

    public static final String COL_PATIENT_ID = "patient_id";

    public static final String COL_RETAIL_NUMBER = "retail_number";

    public static final String COL_DEPARTMENT_ID = "department_id";

    public static final String COL_MEMBER_ID = "member_id";

    public static final String COL_REGISTRATION_ID = "registration_id";

    public static final String COL_IS_DELETE = "is_delete";

    public static final String COL_CREATION_TIME = "creation_time";

    public static final String COL_ORDER_MENEY = "order_meney";

    public static final String COL_ORDER_STATUE = "order_statue";

    public static final String COL_ORDER_TYPE = "order_type";
}