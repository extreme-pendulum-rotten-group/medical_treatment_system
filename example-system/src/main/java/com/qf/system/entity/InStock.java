package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.entity
 * @ClassName : OutStock.java
 * @createTime : 2022/3/29 10:57
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */

@Data
@TableName(value = "nf_stock")
public class InStock {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 入库日期
     */
    @TableField(value = "in_stock_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date inStockDate;

    /**
     * 入库人员
     */
    @TableField(value = "in_people")
    private String inPeople;

    /**
     * 入库类型
     */
    @TableField(value = "in_stock_type")
    private String inStockType;

    /**
     * 供应商
     */
    @TableField(value = "supplier")
    private String supplier;

    /**
     * 入库单号
     */
    @TableField(value = "in_number")
    private String inNumber;

    /**
     * 入库制单日期
     */
    @TableField(value = "in_paper_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date inPaperDate;

    /**
     * 入库制单人员
     */
    @TableField(value = "in_paper_people")
    private String inPaperPeople;

    /**
     * 入库备注
     */
    @TableField(value = "in_remarks")
    private String inRemarks;

    /**
     * 入库药品ID
     */
    @TableField(value = "in_chemical_id")
    private Integer inChemicalId;

    /**
     * 入库审核人员
     */
    @TableField(value = "`in_reviewed _people`")
    private String inReviewedPeople;

    /**
     * 入库审核日期
     */
    @TableField(value = "`in_reviewed _date`")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date inReviewedDate;

    /**
     * 入库是否通过审核
     */
    @TableField(value = "in_enable")
    private Boolean inEnable;

    @TableField(value = "purchase_amount")
    private BigDecimal purchaseAmount;


    public static final String COL_ID = "id";

    public static final String COL_IN_ENABLE = "in_enable";

    public static final String COL_OUT_ENABLE = "out_enable";

    public static final String COL_IN_STOCK_DATE = "in_stock_date";

    public static final String COL_IN_PEOPLE = "in_people";

    public static final String COL_IN_STOCK_TYPE = "in_stock_type";

    public static final String COL_SUPPLIER = "supplier";

    public static final String COL_IN_NUMBER = "in_number";

    public static final String COL_IN_PAPER_DATE = "in_paper_date";

    public static final String COL_INT_PAPER_PEOPLE = "in_paper_people";

    public static final String COL_IN_REMARKS = "in_remarks";

    public static final String COL_IN_CHEMICAL_ID = "in_chemical_id";

    public static final String COL_IN_REVIEWED_PEOPLE = "in_reviewed_people";

    public static final String COL_IN_REVIEWED_DATE = "in_reviewed_date";

}