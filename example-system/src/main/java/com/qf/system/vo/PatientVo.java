package com.qf.system.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.Date;

@Data
public class PatientVo {

    /**
     * 挂号单号
     */
    private String registrationNumber;
    /**
     * 患者姓名
     */
    private String patientName;
    /**
     * 性别
     */
    private Integer gender;
    /**
     * 出生日期
     */
    private Date birthday;
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 会员等级
     */
    private String vipClass;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 员工id
     */
    private Long memberId;
}
