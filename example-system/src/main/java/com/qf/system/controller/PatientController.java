package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.qo.PatientQo;
import com.qf.system.service.PatientService;
import com.qf.system.vo.PatientVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController("/patient")
public class PatientController {
    @Resource
    PatientService patientService;



    @PostMapping("/add/patient")
    public ResponseResult addPatient(PatientQo patientQo) {
        Integer integer = patientService.addPatient(patientQo);
        return ResponseResult.success(integer);
    }

    @GetMapping("/find/patient")
    public ResponseResult findPatient() {
        List<PatientVo> list = patientService.findPatient();
        return ResponseResult.success(list);
    }

}
