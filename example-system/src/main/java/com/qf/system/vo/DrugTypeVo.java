package com.qf.system.vo;

import com.qf.system.entity.DrugType;
import lombok.Data;

@Data
public class DrugTypeVo extends DrugType {
    private String name;
}
