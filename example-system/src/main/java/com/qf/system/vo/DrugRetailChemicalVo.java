package com.qf.system.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DrugRetailChemicalVo {
    /**
     * 药品编码
     */
    private String number;

    /**
     * 通用名
     */
    private String name;

    /**
     * 药品类型
     */
    private String type;

    /**
     * 药品规格
     */
    private String specifications;

    /**
     * 生产厂家
     */
    private String producer;

    /**
     * 总量
     */
    private Integer total;

    /**
     * 当前零售价
     */
    private BigDecimal currentRetailPrice;
}
