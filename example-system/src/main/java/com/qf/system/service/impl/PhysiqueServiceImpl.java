package com.qf.system.service.impl;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.entity.Physique;
import com.qf.system.mapper.PhysiqueMapper;
import com.qf.system.service.PhysiqueService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author ZJW
 * @version 1.0.0
 * @Description TODO
 * @ClassName PhysiqueServiceImpl.java
 * @createTime 2022年03月29日 17:14:00
 */
@Service
public class PhysiqueServiceImpl implements PhysiqueService {
    @Resource
    PhysiqueMapper physiqueMapper;

    @Override
    public ResponseResult addPhysique(Physique physique) {
        int count = physiqueMapper.addPhysique(physique);
        if (count>0){
            return ResponseResult.success("添加成功");
        }
        return ResponseResult.error();
    }

    @Override
    public ResponseResult updatePhysique(Physique physique) {
        int count = physiqueMapper.updatePhysique(physique);
        if (count>0){
            return ResponseResult.success("更新成功");
        }
        return ResponseResult.error();
    }
}
