package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.*;
import com.qf.system.reqeust.DrugMsgParam;
import com.qf.system.vo.DrugTypeVo;
import com.qf.system.vo.DrugUsageVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DrugMsgMapper extends BaseMapper<DrugDosage> {
    List<DrugTypeVo> viewDrugType(DrugMsgParam param);

    List<DrugDosage> viewDrugDosage(DrugMsgParam param);

    List<DrugUsageVo> viewDrugUsage(DrugMsgParam param);

    List<Pack> viewPack(DrugMsgParam param);

    List<Invoice> viewInvoice(DrugMsgParam param);

    List<Manufacturer> viewManufacturer(DrugMsgParam param);

    List<Warehouse> viewWarehouse(DrugMsgParam param);
}
