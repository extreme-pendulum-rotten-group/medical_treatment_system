package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName(value = "zjw_cart")
public class Cart {
    /**
     * 购物车数据id
     */
    @TableId(value = "cart_id", type = IdType.INPUT)
    private Integer cartId;

    /**
     * 订单id
     */
    @TableField(value = "order_number")
    private Integer orderNumber;

    /**
     * 药品id
     */
    @TableField(value = "chemical_id")
    private Integer chemicalId;

    /**
     * 药品数量
     */
    @TableField(value = "num")
    private Integer num;

    /**
     * 加入时商品价格
     */
    @TableField(value = "price")
    private BigDecimal price;

    /**
     * 最后价格
     */
    @TableField(value = "real_price")
    private Long realPrice;

    public static final String COL_CART_ID = "cart_id";

    public static final String COL_ORDER_NUMBER = "order_number";

    public static final String COL_CHEMICAL_ID = "chemical_id";

    public static final String COL_NUM = "num";

    public static final String COL_PRICE = "price";

    public static final String COL_REAL_PRICE = "real_price";
}