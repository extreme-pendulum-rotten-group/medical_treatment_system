package com.qf.system.vo;

import com.qf.system.entity.Supplier;
import lombok.Data;

@Data
public class SupplierVo extends Supplier {
}
