package com.qf.system.qo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "wyz_registration")
public class RegistrationQo {

    /**
     * 挂号单号
     */
    @TableField(value = "registration_number")
    private String registrationNumber;

    /**
     * 科室 连接科室表主键
     */
    @TableField(value = "office")
    private Integer office;

    /**
     * 接诊类型 1-出诊 2-复诊
     */
    @TableField(value = "clinical_type")
    private Integer clinicalType;

    /**
     * 接诊医生 连接医生表主键
     */
    @TableField(value = "menber_id")
    private Integer menberId;

    /**
     * 挂号费用 连接附加费用表主键
     */
    @TableField(value = "registration_cost")
    private Integer registrationCost;

    /**
     * 诊疗费用 连接附加费用表
     */
    @TableField(value = "consultation_fee")
    private Integer consultationFee;


    /**
     * 患者信息，连接患者表主键
     */
    @TableField(value = "patient_id")
    private Integer patientId;

    public static final String COL_ID = "id";

    public static final String COL_REGISTRATION_NUMBER = "registration_number";

    public static final String COL_OFFICE = "office";

    public static final String COL_CLINICAL_TYPE = "clinical_type";

    public static final String COL_MENBER_ID = "menber_id";

    public static final String COL_REGISTRATION_COST = "registration_cost";

    public static final String COL_CONSULTATION_FEE = "consultation_fee";

    public static final String COL_REGISTRATION_TIME = "registration_time";

    public static final String COL_PATIENT_ID = "patient_id";
}