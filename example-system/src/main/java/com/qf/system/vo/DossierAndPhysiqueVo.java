package com.qf.system.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qf.system.entity.Physique;
import lombok.Data;

@Data
@TableName(value = "dossier")
public class DossierAndPhysiqueVo {
    private Long id;
    private String appeal;
    private String presentHistory;
    private String previousHistory;
    private String allergy;
    private String person;
    private String family;
    private String accessoryCheck;
    private String treatmentOptions;
    private String remarks;
    private String cardNumber;

   /* private Physique physique;*/

    private String temperature;
    private String breathe;
    private String sphygmus;
    private String bloodPressure;
    private String height;
    private String weight;
    private String bloodGlucose;
    private String bloodFat;
}