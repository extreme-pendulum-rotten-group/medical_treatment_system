package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.entity.Dossier;
import com.qf.system.entity.Physique;
import com.qf.system.reqeust.DossierAndPhysiqueQo;
import com.qf.system.reqeust.DossierQo;
import com.qf.system.reqeust.PatientOnReceptionQo;
import com.qf.system.service.DossierService;
import com.qf.system.service.PatientService;
import com.qf.system.service.PhysiqueService;
import com.qf.system.vo.DossierAndPhysiqueVo;
import com.qf.system.vo.DossierVo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ZJW
 * @version 1.0.0
 * @Description TODO
 * @ClassName WorkController.java
 * @createTime 2022年03月28日 17:31:00
 */
@RestController("/work")
public class WorkController {
    @Resource
    DossierService dossierService;

    @Resource
    PatientService patientService;

    @Resource
    PhysiqueService physiqueService;


    /**
     * 添加病历信息
     * @param dossierQo
     * @return
     * @throws Exception
     */
    @PostMapping("/add/dossier")
    public ResponseResult<DossierVo> addDossier(@RequestBody DossierQo dossierQo) throws Exception {
        DossierVo dossierVo = dossierService.addDossier(dossierQo);
        return ResponseResult.success(dossierVo);
    }

    /**
     * 根据患者卡号查询病历信息
     * @param cardNumber
     * @return
     */
    @PostMapping("/select/dossier/by/cardNumber")
    public ResponseResult<List<DossierVo>> selectDossierByCardNumber(String cardNumber) throws Exception {
        return ResponseResult.success(dossierService.selectDossierByCardNumber(cardNumber));
    }

    /**
     * 根据id更新病历信息
     * @param
     * @return
     * @throws Exception
     */
    @PostMapping("/update/dossier/by/id")
    public ResponseResult updateDossierById(@RequestBody Dossier dossier){
        return ResponseResult.success(dossierService.updateDossierById(dossier));
    }

    /**
     * 添加体格信息
     * @param physique
     * @return
     */
    @PostMapping("/add/physique")
    public ResponseResult addPhysique(@RequestBody Physique physique){
        return ResponseResult.success(physiqueService.addPhysique(physique));
    }

    /**
     * 更新体格信息
     * @param physique
     * @return
     */
    @PostMapping("/update/physique")
    public ResponseResult updatePhysique(@RequestBody Physique physique){
        return ResponseResult.success(physiqueService.updatePhysique(physique));
    }

    /**
     * 查询病历和体格信息
     * @param dossierAndPhysiqueQo
     * @return
     */
    @PostMapping("select/dossier/and/physique")
    public ResponseResult<DossierAndPhysiqueVo> selectDossierAndPhysique(@RequestBody DossierAndPhysiqueQo dossierAndPhysiqueQo){
        return ResponseResult.success(dossierService.selectDossierAndPhysique(dossierAndPhysiqueQo));
    }

    /**
     * 查询工作台上的全部患者信息
     * @return
     */
    @PostMapping("select/patient/on/reception")
    public ResponseResult selectPatientOnReception(){
        return ResponseResult.success(patientService.selectPatientOnReception());
    }

    /**
     * 根据条件查询工作台上的全部患者信息
     * @param patientOnReceptionQo
     * @return
     */
    @PostMapping("select/patient/on/reception/by/something")
    public ResponseResult selectPatientOnReceptionBySomething(PatientOnReceptionQo patientOnReceptionQo){
        return ResponseResult.success(patientService.selectPatientOnReceptionBySomething(patientOnReceptionQo));
    }

}
