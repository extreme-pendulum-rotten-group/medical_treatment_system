package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "zjw_prescription_surcharge")
public class PrescriptionSurcharge {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 处方id
     */
    @TableField(value = "prescription_id")
    private Integer prescriptionId;

    /**
     * 额外费用id
     */
    @TableField(value = "surcharge_id")
    private Integer surchargeId;

    public static final String COL_ID = "id";

    public static final String COL_PRESCRIPTION_ID = "prescription_id";

    public static final String COL_SURCHARGE_ID = "surcharge_id";
}