package com.qf.system.service.impl;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.*;
import com.qf.system.mapper.DrugMsgMapper;
import com.qf.system.reqeust.DrugMsgParam;
import com.qf.system.service.DrugMsgService;
import com.qf.system.vo.DrugTypeVo;
import com.qf.system.vo.DrugUsageVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DrugMsgServiceImpl implements DrugMsgService {

    @Resource
    DrugMsgMapper drugMsgMapper;

    @Override
    public ResponseResult viewDrugType(DrugMsgParam param) {
        List<DrugTypeVo> drugTypes = drugMsgMapper.viewDrugType(param);
        if (drugTypes.size() == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,drugTypes);
    }

    @Override
    public ResponseResult viewDrugDosage(DrugMsgParam param) {
        List<DrugDosage> drugDosages = drugMsgMapper.viewDrugDosage(param);
        if (drugDosages.size() == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,drugDosages);
    }

    @Override
    public ResponseResult viewDrugUsage(DrugMsgParam param) {
        List<DrugUsageVo> drugUsages = drugMsgMapper.viewDrugUsage(param);
        if (drugUsages.size() == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,drugUsages);
    }

    @Override
    public ResponseResult viewPack(DrugMsgParam param) {
        List<Pack> packs = drugMsgMapper.viewPack(param);
        if (packs.size() == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,packs);
    }

    @Override
    public ResponseResult viewInvoice(DrugMsgParam param) {
        List<Invoice> invoices = drugMsgMapper.viewInvoice(param);
        if (invoices.size() == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,invoices);
    }

    @Override
    public ResponseResult viewManufacturer(DrugMsgParam param) {
        List<Manufacturer> manufacturers = drugMsgMapper.viewManufacturer(param);
        if (manufacturers.size() == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,manufacturers);
    }

    @Override
    public ResponseResult viewWarehouse(DrugMsgParam param) {
        List<Warehouse> warehouses = drugMsgMapper.viewWarehouse(param);
        if (warehouses.size() == 0){
            return ResponseResult.error(ResultCode.AUTH_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,warehouses);
    }
}
