package com.qf.system.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ChemicalOnReceptionVo {

    private Integer id;
    /**
     * 通用名
     */
    private String name;
    /**
     * 药品类型
     */
    private String type;
    /**
     * 药品规格
     */
    private String specifications;
    /**
     * 零售价
     */
    private BigDecimal retailPrice;

    /**
     * 当前库存数
     */
    private Double stockCurrent;






}