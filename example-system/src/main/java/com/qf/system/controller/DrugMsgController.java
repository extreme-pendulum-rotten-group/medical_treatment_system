package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.DrugMsgParam;
import com.qf.system.service.DrugMsgService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/drug")
public class DrugMsgController {

    @Resource
    DrugMsgService drugMsgService;

    /**
     * 查找与展示 药品分类
     * @param param
     * @return
     */
    @GetMapping("/view/drug/type")
    public ResponseResult viewDrugType (DrugMsgParam param){
        return drugMsgService.viewDrugType(param);
    }

    /**
     * 查找与展示 药品剂型
     * @param param
     * @return
     */
    @GetMapping("/view/drug/dosage")
    public ResponseResult viewDrugDosage (DrugMsgParam param){
        return drugMsgService.viewDrugDosage(param);
    }

    /**
     * 查找与展示 药品用法
     * @param param
     * @return
     */
    @GetMapping("/view/drug/usage")
    public ResponseResult viewDrugUsage (DrugMsgParam param){
        return drugMsgService.viewDrugUsage(param);
    }

    /**
     * 查找与展示 包装
     * @param param
     * @return
     */
    @GetMapping("/view/pack")
    public ResponseResult viewPack (DrugMsgParam param){
        return drugMsgService.viewPack(param);
    }

    /**
     * 查找与展示 发票
     * @param param
     * @return
     */
    @GetMapping("/view/invoice")
    public ResponseResult viewInvoice (DrugMsgParam param){
        return drugMsgService.viewInvoice(param);
    }

    /**
     * 查找与展示 生厂商
     * @param param
     * @return
     */
    @GetMapping("/view/manufacturer")
    public ResponseResult viewManufacturer (DrugMsgParam param){
        return drugMsgService.viewManufacturer(param);
    }

    /**
     * 查找与展示 出入库
     * @param param
     * @return
     */
    @GetMapping("/view/warehouse")
    public ResponseResult viewWarehouse (DrugMsgParam param){
        return drugMsgService.viewWarehouse(param);
    }
}
