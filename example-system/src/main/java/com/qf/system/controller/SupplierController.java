package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.SupplierParam;
import com.qf.system.service.SupplierService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Resource
    SupplierService supplierService;

    /**
     * 查找与展示
     * 梁浩然
     * @param param
     * @return
     */
    @GetMapping("/view/supplier")
    public ResponseResult selectSupplier(SupplierParam param) {
        return supplierService.selectSupplier(param);
    }

    /**
     * 删除
     * 梁浩然
     * @param array
     * @return
     */
    @PostMapping("/delete/list")
    public ResponseResult deleteList (@RequestParam(value="array")Integer[] array){
        List<Integer> list = Arrays.asList(array);
        return  supplierService.deleteByList(list);
    }

    /**
     * 编辑和修改
     * 梁浩然
     * @param param
     * @return
     */
    @PostMapping("/change/id")
    public ResponseResult changeId (SupplierParam param){
        return supplierService.changeById(param);
    }

    /**
     * 新增
     * 梁浩然
     * @param param
     * @return
     */
    @PostMapping("/add/supplier")
    public ResponseResult addSupplier (SupplierParam param){
        return supplierService.addSupplier(param);
    }
}
