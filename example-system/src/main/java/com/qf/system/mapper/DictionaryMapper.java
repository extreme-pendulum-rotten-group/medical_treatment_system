package com.qf.system.mapper;

import com.qf.system.entity.Dossier;
import com.qf.system.reqeust.DossierQo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DictionaryMapper {
    //主诉信息
    List<Dossier> selectMainSuit(DossierQo dossierQo);

    int deleteMainSuit(Long id);

    int addMainSuit(Dossier dossier);

    List<Dossier> selectappeal(@Param("appeal") Dossier appeal);

    //现病史
    List<Dossier> selectPresents(DossierQo dossierQo);

    int deletePresent(Long id);

    int addPresent(Dossier dossier);

    List<Dossier> selectPresent(@Param("presentHistory") Dossier appeal);
}
