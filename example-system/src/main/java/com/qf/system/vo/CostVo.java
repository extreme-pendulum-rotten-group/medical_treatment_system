package com.qf.system.vo;

import com.qf.system.entity.Cost;
import lombok.Data;

@Data
public class CostVo extends Cost {
    private String username;
}
