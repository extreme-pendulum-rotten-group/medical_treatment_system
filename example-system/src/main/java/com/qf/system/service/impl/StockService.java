package com.qf.system.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.Stock;
import com.qf.system.mapper.StockMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.service.impl
 * @ClassName : StockService.java
 * @createTime : 2022/3/31 15:11
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */
@Service
public class StockService {
@Resource
    StockMapper stockMapper;

    public List<Stock> stockPaging(Integer page, Integer size, String param1, String param2, String param3, String param4) {
        List<Stock> stocks =stockMapper.stockPaging((page-1)*size,size,param1,param2,param3,param4);
        if (stocks ==null){
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return stocks;
    }
}
