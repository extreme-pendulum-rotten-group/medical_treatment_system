package com.qf.system.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class DrugRetailPatientVo {
    /**
     * 主键
     */
    private Long patientId;
    /**
     * 患者姓名
     */
    private String patientName;

    /**
     * 患者卡号
     */
    private String cardNumber;

    /**
     * 出生日期
     */
    private Date birthday;
    /**
     * 性别
     */
    private Integer gender;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 证件号码
     */
    private String idNumber;
}
