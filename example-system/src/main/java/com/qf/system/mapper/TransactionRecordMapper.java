package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.TransactionRecord;
import com.qf.system.reqeust.VipRequestParam;

import java.util.List;

/**
 * @author hhf
 */
public interface TransactionRecordMapper extends BaseMapper<TransactionRecord> {
    default List<TransactionRecord> selectTransactionRecord(VipRequestParam vipRequestParam) {
        QueryWrapper<TransactionRecord> qw = new QueryWrapper<>();
        qw.lambda().eq(TransactionRecord::getPatientId, vipRequestParam.getPatientId());
        return this.selectList(qw);
    }
}