package com.qf.system.reqeust;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
    * 患者表
 * @author 78458
 */
@Data
public class PatientOnReceptionQo {

    private Long patientId;
    private String patientName;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date birthday;
    private String cardNumber;
    private String phone;
    private Integer vipTypeId;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date createTime;
    private Integer registrationStatus;

    private String departmentName;
    private String  decl;





}