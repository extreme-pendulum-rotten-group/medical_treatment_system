package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.entity
 * @ClassName : OutStock.java
 * @createTime : 2022/3/29 10:57
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */

@Data
@TableName(value = "nf_stock")
public class OutStock {
    /**
     * 库存编号
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;
    /**
     * 供应商
     */
    @TableField(value = "supplier")
    private String supplier;
    /**
     * 出库单号
     */
    @TableField(value = "out_number")
    private String outNumber;

    /**
     * 出库日期
     */
    @TableField(value = "out_stock_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date outStockDate;

    /**
     * 出库人员
     */
    @TableField(value = "out_people")
    private String outPeople;

    /**
     * 出库类型
     */
    @TableField(value = "out_stock_type")
    private String outStockType;

    /**
     * 出库制单日期
     */
    @TableField(value = "out_paper_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date outPaperDate;

    /**
     * 出库制单人员
     */
    @TableField(value = "out_paper_people")
    private String outPaperPeople;

    /**
     * 出库备注
     */
    @TableField(value = "out_remarks")
    private String outRemarks;

    /**
     * 出库药品ID
     */
    @TableField(value = "out_chemical_id")
    private Integer outChemicalId;

    /**
     * 出库审核人员
     */
    @TableField(value = "`out_reviewed_people`")
    private String outReviewedPeople;

    /**
     * 出库审核日期
     */
    @TableField(value = "`out_reviewed_date`")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date outReviewedDate;

    /**
     * 出库审核是否通过
     */
    @TableField(value = "out_enable")
    private Boolean outEnable;

    public static final String COL_ID = "id";

    public static final String COL_SUPPLIER = "supplier";

    public static final String COL_OUT_NUMBER = "out_number";

    public static final String COL_OUT_STOCK_DATE = "out_stock_date";

    public static final String COL_OUT_PEOPLE = "out_people";

    public static final String COL_OUT_STOCK_TYPE = "out_stock_type";

    public static final String COL_OUT_PAPER_DATE = "out_paper_date";

    public static final String COL_OUT_PAPER_PEOPLE = "out_paper_people";

    public static final String COL_OUT_REMARKS = "out_remarks";

    public static final String COL_OUT_CHEMICAL_ID = "out_chemical_id";

    public static final String COL_OUT_REVIEWED_PEOPLE = "out_reviewed_people";

    public static final String COL_OUT_REVIEWED_DATE = "out_reviewed_date";
}