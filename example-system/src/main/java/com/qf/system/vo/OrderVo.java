package com.qf.system.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class OrderVo {

    /**
     * 订单主键id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 订单号
     */
    @TableField(value = "order_number")
    private String orderNumber;
    /**
     * 订单类型 1-挂号 2-零售 3-处方
     */
    @TableField(value = "order_type")
    private Integer orderType;


    /**
     * 患者姓名
     */
    @TableField(value = "patient_name")
    private String patientName;


    /**
     * 性别
     */
    @TableField(value = "gender")
    private Integer gender;

    /**
     * 手机号码
     */
    @TableField(value = "phone")
    private String phone;

    /**
     * 出生日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @TableField(value = "birthday")
    private Date birthday;


    /**
     * 科室名
     */
    @TableField(value = "department_name")
    private String departmentName;

    /**
     * 订单创建时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @TableField(value = "creation_time")
    private Date creationTime;

    /**
     * 订单总额
     */
    @TableField(value = "order_meney")
    private Double orderMeney;

    /**
     * 订单状态 1-未付款 2-已付款 0-已退款
     */
    @TableField(value = "order_statue")
    private Integer orderStatue;
}
