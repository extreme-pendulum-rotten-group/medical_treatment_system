package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.*;
import com.qf.system.service.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author ZJW
 * @version 1.0.0
 * @Description TODO
 * @ClassName TreatmentController.java
 * @createTime 2022年03月28日 20:13:00
 */
@RestController
@RequestMapping("/treatment")
public class TreatmentController {

    @Resource
    PatientService patientService;

    @Resource
    ZJWChemicalService zjwChemicalService;

    @Resource
    ZJWCheckProjectService zjwCheckProjectService;

    @Resource
    PrescriptionChemicalService prescriptionChemicalService;

    @Resource
    PrescriptionProjectService prescriptionProjectService;

    /**
     * 查询处方项目的药品
     * @return
     */
    @PostMapping("/select/all/chemical/and/stock")
    public ResponseResult selectAllChemicalAndStock(){
        return ResponseResult.success(zjwChemicalService.selectAllChemicalAndStock());
    }

    /**
     * 通过条件查询处方的药品
     * @param chemicalQo
     * @return
     */
    @PostMapping("/select/chemical/by/something")
    public ResponseResult selectChemicalBySomething(ChemicalOnReceptionQo chemicalQo){
        return ResponseResult.success(zjwChemicalService.selectChemicalBySomething(chemicalQo));
    }

    /**
     * 查询处方表的检查项目
     * @return
     */
    @PostMapping("/select/all/check/project/on/prescription")
    public ResponseResult selectAllCheckProjectOnPrescription(){
        return ResponseResult.success(zjwCheckProjectService.selectAllCheckProjectOnPrescription());
    }

    /**
     * 通过条件查询处方表中的检查项目
     * @param checkProjectOnPrescriptionQo
     * @return
     */
    @PostMapping("/select/check/project/on/prescription/by/something")
    public ResponseResult selectCheckProjectOnPrescriptionBySomething(CheckProjectOnPrescriptionQo checkProjectOnPrescriptionQo){
        return ResponseResult.success(zjwCheckProjectService.selectCheckProjectOnPrescriptionBySomething(checkProjectOnPrescriptionQo));
    }


    /**
     * 一条处方添加多条药品数据
     * @param prescriptionChemicalQo
     * @return
     * 注意事项：@RequestBody 有这个注解，才可以从前端传入json数据解析
     */
    @PostMapping("/insert/prescription")
    public ResponseResult insertPrescription(@RequestBody PrescriptionChemicalQo prescriptionChemicalQo){
        return ResponseResult.success(prescriptionChemicalService.insertPrescription(prescriptionChemicalQo));
    }


    /**
     * 插入处方的检查项目
     * @param ppq
     * @return
     */
    @PostMapping("/insert/prescription/and/project")
    public ResponseResult insertPrescriptionAndProject(@RequestBody PrescriptionProjectQo ppq){
        return ResponseResult.success(prescriptionProjectService.insertPrescriptionAndProject(ppq));
    }


    /**
     * 根据患者id查询部分信息和部分病历信息
     * @param patientOnTreatmentQo
     * @return
     */
    @PostMapping("select/patient/on/treatment/by/id")
    public ResponseResult selectPatientOnTreatmentById (PatientOnTreatmentQo patientOnTreatmentQo){
        return ResponseResult.success(patientService.selectPatientOnTreatmentById(patientOnTreatmentQo));
    }

    /**
     *在处方里添加附加费用
     */
    @Resource
    PrescriptionSurchargeService prescriptionSurchargeService;
    @PostMapping("/insert/prescription/and/surcharge")
    public ResponseResult insertPrescriptionAndSurcharge(@RequestBody  PrescriptionSurchargeQo psq){
        return ResponseResult.success(prescriptionSurchargeService.insertPrescriptionAndSurcharge(psq));
    }


}
