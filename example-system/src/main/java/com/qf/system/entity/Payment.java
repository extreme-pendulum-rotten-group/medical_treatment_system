package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "lhr_payment")
public class Payment {
    /**
     * 支付方式ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 方式名称
     */
    @TableField(value = "payment_name")
    private String paymentName;

    /**
     * 状态 - 1：启用；2：禁用
     */
    @TableField(value = "`status`")
    private Integer status;

    public static final String COL_ID = "id";

    public static final String COL_PAYMENT_NAME = "payment_name";

    public static final String COL_STATUS = "status";
}