package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.CheckProject;
import com.qf.system.reqeust.CheckParam;import com.qf.system.vo.CheckVo;import org.apache.ibatis.annotations.Mapper;import java.util.List;

@Mapper
public interface CheckProjectMapper extends BaseMapper<CheckProject> {
    /**
     * 查询和展示
     *
     * @param param
     * @return
     */
    List<CheckVo> selectCheck(CheckParam param);

    /**
     * 删除
     *
     * @param list
     * @return
     */
    Integer deleteByList(List<Integer> list);

    /**
     * 编辑个修改
     *
     * @param param
     * @return
     */
    Integer changeById(CheckParam param);

    /**
     * 新增
     *
     * @param param
     * @return
     */
    Integer addCheck(CheckParam param);
}