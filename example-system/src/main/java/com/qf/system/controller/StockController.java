package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.service.impl.StockService;
import com.qf.system.vo.StockPagingVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.controller
 * @ClassName : StockController.java
 * @createTime : 2022/3/31 15:11
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */
@RestController
public class StockController {
    @Resource
    StockService stockService;

    @GetMapping("/stock/paging")
    ResponseResult stockPaging(@RequestBody StockPagingVo stockPagingVo) {
       return ResponseResult.success(ResultCode.SUCCESS,stockService.stockPaging(
                stockPagingVo.getPage(),stockPagingVo.getSize(),stockPagingVo.getParam1(),stockPagingVo.getParam2(),
               stockPagingVo.getParam3(),stockPagingVo.getParam4()
        ));
    }
}
