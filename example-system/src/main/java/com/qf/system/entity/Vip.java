package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

/**
 * 患者表
 */
@Data
@TableName(value = "hhf_patient")
@Validated
public class Vip {
    public interface Deposit{

    }

    /**
     * 主键
     */
    @TableId(value = "patient_id", type = IdType.INPUT)
    private Long patientId;

    /**
     * 患者姓名
     */
    @TableField(value = "patient_name")
    private String patientName;

    /**
     * 患者卡号
     */
    @TableField(value = "card_number")
    private String cardNumber;

    /**
     * 出生日期
     */
    @TableField(value = "birthday")
    private Date birthday;

    /**
     * 性别
     */
    @TableField(value = "gender")
    private Integer gender;

    /**
     * 手机号码
     */
    @TableField(value = "phone")
    private String phone;

    /**
     * 证件号码
     */
    @TableField(value = "id_number")
    private String idNumber;

    /**
     * 患者来源id
     */
    @TableField(value = "patient_source_id")
    private Integer patientSourceId;

    /**
     * 会员类型id
     */
    @TableField(value = "vip_type_id")
    private Integer vipTypeId;

    /**
     * 到期时间
     */
    @TableField(value = "expiration_time")
    private Date expirationTime;

    /**
     * 民族
     */
    @TableField(value = "nation")
    private String nation;

    /**
     * 婚姻状况：1未婚，2已婚，3保密
     */
    @TableField(value = "marital_status")
    private Integer maritalStatus;

    /**
     * 地址
     */
    @TableField(value = "address")
    private String address;

    /**
     * 患者学历id
     */
    @TableField(value = "patient_education_background_id")
    private Integer patientEducationBackgroundId;

    /**
     * 患者职业id
     */
    @TableField(value = "patient_occupation_id")
    private Integer patientOccupationId;

    /**
     * 工作单位
     */
    @TableField(value = "work_unit")
    private String workUnit;

    /**
     * 备注
     */
    @TableField(value = "remarks")
    private String remarks;

    /**
     * 累计消费
     */
    @TableField(value = "all_consumption")
    private Double allConsumption;

    /**
     * 储值余额
     */
    @TableField(value = "balance")
    @NotNull(groups = Deposit.class,message = "储值余额不能为空")
    private Double balance;

    /**
     * 累计储值
     */
    @NotNull(groups = Deposit.class,message = "累计储值不能为空")
    @TableField(value = "all_deposit")
    private Double allDeposit;

    /**
     * 会员状态：1启用，0禁用
     */
    @TableField(value = "vip_status")
    private Integer vipStatus;

    /**
     * 会员积分
     */
    @NotNull(groups = Deposit.class,message = "会员积分不能为空")
    @TableField(value = "vip_points")
    private Double vipPoints;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 接诊状态：1待接诊，2接诊中，3已完成
     */
    @TableField(value = "accepts_status")
    private Integer acceptsStatus;

    /**
     * 员工id
     */
    @TableField(value = "member_id")
    private Long memberId;

    /**
     * 累计赠送
     */
    @NotNull(groups = Deposit.class,message = "累计赠送不能为空")
    @TableField(value = "all_presents")
    private Double allPresents;

    /**
     * 是否删除：1已删除，0未删除
     */
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    public static final String COL_PATIENT_ID = "patient_id";

    public static final String COL_PATIENT_NAME = "patient_name";

    public static final String COL_CARD_NUMBER = "card_number";

    public static final String COL_BIRTHDAY = "birthday";

    public static final String COL_GENDER = "gender";

    public static final String COL_PHONE = "phone";

    public static final String COL_ID_NUMBER = "id_number";

    public static final String COL_PATIENT_SOURCE_ID = "patient_source_id";

    public static final String COL_VIP_TYPE_ID = "vip_type_id";

    public static final String COL_EXPIRATION_TIME = "expiration_time";

    public static final String COL_NATION = "nation";

    public static final String COL_MARITAL_STATUS = "marital_status";

    public static final String COL_ADDRESS = "address";

    public static final String COL_PATIENT_EDUCATION_BACKGROUND_ID = "patient_education_background_id";

    public static final String COL_PATIENT_OCCUPATION_ID = "patient_occupation_id";

    public static final String COL_WORK_UNIT = "work_unit";

    public static final String COL_REMARKS = "remarks";

    public static final String COL_ALL_CONSUMPTION = "all_consumption";

    public static final String COL_BALANCE = "balance";

    public static final String COL_ALL_DEPOSIT = "all_deposit";

    public static final String COL_VIP_STATUS = "vip_status";

    public static final String COL_VIP_POINTS = "vip_points";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_ACCEPTS_STATUS = "accepts_status";

    public static final String COL_MEMBER_ID = "member_id";

    public static final String COL_ALL_PRESENTS = "all_presents";

    public static final String COL_IS_DELETED = "is_deleted";
}