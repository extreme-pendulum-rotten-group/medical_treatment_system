package com.qf.system.vo;

import com.qf.system.entity.DrugUsage;
import lombok.Data;

@Data
public class DrugUsageVo extends DrugUsage {
    private String name;
}
