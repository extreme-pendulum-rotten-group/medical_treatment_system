package com.qf.system.entity;

import java.util.Date;

public class Registration {
    /**
     * 主键id
     */
    private Integer id;

    /**
     * 挂号单号
     */
    private String registrationNumber;

    /**
     * 科室 连接科室表主键
     */
    private Integer office;

    /**
     * 接诊类型 1-出诊 2-复诊
     */
    private Integer clinicalType;

    /**
     * 接诊医生 连接医生表主键
     */
    private Integer menberId;

    /**
     * 挂号费用 连接附加费用表主键
     */
    private Integer registrationCost;

    /**
     * 诊疗费用 连接附加费用表
     */
    private Integer consultationFee;

    /**
     * 挂号时间
     */
    private Date registrationTime;

    /**
     * 患者信息，连接患者表主键
     */
    private Integer patientId;

    /**
     * 挂号状态 1-未就诊 2-已就诊 3-已退号
     */
    private Integer registrationStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Integer getOffice() {
        return office;
    }

    public void setOffice(Integer office) {
        this.office = office;
    }

    public Integer getClinicalType() {
        return clinicalType;
    }

    public void setClinicalType(Integer clinicalType) {
        this.clinicalType = clinicalType;
    }

    public Integer getMenberId() {
        return menberId;
    }

    public void setMenberId(Integer menberId) {
        this.menberId = menberId;
    }

    public Integer getRegistrationCost() {
        return registrationCost;
    }

    public void setRegistrationCost(Integer registrationCost) {
        this.registrationCost = registrationCost;
    }

    public Integer getConsultationFee() {
        return consultationFee;
    }

    public void setConsultationFee(Integer consultationFee) {
        this.consultationFee = consultationFee;
    }

    public Date getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(Date registrationTime) {
        this.registrationTime = registrationTime;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getRegistrationStatus() {
        return registrationStatus;
    }

    public void setRegistrationStatus(Integer registrationStatus) {
        this.registrationStatus = registrationStatus;
    }
}

