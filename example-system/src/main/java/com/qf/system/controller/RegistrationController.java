package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.qo.ConditionRegistrationQo;
import com.qf.system.qo.RegistrationQo;
import com.qf.system.service.RegistrationService;
import com.qf.system.vo.RegistrationDetailsVo;
import com.qf.system.vo.RegistrationVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController("/registration")
public class RegistrationController {
    @Resource
    RegistrationService registrationService;

    @PostMapping("/add/registration")
    public ResponseResult addRegistration(RegistrationQo registrationQo) {
        Integer integer = registrationService.setRegistration(registrationQo);
        return ResponseResult.success(integer);
    }

    @GetMapping("/find/list/by/registration")
    public ResponseResult findListByRegistration(Integer status) {
        List<RegistrationVo> list = registrationService.findListByRegistration(status);
        return ResponseResult.success(list);
    }

    @GetMapping("/condition/find/registration")
    public ResponseResult conditionFindRegistration(ConditionRegistrationQo conditionRegistrationQo) {
        List<RegistrationVo> list = registrationService.conditionFindRegistration(conditionRegistrationQo);
        return ResponseResult.success(list);
    }

    @GetMapping("/find/registration/by/number")
    public ResponseResult findRegistrationByNumber(String number) {
        RegistrationDetailsVo vo = registrationService.selectRegistrationById(number);
        return ResponseResult.success(vo);
    }
}
