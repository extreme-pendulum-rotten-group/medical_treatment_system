package com.qf.system.service.impl;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.Dossier;
import com.qf.system.entity.Patient;
import com.qf.system.mapper.DossierMapper;
import com.qf.system.mapper.PatientMapper;
import com.qf.system.reqeust.DossierAndPhysiqueQo;
import com.qf.system.reqeust.DossierQo;
import com.qf.system.service.DossierService;
import com.qf.system.vo.DossierAndPhysiqueVo;
import com.qf.system.vo.DossierVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ZJW
 * @version 1.0.0
 * @Description TODO
 * @ClassName DossierServiceImpl.java
 * @createTime 2022年03月28日 20:44:00
 */
@Service
public class DossierServiceImpl implements DossierService {
    @Resource
    DossierMapper dossierMapper;

    @Resource
    PatientMapper patientMapper;

    @Override
    public DossierVo addDossier(DossierQo dossierQo) throws Exception {
        int count = dossierMapper.addDossier(dossierQo);
        Dossier dossier = new Dossier();
        BeanUtils.copyProperties(dossierQo,dossier);
        if (count>0){
            DossierVo dossierVo = new DossierVo();
            BeanUtils.copyProperties(dossier,dossierVo);
            return dossierVo;
        }else {
            throw new Exception("添加病历失败");
        }
    }

    @Override
    public List<DossierVo> selectDossierByCardNumber(String cardNumber) throws Exception {
        Patient patient= patientMapper.selectPatientCordNumber(cardNumber);
        if (ObjectUtils.isEmpty(patient)){
            throw new Exception("患者不存在或卡号错误");
        }else{
            List<DossierVo> dossierVoList= dossierMapper.selectDossierByCardNumber(cardNumber);
            if (ObjectUtils.isEmpty(dossierVoList)){
                throw new Exception("该患者还未有病历信息");
            }else {
                return dossierVoList;
            }
        }
    }

    @Override
    public ResponseResult updateDossierById(Dossier dossier) {
        int count = dossierMapper.updateDossierById(dossier);
        if (count>0){
            return ResponseResult.success(ResultCode.SUCCESS);
        }
        return ResponseResult.error(ResultCode.ERROR);
    }

    @Override
    public DossierAndPhysiqueVo selectDossierAndPhysique(DossierAndPhysiqueQo dossierAndPhysiqueQo) {
        DossierAndPhysiqueQo andPhysiqueQo = dossierMapper.selectDossierAndPhysique(dossierAndPhysiqueQo.getId());
        if (!ObjectUtils.isEmpty(andPhysiqueQo)){
            DossierAndPhysiqueVo andPhysiqueVo = new DossierAndPhysiqueVo();
            BeanUtils.copyProperties(andPhysiqueQo,andPhysiqueVo);
            return andPhysiqueVo;
        }
        return null;
    }
}
