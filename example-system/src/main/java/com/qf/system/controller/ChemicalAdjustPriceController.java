package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.service.impl.ChemicalAdjustPriceService;
import com.qf.system.vo.AdjustPriceVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.controller
 * @ClassName : ChemicalAdjustPriceController.java
 * @createTime : 2022/3/30 18:36
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */
@RestController
public class ChemicalAdjustPriceController {

    @Resource
    ChemicalAdjustPriceService chemicalAdjustPriceService;

    @PutMapping("/chemical/price/adjust/{currentPrice}/{remarks}/{chemicalId}")
    ResponseResult adjustPrice(@PathVariable BigDecimal currentPrice, @PathVariable String remarks, @PathVariable Integer chemicalId) {
        return ResponseResult.success(ResultCode.SUCCESS, chemicalAdjustPriceService.adjustPrice(currentPrice, remarks, chemicalId));
    }

    @GetMapping("/chemical/price/adjust/details/paging")
    ResponseResult adjustPaging(@RequestBody AdjustPriceVo adjustPriceVo) {
        return ResponseResult.success(ResultCode.SUCCESS,chemicalAdjustPriceService.adjustPaging(
                adjustPriceVo.getPage(),adjustPriceVo.getSize(),adjustPriceVo.getType(),adjustPriceVo.getParam1(),adjustPriceVo.getParam2(),adjustPriceVo.getParam3()
        ));
    }
}
