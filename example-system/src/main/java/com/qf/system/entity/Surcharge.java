package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "lhr_surcharge")
public class Surcharge {
    public static final String COL_ID = "id";
    /**
     * id
     */
    @TableId(value = "surcharge_id", type = IdType.INPUT)
    private Integer surchargeId;

    /**
     * 附加费名称
     */
    @TableField(value = "surcharge")
    private String surcharge;

    /**
     * 处方类型
     */
    @TableField(value = "prescription_type")
    private String prescriptionType;

    /**
     * 成本价
     */
    @TableField(value = "cost_price")
    private Double costPrice;

    /**
     * 金额
     */
    @TableField(value = "momey")
    private Double momey;

    /**
     * 创建时间
     */
    @TableField(value = "establish")
    private String establish;

    /**
     * 创建人
     */
    @TableField(value = "`member`")
    private Long member;

    /**
     * 折扣 - 1：是；2：否
     */
    @TableField(value = "discount")
    private Integer discount;

    /**
     * 状态 - 1：启用；2：禁用
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 删除
     */
    @TableField(value = "is_del")
    private Integer isDel;

    public static final String COL_SURCHARGE_ID = "surcharge_id";

    public static final String COL_SURCHARGE = "surcharge";

    public static final String COL_PRESCRIPTION_TYPE = "prescription_type";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_MOMEY = "momey";

    public static final String COL_ESTABLISH = "establish";

    public static final String COL_MEMBER = "member";

    public static final String COL_DISCOUNT = "discount";

    public static final String COL_STATUS = "status";

    public static final String COL_IS_DEL = "is_del";
}