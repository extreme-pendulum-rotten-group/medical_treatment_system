package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "lhr_position")
public class Position {
    /**
     * id
     */
    @TableId(value = "position_id", type = IdType.INPUT)
    private Integer positionId;

    /**
     * 职位名称
     */
    @TableField(value = "position_name")
    private String positionName;

    /**
     * 说明
     */
    @TableField(value = "brief_introduction")
    private String briefIntroduction;

    /**
     * 删除
     */
    @TableField(value = "`is_del`")
    private Integer isDel;

    public static final String COL_POSITION_ID = "position_id";

    public static final String COL_POSITION_NAME = "position_name";

    public static final String COL_BRIEF_INTRODUCTION = "brief_introduction";

    public static final String IS_DEL = "is_del";
}