package com.qf.system.reqeust;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

@Data
public class PrescriptionSurchargeQo {

    private Long id;


    private Integer prescriptionId;


    private List<Integer> surchargeIdList;


}