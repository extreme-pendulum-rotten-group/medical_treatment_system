package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.entity.Dossier;
import com.qf.system.reqeust.DossierQo;

import java.util.Date;

public interface DictionaryService {
            //=========主诉信息===========
    //展示
    ResponseResult selectMainSuit(DossierQo dossierQo);
    //伪删除
    ResponseResult deleteMainSuit(Long id);
    //添加
    ResponseResult addMainSuit(Dossier dossier);
    //模糊查询
    ResponseResult selectappeal(Dossier dossier);

    //=========现病史===========
    //展示
    ResponseResult selectPresents(DossierQo dossierQo);
    //伪删除
    ResponseResult deletePresent(Long id);
    //添加
    ResponseResult addPresent(Dossier dossier);
    //模糊查询
    ResponseResult selectPresent(Dossier dossier);


}
