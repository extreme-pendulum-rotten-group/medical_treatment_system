package com.qf.system.vo;

import com.qf.system.entity.Patient;
import lombok.Data;

import java.util.List;

@Data
public class RegistrationDetailsVo {
    /**
     * 科室名
     */
    private String departmentName;

    /**
     * 接诊类型 1-出诊 2-复诊
     */
    private Integer clinicalType;

    /**
     * 医生姓名
     */
    private String username;

    /**
     * 挂号费用 连接附加费用表主键
     */
    private double registrationCost;

    /**
     * 诊疗费用 连接附加费用表
     */
    private double consultationFee;

    /**
     * 患者信息，连接患者表主键
     */
    private List<Patient> patients;
}
