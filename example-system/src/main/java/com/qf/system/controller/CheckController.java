package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.CheckParam;
import com.qf.system.service.CheckService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/check")
public class CheckController {
    @Resource
    CheckService checkService;

    /**
     * 1.查询和展示
     * 梁浩然
     * @param param
     * @return
     */
    @GetMapping("/view/check")
    public ResponseResult selectCheck (CheckParam param){
        return checkService.selectCheck(param);
    }

    /**
     * 2.删除
     * 梁浩然
     * @param array
     * @return
     */
    @PostMapping("/delete/list")
    public ResponseResult deleteList (@RequestParam(value="array")Integer[] array){
        List<Integer> list = Arrays.asList(array);
        return checkService.deleteByList(list);
    }

    /**
     * 3.编辑和修改
     * 梁浩然
     * @param param
     * @return
     */
    @PostMapping("/update/id")
    public ResponseResult updateId (CheckParam param) {
        return checkService.updateById(param);
    }

    /**
     * 4.新增
     * 梁浩然
     * @param param
     * @return
     */
    @PostMapping("/add/check")
    public ResponseResult addCheck (CheckParam param) {
        return checkService.addCheck(param);
    }
}
