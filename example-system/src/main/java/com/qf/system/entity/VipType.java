package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 会员类型表
 */
@Data
@TableName(value = "hhf_vip_type")
public class VipType {
    /**
     * 主键
     */
    @TableId(value = "vip_type_id", type = IdType.INPUT)
    private Integer vipTypeId;

    /**
     * 会员名称
     */
    @TableField(value = "vip_type_name")
    private String vipTypeName;

    /**
     * 会员等级
     */
    @TableField(value = "vip_class")
    private String vipClass;

    /**
     * 折扣
     */
    @TableField(value = "discount")
    private String discount;

    /**
     * 最低积分
     */
    @TableField(value = "min_points")
    private Integer minPoints;

    /**
     * 升级所需积分
     */
    @TableField(value = "upgrade_points")
    private Integer upgradePoints;

    /**
     * 备注
     */
    @TableField(value = "remarks")
    private String remarks;

    /**
     * 会员类型状态：1启用，2禁用
     */
    @TableField(value = "vip_type_status")
    private Integer vipTypeStatus;

    /**
     * 是否删除：1已删除，0未删除
     */
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    public static final String COL_VIP_TYPE_ID = "vip_type_id";

    public static final String COL_VIP_TYPE_NAME = "vip_type_name";

    public static final String COL_VIP_CLASS = "vip_class";

    public static final String COL_DISCOUNT = "discount";

    public static final String COL_MIN_POINTS = "min_points";

    public static final String COL_UPGRADE_POINTS = "upgrade_points";

    public static final String COL_REMARKS = "remarks";

    public static final String COL_VIP_TYPE_STATUS = "vip_type_status";

    public static final String COL_IS_DELETED = "is_deleted";
}