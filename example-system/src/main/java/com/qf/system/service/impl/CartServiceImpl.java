package com.qf.system.service.impl;

import com.qf.system.entity.Cart;
import com.qf.system.mapper.CartMapper;
import com.qf.system.mapper.ZJWChemicalMapper;
import com.qf.system.reqeust.ChemicalOnReceptionQo;
import com.qf.system.service.CartService;
import com.qf.system.vo.ChemicalOnReceptionVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ZJW
 * @version 1.0.0
 * @Description TODO
 * @ClassName CartServiceImpl.java
 * @createTime 2022年04月01日 20:41:00
 */
@Service
public class CartServiceImpl implements CartService {

    @Resource
    CartMapper cartMapper;

    @Resource
    ZJWChemicalMapper zjwChemicalMapper;


    /**
     * 插入订单信息
     * @param cart
     * @return
     */
    @Override
    public Integer insertCart(Cart cart) {
        Integer add = cartMapper.insertCart(cart);
        if (add > 0) {
         return add;
        }
        return -1;
    }

    /**
     * 更新订单一种药品的总数
     * @param cartId
     * @param num
     * @return
     */
    @Override
    public Integer updateNumByCid(@Param("cartId") Integer cartId,@Param("num") Integer num) {
        Integer update = cartMapper.updateNumByCid(cartId,num);
        if (update > 0) {
            return update;
        }
        return -1;
    }

    /**
     * 查找出一种药品的全部信息
     * @param orderNumber
     * @param chemicalId
     * @return
     */
    @Override
    public Cart findByOrderNumberAndChemicalId(Integer orderNumber, Integer chemicalId) {
        return cartMapper.findByOrderNumberAndChemicalId(orderNumber,chemicalId);
    }

    /**
     * 生成订单信息
     * @param orderNumber
     * @param chemicalId
     * @param num
     */
    @Override
    public void addToCart(Integer orderNumber, Integer chemicalId, Integer num) {
        //调用上面方法接口查询购物车中是否已经添加这个药品
        Cart cart = findByOrderNumberAndChemicalId(orderNumber,chemicalId);
        //创建输入的药品Qo对象
        ChemicalOnReceptionQo qo = new ChemicalOnReceptionQo();
        //将查到的药品id传入qo对象
        qo.setId(chemicalId);
            //cart为空表示购物车里面没有这个药品
        if (ObjectUtils.isEmpty(cart)){
            //调用接口查询某个药品全部信息
            ChemicalOnReceptionVo cvo = zjwChemicalMapper.selectChemicalById(qo);
            //创建新的购物车信息，并赋值
            Cart cart1 = new Cart();
            cart1.setOrderNumber(orderNumber);
            cart1.setChemicalId(chemicalId);
            cart1.setNum(num);
            cart1.setPrice(cvo.getRetailPrice());
            insertCart(cart1);
        }else {
            Integer cartId = cart.getCartId();
            //如果购物车中已经包含这个药品信息，就更新药品数量
            Integer num1 = cart.getNum()+1;
            //进行更新数量接口操作
            updateNumByCid(cartId,num1);
        }
    }
}
