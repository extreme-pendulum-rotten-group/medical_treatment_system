package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.CostParam;
import com.qf.system.service.CostService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/cost")
public class CostController {

    @Resource
    CostService costService;

    /**
     *  通过费用类型查找
     *  通过模糊搜索来查找
     *  梁浩然
     * @return
     */
    @GetMapping("/view/cost")
    public ResponseResult selectCost(CostParam param){
        return costService.selectCost(param);
    }

    /**
     * 批量删除
     * 梁浩然
     * @param array
     * @return
     */
    @PostMapping("/delete/list")
    public ResponseResult deleteList(@RequestParam(value = "array") Integer[] array){
        List<Integer> list = Arrays.asList(array);
        return costService.deleteList(list);
    }

    /**
     * 编辑修改
     * @param param
     * @return
     */
    @PostMapping("/update/id")
    public ResponseResult updateId (CostParam param){
        return costService.updateById(param);
    }

    /**
     * 新增费用
     * @param param
     * @return
     */
    @PostMapping("/add/cost")
    public ResponseResult addCost (CostParam param){
        return costService.addCost(param);
    }

}
