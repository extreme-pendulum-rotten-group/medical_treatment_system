package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "lhr_clinic")
public class Clinic {
    /**
     * 诊所id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 诊所名称
     */
    @TableField(value = "clinic_name")
    private String clinicName;

    /**
     * 地址
     */
    @TableField(value = "`location`")
    private String location;

    /**
     * 状态
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 删除
     */
    @TableField(value = "is_del")
    private Integer isDel;

    public static final String COL_ID = "id";

    public static final String COL_CLINIC_NAME = "clinic_name";

    public static final String COL_LOCATION = "location";

    public static final String COL_STATUS = "status";

    public static final String COL_IS_DEL = "is_del";
}