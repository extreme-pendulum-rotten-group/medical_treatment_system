package com.qf.system.service.impl;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.mapper.SurchargeMapper;
import com.qf.system.reqeust.SurchargeParam;
import com.qf.system.service.SurchargeService;
import com.qf.system.vo.SurchargeVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class SurchargeServiceImpl implements SurchargeService {

    @Resource
    SurchargeMapper surchargeMapper;

    /**
     * 查找和展示
     * @param param
     * @return
     */
    @Override
    public ResponseResult selectSurcharge(SurchargeParam param) {
        List<SurchargeVo> surchargeVos = surchargeMapper.selectSurcharge(param);
        if (surchargeVos.size() == 0){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,surchargeVos);
    }

    /**
     * 删除
     * @param list
     * @return
     */
    @Override
    public ResponseResult deleteByList(List<Integer> list) {
        Integer number = surchargeMapper.deleteByList(list);
        if (number == null){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 编辑与修改
     * @param param
     * @return
     */
    @Override
    public ResponseResult changeById(SurchargeParam param) {
        Integer number = surchargeMapper.insert(param);
        if (number == null){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 新增
     * @param param
     * @return
     */
    @Override
    public ResponseResult addSurcharge(SurchargeParam param) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //获取当前时间String类型
        String time = sdf.format(date);
        param.setEstablish(time);
        Integer number = surchargeMapper.addCheck(param);
        if (number == null){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }
}
