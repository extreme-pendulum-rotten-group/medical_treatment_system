package com.qf.system.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qf.system.entity.Dossier;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
    * 患者表
 * @author 78458
 */
@Data
public class PatientOnReceptionVo {

    private Long patientId;
    private String patientName;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date birthday;
    private String cardNumber;
    private String phone;
    private Integer vipTypeId;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date createTime;
    private Integer registrationStatus;

    private String departmentName;
    private String  decl;





}