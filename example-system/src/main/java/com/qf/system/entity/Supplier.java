package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "lhr_supplier")
public class Supplier {
    public static final String IS_DEL = "is_del";
    /**
     * 供应商ID
     */
    @TableId(value = "supplier_id", type = IdType.INPUT)
    private Integer supplierId;

    /**
     * 供应商名称
     */
    @TableField(value = "supplier_name")
    private String supplierName;

    /**
     * 联系人名称
     */
    @TableField(value = "linkman")
    private String linkman;

    /**
     * 联系人电话
     */
    @TableField(value = "linkman_phone")
    private Long linkmanPhone;

    /**
     * 创建人
     */
    @TableField(value = "`member`")
    private Integer member;

    /**
     * 创建时间
     */
    @TableField(value = "establish")
    private String establish;

    /**
     * 备注
     */
    @TableField(value = "relation")
    private String relation;

    /**
     * 状态(1是启用，0是禁用)
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 删除
     */
    @TableField(value = "is_del")
    private Integer isDel;

    public static final String COL_SUPPLIER_ID = "supplier_id";

    public static final String COL_SUPPLIER_NAME = "supplier_name";

    public static final String COL_LINKMAN = "linkman";

    public static final String COL_LINKMAN_PHONE = "linkman_phone";

    public static final String COL_MEMBER = "member";

    public static final String COL_ESTABLISH = "establish";

    public static final String COL_RELATION = "relation";

    public static final String COL_STATUS = "status";

    public static final String COL_IS_DEL = "is_del";
}