package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "lhr_department")
public class Department {
    /**
     * 科室id
     */
    @TableId(value = "department_id", type = IdType.INPUT)
    private Integer departmentId;

    /**
     * 科室名
     */
    @TableField(value = "department_name")
    private String departmentName;

    /**
     * 状态 - 1：启用；2：禁用
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 说明
     */
    @TableField(value = "brief_introduction")
    private String briefIntroduction;

    /**
     * 删除
     */
    @TableField(value = "`is_del`")
    private Integer isDel;

    public static final String COL_DEPARTMENT_ID = "department_id";

    public static final String COL_DEPARTMENT_NAME = "department_name";

    public static final String COL_STATUS = "status";

    public static final String COL_BRIEF_INTRODUCTION = "brief_introduction";

    public static final String IS_DEL = "is_del";
}