package com.qf.system.qo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class DrugRetailCostQo {

    /**
     * 订单号
     */
    private String orderNumber;

    /**
     * 零售（处方）（挂号）单号
     */
    private String retailNumber;

    /**
     * 患者id
     */
    private Integer patientId;

    /**
     * 科室id
     */
    private Integer departmentId;

    /**
     * 医生id
     */
    private Integer memberId;

    /**
     * 挂号id
     */
    private Integer registrationId;

    /**
     * 订单总额
     */
    private Double orderMeney;


    private DrugRetailQo drugRetailQo;
}
