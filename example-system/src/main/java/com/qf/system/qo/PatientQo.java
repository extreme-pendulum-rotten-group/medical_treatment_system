package com.qf.system.qo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.Date;

@Data
public class PatientQo {
    /**
     * 患者姓名
     */
    private String patientName;

    /**
     * 患者卡号
     */
    private String cardNumber;

    /**
     * 出生日期
     */
    private Date birthday;
    /**
     * 性别
     */
    private Integer gender;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 证件号码
     */
    private String idNumber;

    /**
     * 患者来源id
     */
    private Integer patientSourceId;
    /**
     * 会员类型id
     */
    private Integer vipTypeId;

    /**
     * 到期时间
     */
    private Date expirationTime;

    /**
     * 民族
     */
    private String nation;

    /**
     * 婚姻状况：1未婚，2已婚，3保密
     */
    private Integer maritalStatus;

    /**
     * 地址
     */
    private String address;

    /**
     * 患者学历id
     */
    private Integer patientEducationBackgroundId;

    /**
     * 患者职业id
     */
    private Integer patientOccupationId;

    /**
     * 工作单位
     */
    private String workUnit;

    /**
     * 备注
     */
    private String remarks;
}
