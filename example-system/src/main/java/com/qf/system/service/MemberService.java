package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.reqeust.MemberParam;

import java.util.List;

public interface MemberService {
    ResponseResult login (MemberParam param);

    ResponseResult changePassword(MemberParam param);

    ResponseResult selectPatient();

    ResponseResult selectMember(MemberParam param);

    ResponseResult deleteList(List<Integer> list);

    ResponseResult changeMember(MemberParam param);

    ResponseResult addMember(MemberParam param);
}
