package com.qf.system.qo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DrugRetailChemicalQo {
    /**
     * 药品编码
     */
    private String str;

    /**
     * 药品类型
     */
    private String type;

}
