package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.CheckProject;
import com.qf.system.reqeust.CheckProjectOnPrescriptionQo;
import com.qf.system.vo.CheckProjectOnPrescriptionVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ZJWCheckProjectMapper extends BaseMapper<CheckProject> {
    List<CheckProjectOnPrescriptionVo> selectAllCheckProjectOnPrescription();

    List<CheckProjectOnPrescriptionVo> selectCheckProjectOnPrescriptionBySomething(CheckProjectOnPrescriptionQo checkProjectOnPrescriptionQo);
}