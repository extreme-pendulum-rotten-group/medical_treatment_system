package com.qf.system.service.impl;

import com.qf.system.entity.PrescriptionChemical;
import com.qf.system.mapper.PrescriptionChemicalMapper;
import com.qf.system.mapper.ZJWChemicalMapper;
import com.qf.system.reqeust.PrescriptionChemicalQo;
import com.qf.system.service.PrescriptionChemicalService;
import com.qf.system.service.ZJWChemicalService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;

/**
 * @author ZJW
 * @version 1.0.0
 * @Description TODO
 * @ClassName PrescriptionChemicalServiceImpl.java
 * @createTime 2022年03月31日 10:53:00
 */

@Service
public class PrescriptionChemicalServiceImpl implements PrescriptionChemicalService {

    @Resource
    PrescriptionChemicalMapper prescriptionChemicalMapper;

    @Resource
    ZJWChemicalMapper zjwChemicalMapper;




    @Override
    public Integer insertPrescription(PrescriptionChemicalQo pcq) {
        //count作为返回给前端展示的数据成功的次数
        int count = 0;
        //插入数据只需要插入处方id和药品id就可以了
        PrescriptionChemical pc = new PrescriptionChemical();
        pc.setPrescriptionId(pcq.getPrescriptionId());
        //循环插入每条处方所拥有的药品数
        for (int i = 0; i <pcq.getChemicalIdList().size() ; i++) {
            pc.setChemicalId(pcq.getChemicalIdList().get(i));
          int j = prescriptionChemicalMapper.insertPrescription(pc);
            if (j>0){
            count ++;
            }
        }

        return count;
    }


//    @Override
//    public Integer showPrescription(Prescription prescription) {
//        PrescriptionChemical pc = new PrescriptionChemical();
//        return null;
//    }
}

/*for (int i = 0; i <pcq.getChemicalIdList().size(); i++) {
            int num = prescriptionChemicalMapper.selectCountById(pcq.getChemicalIdList().get(i));
        }
        Prescription p = new Prescription();
        p.setId(pcq.getPrescriptionId());*/