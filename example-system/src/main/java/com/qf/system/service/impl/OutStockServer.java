package com.qf.system.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.OutStock;
import com.qf.system.mapper.OutStockMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.service.impl
 * @ClassName : InStockServer.java
 * @createTime : 2022/3/29 11:15
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */
@Service
public class OutStockServer {

    @Resource
    OutStockMapper outStockMapper;


    public String addOutStock(OutStock outStock) {
        Integer result = outStockMapper.addStock(outStock);
        if (result != 1) {
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return "新增入库成功";
    }

    public String updateOutStock(OutStock outStock) {
        Integer result = outStockMapper.updateStock(outStock);
        if (result != 1) {
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return "编辑入库信息成功";
    }

    public String outReviewEnable(Integer outEnable, String outNumber) {
        Integer result = outStockMapper.reviewEnable(outEnable, outNumber);
        if (result == 0) {
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return "入库审核通过";
    }

    public List<OutStock> outPaging(Integer page, Integer size, Boolean enable, String type, String number, String supplier) {
        List<OutStock> outStocks = outStockMapper.outPaging((page - 1) * size, size, enable, type, number, supplier);
        if (outStocks == null) {
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return outStocks;
    }

    public String deleteOutById(Integer id) {
        Integer result = outStockMapper.deleteOutById(id);
        if (result!=1){
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return "删除成功";
    }
}
