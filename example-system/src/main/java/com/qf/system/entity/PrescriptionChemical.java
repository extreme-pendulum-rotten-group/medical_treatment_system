package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "zjw_prescription_chemical")
public class PrescriptionChemical {
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 处方id
     */
    @TableField(value = "prescription_id")
    private Long prescriptionId;

    /**
     * 药品id
     */
    @TableField(value = "chemical_id")
    private Long chemicalId;

    public static final String COL_ID = "id";

    public static final String COL_PRESCRIPTION_ID = "prescription_id";

    public static final String COL_CHEMICAL_ID = "chemical_id";
}