package com.qf.system.service;

import com.qf.system.entity.VipType;

import java.util.List;

/**
 * @author hhf
 */
public interface VipTypeService {
    Integer addVipType(VipType vipType);

    Integer updateVipType(VipType vipType);

    List<VipType> selectVipType();

    String selectVipTypeNameById(Integer vipTypeId);
}
