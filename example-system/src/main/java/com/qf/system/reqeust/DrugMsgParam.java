package com.qf.system.reqeust;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.system.entity.DrugType;
import lombok.Data;

import java.util.Date;

@Data
public class DrugMsgParam {
    /**
     * 剂型id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 剂型名称
     */
    @TableField(value = "dosage_name")
    private String dosageName;

    /**
     * 处方分类
     */
    @TableField(value = "prescription_type")
    private Integer prescriptionType;

    /**
     * 药品分类名称
     */
    @TableField(value = "drug_type_name")
    private String drugTypeName;

    /**
     * 用法名称
     */
    @TableField(value = "usage_name")
    private String usageName;

    /**
     * 项目名称ID
     */
    @TableField(value = "project_name")
    private Integer projectName;

    /**
     * 厂家名称
     */
    @TableField(value = "manufacturer_name")
    private String manufacturerName;

    /**
     * 单位名称
     */
    @TableField(value = "units_name")
    private String unitsName;

    /**
     * 类型
     */
    @TableField(value = "type")
    private String type;

    /**
     * 出库类型
     */
    @TableField(value = "warehouse_name")
    private String warehouseName;

    /**
     * 创建时间
     */
    @TableField(value = "creation_time")
    private Date creationTime;

    /**
     * 创建人ID
     */
    @TableField(value = "`member`")
    private Long member;

    /**
     * 删除
     */
    @TableField(value = "is_del")
    private Integer isDel;
}
