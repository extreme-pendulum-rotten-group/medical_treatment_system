package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Chemical;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.mapper
 * @ClassName : ChemicalMapper.java
 * @createTime : 2022/3/28 19:59
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */
@Mapper
public interface ChemicalMapper extends BaseMapper<Chemical> {

    Integer addChemical(@Param("chemical") Chemical chemical);

    List<Chemical> chemicalInStockView();

    List<Chemical> inReviewEnableViews();

    List<Chemical> inReviewUnEnableViews();

    List<Chemical> outReviewEnableViews();

    List<Chemical> outReviewUnEnableViews();

    Chemical getChemicalById(@Param("chemicalId") Integer chemicalId);

    Chemical chemicalPaging(@Param("info") int info, @Param("size") Integer size, @Param("type") String param1,@Param("enable") Boolean param2,@Param("createTime") Date date, @Param("name") String param3, @Param("number") String param4, @Param("producer") String param5);

    Integer deleteChemicalById(Integer id);
}