package com.qf.system.reqeust;

import com.qf.system.entity.Cost;
import lombok.Data;

@Data
public class CostParam extends Cost {
    private String dim;
    private String time;
}
