package com.qf.system.mapper;

import com.qf.system.vo.RoleVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface RolePermissionMapper {
    List<RoleVo> selectRolesById(@Param("uid") Long uid);
}
