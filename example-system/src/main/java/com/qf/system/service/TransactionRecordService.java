package com.qf.system.service;

import com.qf.system.entity.TransactionRecord;
import com.qf.system.reqeust.VipRequestParam;

import java.util.List;

/**
 * @author hhf
 */
public interface TransactionRecordService {
    List<TransactionRecord> selectTransactionRecord(VipRequestParam vipRequestParam);

    int addAddDepositRecord(VipRequestParam vipRequestParam);

    int addReduceDepositRecord(VipRequestParam vipRequestParam);
}
