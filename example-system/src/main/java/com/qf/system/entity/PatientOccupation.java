package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

/**
 * 患者职业表
 */
@Data
@TableName(value = "hhf_patient_occupation")
public class PatientOccupation {
    /**
     * 主键
     */
    @TableId(value = "patient_occupation_id", type = IdType.INPUT)
    private Integer patientOccupationId;

    /**
     * 患者职业名称
     */
    @TableField(value = "patient_occupation_name")
    private String patientOccupationName;

    /**
     * 创建人
     */
    @TableField(value = "creator")
    private String creator;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 是否删除：1已删除，0未删除
     */
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    public static final String COL_PATIENT_OCCUPATION_ID = "patient_occupation_id";

    public static final String COL_PATIENT_OCCUPATION_NAME = "patient_occupation_name";

    public static final String COL_CREATOR = "creator";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_IS_DELETED = "is_deleted";
}