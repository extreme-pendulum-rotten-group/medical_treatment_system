package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Cost;
import com.qf.system.reqeust.CostParam;import com.qf.system.vo.CostVo;import org.apache.ibatis.annotations.Mapper;import java.util.List;

@Mapper
public interface CostMapper extends BaseMapper<Cost> {
    /**
     * 模糊搜索
     *
     * @param param
     * @return
     */
    List<CostVo> selectCost(CostParam param);

    /**
     * 批量删除
     *
     * @param list
     * @return
     */
    Integer deleteList(List<Integer> list);

    /**
     * 新增费用
     *
     * @param param
     * @return
     */
    Integer addCost(CostParam param);
}