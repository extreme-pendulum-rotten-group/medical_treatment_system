package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.entity.Dossier;
import com.qf.system.reqeust.DossierAndPhysiqueQo;
import com.qf.system.reqeust.DossierQo;
import com.qf.system.vo.DossierAndPhysiqueVo;
import com.qf.system.vo.DossierVo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DossierService {
    DossierVo addDossier(DossierQo dossierQo) throws Exception;


    List<DossierVo> selectDossierByCardNumber(String cardNumber) throws Exception;

    ResponseResult updateDossierById(Dossier dossier);

    DossierAndPhysiqueVo selectDossierAndPhysique(DossierAndPhysiqueQo dossierAndPhysiqueQo);
}
