package com.qf.system.vo;

import com.qf.system.entity.Surcharge;
import lombok.Data;

@Data
public class SurchargeVo extends Surcharge {
    private String username;
}
