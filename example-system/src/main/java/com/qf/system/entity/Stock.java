package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * @Project      : medical_treatment_system
 * @Package      : com.qf.system.entity
 * @ClassName    : NfStock.java
 * @createTime   : 2022/3/31 16:58
 * @version      : 1.0
 * @author       : 菜可夫斯基
 * @Email        : 3293477562@qq.com
 * @公众号        : 菜可夫斯基
 * @CSDN         : weixin_44106059
 * @Description  : 
 */

@Data
@TableName(value = "nf_stock")
public class Stock {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 库存
     */
    @TableField(value = "stock_sum")
    private Long stockSum;

    /**
     * 采购金额
     */
    @TableField(value = "purchase_amount")
    private Long purchaseAmount;

    /**
     * 零售金额
     */
    @TableField(value = "retail_amount")
    private Long retailAmount;

    /**
     * 库存预警
     */
    @TableField(value = "stock_warning")
    private Long stockWarning;

    /**
     * 有效期预警
     */
    @TableField(value = "validity_warning")
    private Date validityWarning;

    /**
     * 药品ID
     */
    @TableField(value = "chemical_id")
    private Long chemicalId;

    private Chemical chemical;

    public static final String COL_ID = "id";

    public static final String COL_STOCK_SUM = "stock_sum";

    public static final String COL_PURCHASE_AMOUNT = "purchase_amount";

    public static final String COL_RETAIL_AMOUNT = "retail_amount";

    public static final String COL_STOCK_WARNING = "stock_warning";

    public static final String COL_VALIDITY_WARNING = "validity_warning";

    public static final String COL_CHEMICAL_ID = "chemical_id";
}