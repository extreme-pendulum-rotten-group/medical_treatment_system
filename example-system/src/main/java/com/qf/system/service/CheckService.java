package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.CheckParam;

import java.util.List;

public interface CheckService {
    ResponseResult selectCheck(CheckParam param);

    ResponseResult deleteByList(List<Integer> list);

    ResponseResult updateById(CheckParam param);

    ResponseResult addCheck(CheckParam param);
}
