package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.SurchargeParam;
import com.qf.system.service.SurchargeService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/surcharge")
public class SurchargeController {

    @Resource
    SurchargeService surchargeService;

    /**
     * 查找与展示
     * 梁浩然
     * @param param
     * @return
     */
    @GetMapping("/view/surcharge")
    public ResponseResult selectSurcharge(SurchargeParam param){
        return surchargeService.selectSurcharge(param);
    }

    /**
     * 删除
     * lhr
     * @param array
     * @return
     */
    @PostMapping("/delete/list")
    public ResponseResult deleteByList (@RequestParam(value="array")Integer[] array){
        List<Integer> list = Arrays.asList(array);
        return  surchargeService.deleteByList(list);
    }

    /**
     * 编辑和修改
     * 梁浩然
     * @param param
     * @return
     */
    @PostMapping("/change/id")
    public ResponseResult changeId (SurchargeParam param){
        return surchargeService.changeById(param);
    }

    /**
     * 新增
     * 梁浩然
     * @param param
     * @return
     */
    @PostMapping("/add/supplier")
    public ResponseResult addSupplier (SurchargeParam param){
        return surchargeService.addSurcharge(param);
    }
}
