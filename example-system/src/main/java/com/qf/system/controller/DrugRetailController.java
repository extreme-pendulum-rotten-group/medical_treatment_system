package com.qf.system.controller;


import com.qf.common.base.result.ResponseResult;
import com.qf.system.qo.DrugRetailChemicalQo;
import com.qf.system.qo.DrugRetailCostQo;
import com.qf.system.qo.DrugRetailQo;
import com.qf.system.service.DrugRetailService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController("/retail")
public class DrugRetailController {
    @Resource
    DrugRetailService drugRetailService;

    @PostMapping("/insert")
    public ResponseResult insert(DrugRetailQo drugRetailQo) {
        return ResponseResult.success(drugRetailService.insert(drugRetailQo));
    }

    @GetMapping("/find/chemical/name")
    public ResponseResult findChemicalName() {
        return ResponseResult.success(drugRetailService.findChemicalName());
    }

    @PostMapping("/find/patient")
    public ResponseResult findPatient(String str) {
        return ResponseResult.success(drugRetailService.findPatient(str));
    }

    @GetMapping("/chemical")
    public ResponseResult findChemica(DrugRetailChemicalQo drugRetailChemicalQo) {
        return ResponseResult.success(drugRetailService.findChemical(drugRetailChemicalQo));
    }

    @GetMapping("/find/surcharge")
    public ResponseResult findSurcharge() {
        return ResponseResult.success(drugRetailService.findSurcharge());
    }

    @PostMapping("/add/order")
    public ResponseResult addOrder(DrugRetailCostQo drugRetailCostQo) {
        return null;
    }
}
