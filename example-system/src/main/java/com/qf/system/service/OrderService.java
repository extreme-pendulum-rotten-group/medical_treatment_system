package com.qf.system.service;

import com.qf.system.mapper.OrderMapper;
import com.qf.system.qo.DrugRetailCostQo;
import com.qf.system.qo.OrderHomeQo;
import com.qf.system.qo.OrderQo;
import com.qf.system.qo.OrderSurchargeQo;
import com.qf.system.vo.OrderDetailVo;
import com.qf.system.vo.OrderVo;
import org.apache.ibatis.annotations.Param;

import javax.annotation.Resource;
import java.util.List;

public interface OrderService {

    Integer addOrder(DrugRetailCostQo drugRetailCostQo);

    List<OrderVo> findOrderHome(OrderHomeQo orderHomeQo);

    Integer update(String orderNumber ,Integer onset,Integer end);

    Integer deleteOrder(String orderNumber);

    OrderDetailVo selectOrderDetail(String orderNumber);

}
