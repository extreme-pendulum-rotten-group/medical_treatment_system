package com.qf.system.service;

import com.qf.system.entity.Cart;
import org.springframework.stereotype.Service;

@Service
public interface CartService {

    Integer insertCart(Cart cart);

    Integer updateNumByCid(Integer cartId,Integer num);

    Cart findByOrderNumberAndChemicalId(Integer orderNumber, Integer chemicalId);

    void addToCart(Integer orderNumber,Integer chemicalId, Integer num);

}
