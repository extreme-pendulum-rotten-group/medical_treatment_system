package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.VipTypeChangeRecord;

public interface VipTypeChangeRecordMapper extends BaseMapper<VipTypeChangeRecord> {
}