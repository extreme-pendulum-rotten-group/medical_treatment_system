package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.PrescriptionChemical;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PrescriptionChemicalMapper extends BaseMapper<PrescriptionChemical> {
    int insertPrescription(PrescriptionChemical pc);
}