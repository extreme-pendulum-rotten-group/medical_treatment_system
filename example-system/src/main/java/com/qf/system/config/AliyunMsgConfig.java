package com.qf.system.config;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.profile.DefaultProfile;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Configuration
public class AliyunMsgConfig {

    @Resource
    MsgProperites msgProperites;

    @Bean
    public IAcsClient iAcsClient(){
        /**
         * 获得 regionid ， access-key-id 和 access-key-secret
         */
        DefaultProfile profile = DefaultProfile.getProfile(msgProperites.getRegionId(), msgProperites.getAccessKeyId(), msgProperites.getAccessKeySecret());
        DefaultAcsClient client = new DefaultAcsClient(profile);
        return client;
    }
}
