package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.SurchargeParam;

import java.util.List;

public interface SurchargeService {

    ResponseResult selectSurcharge(SurchargeParam param);

    ResponseResult deleteByList(List<Integer> list);

    ResponseResult changeById(SurchargeParam param);

    ResponseResult addSurcharge(SurchargeParam param);
}
