package com.qf.system.reqeust;

import com.qf.system.entity.Vip;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author: hhf
 */
@Data
public class VipRequestParam extends Vip {
    private Date startTime;

    private Date endTime;

    @NotNull(groups = Deposit.class,message = "充值金额不能为空")
    private Double deposit;

    @NotNull(groups = Deposit.class,message = "赠送金额不能为空")
    private Double present;

    private Double changePoints;
}
