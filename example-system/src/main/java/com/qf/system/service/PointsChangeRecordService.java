package com.qf.system.service;

import com.qf.system.entity.PointsChangeRecord;
import com.qf.system.entity.TransactionRecord;
import com.qf.system.reqeust.VipRequestParam;

import java.util.List;

/**
 * @author hhf
 */
public interface PointsChangeRecordService {
    List<PointsChangeRecord> selectPointsChangeRecord(VipRequestParam vipRequestParam);

    int addDepositPointRecord(VipRequestParam vipRequestParam);

    int addAddPointRecord(VipRequestParam vipRequestParam);

    int addReducePointRecord(VipRequestParam vipRequestParam);

    int resetReducePointRecord(VipRequestParam vipRequestParam);
}
