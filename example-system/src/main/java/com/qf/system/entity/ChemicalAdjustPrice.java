package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.entity
 * @ClassName : ChemicalAdjustPrice.java
 * @createTime : 2022/3/30 18:19
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */

@Data
@TableName(value = "nf_chemical_adjust_price")
public class ChemicalAdjustPrice {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 当前零售价
     */
    @TableField(value = "current_retail_price")
    private BigDecimal currentRetailPrice;

    /**
     * 药品差价
     */
    @TableField(value = "difference_price")
    private Long differencePrice;

    /**
     * 比例
     */
    @TableField(value = "proportion")
    private Double proportion;

    /**
     * 备注
     */
    @TableField(value = "remarks")
    private String remarks;

    /**
     * 操作员
     */
    @TableField(value = "`operator`")
    private String operator;

    /**
     * 操作时间
     */
    @TableField(value = "operator_time")
    private Date operatorTime;

    @TableField(value = "chemical_id")
    private Integer chemicalId;

    @TableField(value = "operator_count")
    private Integer operatorCount;


    private Chemical chemical;

    public static final String COL_CHEMICAL_ID = "chemical_id";

    public static final String COL_OPERATOR_COUNT = "operator_count";

    public static final String COL_ID = "id";

    public static final String COL_CURRENT_RETAIL_PRICE = "current_retail_price";

    public static final String COL_DIFFERENCE_PRICE = "difference_price";

    public static final String COL_PROPORTION = "proportion";

    public static final String COL_REMARKS = "remarks";

    public static final String COL_OPERATOR = "operator";

    public static final String COL_OPERATOR_TIME = "operator_time";
}