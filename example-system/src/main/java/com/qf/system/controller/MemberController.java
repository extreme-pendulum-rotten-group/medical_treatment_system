package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.reqeust.MemberParam;
import com.qf.system.service.MemberService;
import com.qf.system.service.MsgService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping
public class MemberController {

    @Resource
    MemberService memberService;

    @Resource
    MsgService msgService;

    /**
     * 1.发送短信
     * 梁浩然
     * @param phone
     * @return
     */
    @PostMapping("/set/msg")
    public ResponseResult setMsg(String phone) {
        return msgService.sendMsg(phone);
    }

    /**
     * 2.登录
     * @param param
     * @return
     */
    @PostMapping("/login/phone")
    public ResponseResult login(MemberParam param){
        return memberService.login(param);
    }

    /**
     * 3.改密码
     * @param param
     * @return
     */
    @PostMapping("/change/password")
    public ResponseResult changePassword(MemberParam param) {
        return memberService.changePassword(param);
    }

    /**
     * 4.展示病人挂号记录
     * @return
     */
    @GetMapping("/view/patient")
    public ResponseResult selectPatient(){
        return memberService.selectPatient();
    }

    /**
     * 5.查询和展示
     * @param param
     * @return
     */
    @GetMapping("/view/member")
    public ResponseResult selectMember(MemberParam param){
        return memberService.selectMember(param);
    }

    /**
     * 6.删除
     * @param array
     * @return
     */
    @PostMapping("/delete/list")
    public ResponseResult deleteList(@RequestParam(value="array")Integer[] array) {
        List<Integer> list = Arrays.asList(array);
        return memberService.deleteList(list);
    }

    /**
     * 7.更改用户信息
     * @param param
     * @return
     */
    @PostMapping("/change/member")
    public ResponseResult changeMember(MemberParam param){
        return memberService.changeMember(param);
    }

    /**
     * 8.新增用户
     * @param param
     * @return
     */
    @PostMapping("/add/member")
    public ResponseResult addMember(MemberParam param){
        return memberService.addMember(param);
    }
}
