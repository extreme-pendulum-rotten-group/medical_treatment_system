package com.qf.system.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.Vip;
import com.qf.system.entity.VipType;
import com.qf.system.mapper.VipMapper;
import com.qf.system.mapper.VipTypeMapper;
import com.qf.system.reqeust.VipRequestParam;
import com.qf.system.service.PointsChangeRecordService;
import com.qf.system.service.TransactionRecordService;
import com.qf.system.service.VipService;
import com.qf.system.service.VipTypeChangeRecordService;
import com.qf.system.vo.VipVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: hhf
 */
@Service
@Transactional
public class VipServiceImpl implements VipService {
    private static final int UPGRADE_POINT_2 = 1000;
    private static final int UPGRADE_POINT_3 = 3000;
    private static final int UPGRADE_POINT_4 = 5000;
    private static final int UPGRADE_POINT_5 = 10000;
    private static final int UPGRADE_POINT_6 = 20000;

    @Resource
    VipMapper vipMapper;

    @Resource
    TransactionRecordService transactionRecordService;

    @Resource
    PointsChangeRecordService pointsChangeRecordService;

    @Resource
    VipTypeChangeRecordService vipTypeChangeRecordService;

    @Override
    public List<VipVo> selectVips() {
        return vipMapper.selectVips();
    }

    @Override
    public VipVo selectVipById(VipRequestParam vipRequestParam) {
        VipVo vip = vipMapper.selectVipById(vipRequestParam);
        if (vip == null) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return vip;
    }

    @Override
    public List<VipVo> searchVip(VipRequestParam vipRequestParam) {
        return vipMapper.searchVip(vipRequestParam);
    }

    @Override
    public Integer updateVip(Vip vip) {
        int count = vipMapper.updateById(vip);
        if (count < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return count;
    }

    @Override
    public Integer addDeposit(VipRequestParam vipRequestParam) {
        Double allDeposit = vipRequestParam.getAllDeposit();
        Double allPresents = vipRequestParam.getAllPresents();
        Integer oldVipTypeId = vipRequestParam.getVipTypeId();
        Double deposit = vipRequestParam.getDeposit();
        Double present = vipRequestParam.getPresent();
        Double balance = vipRequestParam.getBalance();
        Double vipPoints = vipRequestParam.getVipPoints();
        allDeposit = allDeposit + deposit;
        allPresents = allPresents + present;
        balance = balance + deposit + present;
        vipPoints = vipPoints + deposit;
        //升级机制
        if (vipPoints >= UPGRADE_POINT_6) {
            vipRequestParam.setVipTypeId(6);
        } else if (vipPoints >= UPGRADE_POINT_5) {
            vipRequestParam.setVipTypeId(5);
        } else if (vipPoints >= UPGRADE_POINT_4) {
            vipRequestParam.setVipTypeId(4);
        } else if (vipPoints >= UPGRADE_POINT_3) {
            vipRequestParam.setVipTypeId(3);
        } else if (vipPoints >= UPGRADE_POINT_2) {
            vipRequestParam.setVipTypeId(2);
        }
        Integer newVipTypeId = vipRequestParam.getVipTypeId();
        int count1 = 0;
        if (newVipTypeId > oldVipTypeId) {
            //新增会员等级升级记录
            count1 = vipTypeChangeRecordService.addUpgradeRecord(vipRequestParam);
            if (count1 < 1) {
                throw new ServiceException(ResultCode.ERROR);
            }
        }
        vipRequestParam.setAllDeposit(allDeposit);
        vipRequestParam.setAllPresents(allPresents);
        vipRequestParam.setBalance(balance);
        vipRequestParam.setVipPoints(vipPoints);
        //充值
        Integer count2 = vipMapper.updateById(vipRequestParam);
        if (count2 < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        //新增充值记录
        int count3 = transactionRecordService.addAddDepositRecord(vipRequestParam);
        //新增储值积分记录
        vipRequestParam.setChangePoints(deposit);
        int count4 = pointsChangeRecordService.addDepositPointRecord(vipRequestParam);
        return count1 + count2 + count3 + count4;
    }

    @Override
    public Integer reduceDeposit(VipRequestParam vipRequestParam) {
        Double deposit = vipRequestParam.getDeposit();
        Double balance = vipRequestParam.getBalance();
        balance = balance - deposit;
        vipRequestParam.setBalance(balance);
        int count1 = vipMapper.reduceDeposit(vipRequestParam);
        //新增退款记录
        int count2 = transactionRecordService.addReduceDepositRecord(vipRequestParam);
        return count1 + count2;
    }

    @Override
    public Integer addPoints(VipRequestParam vipRequestParam) {
        Double changePoints = vipRequestParam.getChangePoints();
        Double vipPoints = vipRequestParam.getVipPoints();
        vipPoints = vipPoints + changePoints;
        vipRequestParam.setVipPoints(vipPoints);
        int count1 = vipMapper.updateById(vipRequestParam);
        if (count1 < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        //新增积分增加记录
        int count2 = pointsChangeRecordService.addAddPointRecord(vipRequestParam);
        if (count2 < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return count1 + count2;
    }

    @Override
    public Integer reducePoints(VipRequestParam vipRequestParam) {
        Double changePoints = vipRequestParam.getChangePoints();
        Double vipPoints = vipRequestParam.getVipPoints();
        vipPoints = vipPoints - changePoints;
        vipRequestParam.setVipPoints(vipPoints);
        int count1 = vipMapper.updateById(vipRequestParam);
        if (count1 < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        //新增积分扣减记录
        int count2 = pointsChangeRecordService.addReducePointRecord(vipRequestParam);
        if (count2 < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return count1 + count2;
    }

    @Override
    public Integer resetPoints(VipRequestParam vipRequestParam) {
        Double changePoints = vipRequestParam.getChangePoints();
        Double vipPoints = vipRequestParam.getVipPoints();
        vipPoints = vipPoints - changePoints;
        vipRequestParam.setVipPoints(vipPoints);
        int count1 = vipMapper.updateById(vipRequestParam);
        if (count1 < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        //新增积分清零记录
        int count2 = pointsChangeRecordService.resetReducePointRecord(vipRequestParam);
        if (count2 < 1) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return count1 + count2;
    }
}
