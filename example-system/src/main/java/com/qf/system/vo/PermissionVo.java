package com.qf.system.vo;

import com.qf.system.entity.Permission;
import lombok.Data;

@Data
public class PermissionVo extends Permission {
}
