package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.PatientOccupation;

public interface PatientOccupationMapper extends BaseMapper<PatientOccupation> {
}