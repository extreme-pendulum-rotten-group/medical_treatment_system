package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

/**
 * 患者来源表
 */
@Data
@TableName(value = "hhf_patient_source")
public class PatientSource {
    /**
     * 主键
     */
    @TableId(value = "patient_source_id", type = IdType.INPUT)
    private Integer patientSourceId;

    /**
     * 患者来源名称
     */
    @TableField(value = "patient_source_name")
    private String patientSourceName;

    /**
     * 创建人
     */
    @TableField(value = "creator")
    private String creator;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 是否删除：1已删除，0未删除
     */
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    public static final String COL_PATIENT_SOURCE_ID = "patient_source_id";

    public static final String COL_PATIENT_SOURCE_NAME = "patient_source_name";

    public static final String COL_CREATOR = "creator";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_IS_DELETED = "is_deleted";
}