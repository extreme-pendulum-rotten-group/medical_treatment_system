package com.qf.system.reqeust;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 78458
 */
@Data
public class DossierQo {

    private String appeal;
    private String presentHistory;
    private String previousHistory;
    private String allergy;
    private String person;
    private String family;
    private String accessoryCheck;
    private String treatmentOptions;
    private String remarks;
    private String cardNumber;

}