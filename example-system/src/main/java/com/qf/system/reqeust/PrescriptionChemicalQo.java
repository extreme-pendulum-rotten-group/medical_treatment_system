package com.qf.system.reqeust;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qf.system.entity.Chemical;
import lombok.Data;

import java.util.List;

/**
 * @author 78458
 */
@Data
public class PrescriptionChemicalQo {


    private Long id;

    /**
     * 处方id
     */
    private Long prescriptionId;

    private List<Long> chemicalIdList;

    /**
     * 单价
     */
    private String price;

    /**
     * 最后价格
     */
    @TableField(value = "real_price")
    private String realPrice;

    /**
     * 此商品数量
     */
    @TableField(value = "num")
    private Integer num;

}