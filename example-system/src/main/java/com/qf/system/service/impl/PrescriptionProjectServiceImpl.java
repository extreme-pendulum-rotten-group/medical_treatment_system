package com.qf.system.service.impl;

import com.qf.system.entity.PrescriptionProject;
import com.qf.system.mapper.PrescriptionProjectMapper;
import com.qf.system.reqeust.PrescriptionProjectQo;
import com.qf.system.service.PrescriptionProjectService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author ZJW
 * @version 1.0.0
 * @Description TODO
 * @ClassName PrescriptionProjectServiceImpl.java
 * @createTime 2022年03月31日 15:05:00
 */
@Service
public class PrescriptionProjectServiceImpl implements PrescriptionProjectService {
    @Resource
    PrescriptionProjectMapper prescriptionProjectMapper;

    @Override
    public Integer insertPrescriptionAndProject(PrescriptionProjectQo ppq) {
        Integer count = 0;
        PrescriptionProject pp = new PrescriptionProject();
        pp.setPrescriptionId(ppq.getPrescriptionId());
        for (int i = 0; i <ppq.getProjectIdList().size(); i++) {
            pp.setProjectId(ppq.getProjectIdList().get(i));
            int j = prescriptionProjectMapper.insertPrescriptionAndProject(pp);
            if (j > 0) {
                count++;
            }
        }
        return count;
    }
}
