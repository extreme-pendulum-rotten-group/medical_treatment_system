package com.qf.system.vo;

import lombok.Data;

@Data
public class NumberVo {

    private String number;

    private Integer count;
}
