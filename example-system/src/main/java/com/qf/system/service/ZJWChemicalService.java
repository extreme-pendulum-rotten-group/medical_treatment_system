package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.ChemicalOnReceptionQo;
import org.springframework.stereotype.Service;

/**
 * @author 78458
 */
@Service
public interface ZJWChemicalService {
    ResponseResult selectAllChemicalAndStock();

    ResponseResult selectChemicalBySomething(ChemicalOnReceptionQo chemicalQo);
}
