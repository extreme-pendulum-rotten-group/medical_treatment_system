package com.qf.system.service.impl;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.Dossier;
import com.qf.system.mapper.DictionaryMapper;
import com.qf.system.reqeust.DossierQo;
import com.qf.system.service.DictionaryService;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;

import java.util.List;

@Service
public class DictionaryServiceImpl implements DictionaryService {
    @Resource
    DictionaryMapper dictionaryMapper;

    //主诉信息
    /**
     * 查询和展示
     *
     * @param
     * @return
     */
    @Override
    public ResponseResult selectMainSuit(DossierQo dossierQo) {
        List<Dossier> dossier = dictionaryMapper.selectMainSuit(dossierQo);
        if (dossier.size() == 0) {
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS, dossier);
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public ResponseResult deleteMainSuit(Long id) {
        Integer i = dictionaryMapper.deleteMainSuit(id);
        if (i == null) {
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 添加
     * @param dossier
     * @return
     */
    @Override
    public ResponseResult addMainSuit(Dossier dossier) {
        Integer i = dictionaryMapper.addMainSuit(dossier);
        if (i == null) {
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 模糊查询
     * @param dossier
     * @return
     */
    @Override
    public ResponseResult selectappeal(Dossier dossier) {
        List<Dossier> list = dictionaryMapper.selectappeal(dossier);
        if(list.size() == 0){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    //现病史
    /**
     * 展示
     * @param dossierQo
     * @return
     */
    @Override
    public ResponseResult selectPresents(DossierQo dossierQo) {
        List<Dossier> dossier = dictionaryMapper.selectMainSuit(dossierQo);
        if (dossier.size() == 0) {
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS, dossier);
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public ResponseResult deletePresent(Long id) {
        return null;
    }

    /**
     * 添加
     * @param dossier
     * @return
     */
    @Override
    public ResponseResult addPresent(Dossier dossier) {
        return null;
    }

    /**
     * 模糊查询
     * @param dossier
     * @return
     */
    @Override
    public ResponseResult selectPresent(Dossier dossier) {
        return null;
    }
}
