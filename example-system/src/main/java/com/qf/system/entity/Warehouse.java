package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "lhr_warehouse_out")
public class Warehouse {
    /**
     * 出库类型ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 类型
     */
    @TableField(value = "type")
    private String type;

    /**
     * 出库类型
     */
    @TableField(value = "warehouse_name")
    private String warehouseName;

    /**
     * 创建时间
     */
    @TableField(value = "creation_time")
    private Date creationTime;

    /**
     * 创建人ID
     */
    @TableField(value = "`member`")
    private Long member;

    /**
     * 删除
     */
    @TableField(value = "is_del")
    private Integer isDel;

    public static final String COL_ID = "id";

    public static final String TYPE = "type";

    public static final String COL_WAREHOUSE_OUT_NAME = "warehouse_out_name";

    public static final String COL_CREATION_TIME = "creation_time";

    public static final String COL_MEMBER = "member";

    public static final String COL_IS_DEL = "is_del";
}