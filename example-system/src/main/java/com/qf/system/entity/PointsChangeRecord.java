package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

/**
 * 积分变动记录表
 */
@Data
@TableName(value = "hhf_points_change_record")
public class PointsChangeRecord {
    /**
     * 主键
     */
    @TableId(value = "points_change_record_id", type = IdType.INPUT)
    private Integer pointsChangeRecordId;

    /**
     * 变动时间
     */
    @TableField(value = "change_time")
    private Date changeTime;

    /**
     * 变动类型
     */
    @TableField(value = "change_type")
    private String changeType;

    /**
     * 数量
     */
    @TableField(value = "amount")
    private Double amount;

    /**
     * 备注
     */
    @TableField(value = "remarks")
    private String remarks;

    /**
     * 是否删除：1已删除，0未删除
     */
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    /**
     * 会员id
     */
    @TableField(value = "patient_id")
    private Long patientId;

    public static final String COL_POINTS_CHANGE_RECORD_ID = "points_change_record_id";

    public static final String COL_CHANGE_TIME = "change_time";

    public static final String COL_CHANGE_TYPE = "change_type";

    public static final String COL_AMOUNT = "amount";

    public static final String COL_REMARKS = "remarks";

    public static final String COL_IS_DELETED = "is_deleted";

    public static final String COL_PATIENT_ID = "patient_id";
}