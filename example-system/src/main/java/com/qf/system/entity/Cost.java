package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "lhr_cost")
public class Cost {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 费用类型 - 1：挂号费；2：诊疗费
     */
    @TableField(value = "cost_type")
    private Integer costType;

    /**
     * 费用名称
     */
    @TableField(value = "cost_name")
    private String costName;

    /**
     * 金额
     */
    @TableField(value = "money")
    private Double money;

    /**
     * 成本价
     */
    @TableField(value = "cost_price")
    private Double costPrice;

    /**
     * 创建时间
     */
    @TableField(value = "establish")
    private String establish;

    /**
     * 创建人员
     */
    @TableField(value = "`member`")
    private Long member;

    /**
     * 折扣 - 1：启用；2：禁用
     */
    @TableField(value = "discount")
    private Integer discount;

    /**
     * 状态 - 1：启用；2：禁用
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 删除 - 0：已删；1：未删
     */
    @TableField(value = "is_del")
    private Integer isDel;

    public static final String COL_ID = "id";

    public static final String COL_COST_TYPE = "cost_type";

    public static final String COL_COST_NAME = "cost_name";

    public static final String COL_MONEY = "money";

    public static final String COL_COST_PRICE = "cost_price";

    public static final String COL_ESTABLISH = "establish";

    public static final String COL_MEMBER = "member";

    public static final String COL_DISCOUNT = "discount";

    public static final String COL_STATUS = "status";

    public static final String COL_IS_DEL = "is_del";
}