package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.entity.Dossier;
import com.qf.system.reqeust.DossierQo;
import com.qf.system.service.DictionaryService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

public class DictionaryController {
    @Resource
    private DictionaryService dictionaryService;

    /**
     * 主诉信息
     * @param dossierQo
     * @return
     */
    @GetMapping("/selectmain")
    public ResponseResult selectMainSuit (DossierQo dossierQo){
        return  dictionaryService.selectMainSuit(dossierQo);
        }

    @PostMapping("/deleteMainSuit")
    public ResponseResult deleteMainSuit(Long id){
        return dictionaryService.deleteMainSuit(id);
    }

    @PostMapping("/addMainSuit")
    public ResponseResult addFacility(Dossier dossier){
        return dictionaryService.addMainSuit(dossier);
    }
    @GetMapping("/SelectMainSuit")
    public ResponseResult selectappeal(Dossier dossier ){
        return  dictionaryService.selectappeal(dossier);
    }

    /**
     * 现病史
     * @param dossierQo
     * @return
     */
    @GetMapping("/selectPresents")
    public ResponseResult selectPresents (DossierQo dossierQo){
        return  dictionaryService.selectPresents(dossierQo);
    }

    @PostMapping("/deletePresent")
    public ResponseResult deletePresent(Long id){
        return dictionaryService.deletePresent(id);
    }

    @PostMapping("/addPresent")
    public ResponseResult addPresent(Dossier dossier){
        return dictionaryService.addPresent(dossier);
    }
    @GetMapping("/SelectPresent")
    public ResponseResult selectPresent(Dossier dossier ){
        return  dictionaryService.selectPresent(dossier);
    }

}
