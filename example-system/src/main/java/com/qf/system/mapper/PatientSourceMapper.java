package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.PatientSource;

public interface PatientSourceMapper extends BaseMapper<PatientSource> {
}