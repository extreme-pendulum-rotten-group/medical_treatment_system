package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.SupplierParam;

import java.util.List;

public interface SupplierService {
    ResponseResult selectSupplier(SupplierParam param);

    ResponseResult deleteByList(List<Integer> list);

    ResponseResult changeById(SupplierParam param);

    ResponseResult addSupplier(SupplierParam param);
}
