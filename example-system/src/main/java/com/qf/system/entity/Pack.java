package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "lhr_pack")
public class Pack {
    /**
     * 包装ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;

    /**
     * 单位名称
     */
    @TableField(value = "units_name")
    private String unitsName;

    /**
     * 创建时间
     */
    @TableField(value = "creation_time")
    private Date creationTime;

    /**
     * 创建人ID
     */
    @TableField(value = "`member`")
    private Long member;

    /**
     * 删除
     */
    @TableField(value = "is_del")
    private Integer isDel;

    public static final String COL_ID = "id";

    public static final String COL_UNITS_NAME = "units_name";

    public static final String COL_CREATION_TIME = "creation_time";

    public static final String COL_MEMBER = "member";

    public static final String COL_IS_DEL = "is_del";
}