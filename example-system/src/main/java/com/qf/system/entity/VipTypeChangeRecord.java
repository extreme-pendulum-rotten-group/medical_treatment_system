package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

/**
 * 会员等级变更记录表
 */
@Data
@TableName(value = "hhf_vip_type_change_record")
public class VipTypeChangeRecord {
    /**
     * 主键
     */
    @TableId(value = "vip_type_change_record_id", type = IdType.INPUT)
    private Integer vipTypeChangeRecordId;

    /**
     * 变更时间
     */
    @TableField(value = "change_time")
    private Date changeTime;

    /**
     * 变更类型
     */
    @TableField(value = "change_type")
    private String changeType;

    /**
     * 会员名称
     */
    @TableField(value = "vip_type_name")
    private String vipTypeName;

    /**
     * 会员id
     */
    @TableField(value = "patient_id")
    private Long patientId;

    /**
     * 是否删除：1已删除，0未删除
     */
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    public static final String COL_VIP_TYPE_CHANGE_RECORD_ID = "vip_type_change_record_id";

    public static final String COL_CHANGE_TIME = "change_time";

    public static final String COL_CHANGE_TYPE = "change_type";

    public static final String COL_VIP_TYPE_NAME = "vip_type_name";

    public static final String COL_PATIENT_ID = "patient_id";

    public static final String COL_IS_DELETED = "is_deleted";
}