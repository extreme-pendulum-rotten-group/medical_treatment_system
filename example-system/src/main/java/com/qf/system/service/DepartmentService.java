package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.DeptParam;

import java.util.List;

public interface DepartmentService {
    ResponseResult selectDept(DeptParam param);

    ResponseResult deleteList(List<Integer> list);

    ResponseResult changeDept(DeptParam param);

    ResponseResult addDept(DeptParam param);
}
