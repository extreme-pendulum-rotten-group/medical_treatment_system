package com.qf.system.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.Chemical;
import com.qf.system.mapper.ChemicalMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.service.impl
 * @ClassName : ChemicalService.java
 * @createTime : 2022/3/28 19:28
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */
@Service
public class ChemicalService {

    @Resource
    ChemicalMapper chemicalMapper;

    /**
     * 新增药品 数据层方法
     * @param chemical
     * @return
     */
    public String addChemical(Chemical chemical) {
        if (chemicalMapper.addChemical(chemical) != 1) {
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return "新增药品成功";
    }

    /**
     * 入库添加药品view模板
     * 数据处理
     * @return
     */
    public List<Chemical> chemicalInStockView() {
        List<Chemical> result = chemicalMapper.chemicalInStockView();
        if (result == null) {
            //异常抛出错误
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        result.forEach(chemical -> {
            // 采购金额
            chemical.setPurchaseAmount(BigDecimal.valueOf(chemical.getSuperiorLimitOfInventory()).multiply(chemical.getPurchasePrice()));
            // 销售金额
            chemical.setSalesAmount(BigDecimal.valueOf(chemical.getSuperiorLimitOfInventory()).multiply(chemical.getRetailPrice()));
            //药品有效期
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(new Date());
            calendar.add(calendar.DATE,chemical.getValidity());
            chemical.setTermOfValidity(calendar.getTime());
        });
        return result;
    }


    public List<Chemical> inReviewEnableViews() {
        return chemicalMapper.inReviewEnableViews();
    }

    public List<Chemical> inReviewUnEnableViews() {
        return chemicalMapper.inReviewUnEnableViews();
    }

    public List<Chemical> outReviewEnableViews() {
        return chemicalMapper.outReviewEnableViews();
    }

    public List<Chemical> outReviewUnEnableViews() {
        return chemicalMapper.outReviewUnEnableViews();
    }

    public Chemical getChemicalById(Integer chemicalId) {
        return chemicalMapper.getChemicalById(chemicalId);
    }

    public Chemical chemicalPaging(Integer page, Integer size, String param1, Boolean param2, Date param3, String param4, String param5, String param6) {
        Chemical chemical=chemicalMapper.chemicalPaging((page-1)*size,size,param1,param2,param3,param4,param5,param6);
        if (chemical==null){
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return chemical;
    }

    public String deleteChemicalById(Integer id) {
        Integer result=chemicalMapper.deleteChemicalById(id);
        if (result!=1){
            throw new ServiceException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return "删除药品";
    }
}
