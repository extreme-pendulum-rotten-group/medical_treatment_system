package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.MemberParam;
import com.qf.system.reqeust.PositionParam;
import com.qf.system.service.PositionService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/position")
public class PositionController {

    @Resource
    PositionService positionService;

    /**
     * 查询与展示
     * @param param
     * @return
     */
    @GetMapping("/view/position")
    public ResponseResult viewPosition(PositionParam param){
        return positionService.selectPosition(param);
    }

    /**
     * 删除
     * @param array
     * @return
     */
    @PostMapping("/delete/list")
    public ResponseResult deleteList(@RequestParam(value="array")Integer[] array) {
        List<Integer> list = Arrays.asList(array);
        return positionService.deleteList(list);
    }

    /**
     * 编辑与修改
     * @param param
     * @return
     */
    @PostMapping("/change/position")
    public ResponseResult changePosition(PositionParam param){
        return positionService.changePosition(param);
    }

    /**
     * 新增
     * @param param
     * @return
     */
    @PostMapping("/add/position")
    public ResponseResult addPosition(PositionParam param){
        return positionService.addPosition(param);
    }
}
