package com.qf.system.service;

import com.qf.system.reqeust.PrescriptionProjectQo;
import org.springframework.stereotype.Service;

@Service
public interface PrescriptionProjectService {
    Integer insertPrescriptionAndProject(PrescriptionProjectQo ppq);
}
