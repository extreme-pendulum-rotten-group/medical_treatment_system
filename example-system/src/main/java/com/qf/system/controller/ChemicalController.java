package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.Chemical;
import com.qf.system.service.impl.ChemicalService;
import com.qf.system.vo.ChemicalPagingVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.controller
 * @ClassName : ChemicalController.java
 * @createTime : 2022/3/28 22:03
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */
@RestController
public class ChemicalController {
    @Resource
    ChemicalService chemicalService;

    /**
     * 新增药物接口
     *
     * @param chemical
     * @return
     */
    @PostMapping(value = "/chemical/add")
    ResponseResult addChemical(@RequestBody Chemical chemical) {
        String result = chemicalService.addChemical(chemical);
        return ResponseResult.success(ResultCode.SUCCESS, result);
    }

    /**
     * 编辑入/出库信息展示模板
     *
     * @return
     */
    @GetMapping("/chemical/in/get")
    ResponseResult chemicalInStockViews() {
        List<Chemical> chemicals = chemicalService.chemicalInStockView();
        return ResponseResult.success(ResultCode.SUCCESS, chemicals);
    }

    @DeleteMapping("/chemical/delete/{id}")
    ResponseResult deleteChemicalById(@PathVariable Integer id) {
        return ResponseResult.success(ResultCode.SUCCESS,chemicalService.deleteChemicalById(id));

    }

    /**
     * 入库审核 通过
     *
     * @return
     */
    @GetMapping("/in_enable")
    ResponseResult inReviewEnableViews() {
        return ResponseResult.success(ResultCode.SUCCESS, chemicalService.inReviewEnableViews());
    }

    /**
     * 入库审核 未通过
     *
     * @return
     */
    @GetMapping("/in_un_enable")
    ResponseResult reviewUnEnableViews() {
        return ResponseResult.success(ResultCode.SUCCESS, chemicalService.inReviewUnEnableViews());
    }

    /**
     * 出库审核 通过
     *
     * @return
     */
    @GetMapping("/out_enable")
    ResponseResult outReviewEnableViews() {
        return ResponseResult.success(ResultCode.SUCCESS, chemicalService.outReviewEnableViews());
    }

    /**
     * 出库审核 未通过
     *
     * @return
     */
    @GetMapping("/out_un_enable")
    ResponseResult outReviewUnEnableViews() {
        return ResponseResult.success(ResultCode.SUCCESS, chemicalService.outReviewUnEnableViews());
    }

    /**
     * 药品信息维护查询
     */
    @GetMapping("/chemical/paging")
    ResponseResult chemicalPaging(@RequestBody ChemicalPagingVo pagingVo) {
        Chemical chemical = chemicalService.chemicalPaging(pagingVo.getPage(), pagingVo.getSize(), pagingVo.getParam1(), pagingVo.getParam2(), pagingVo.getParam3(),
                pagingVo.getParam4(), pagingVo.getParam5(), pagingVo.getParam6());
        return ResponseResult.success(ResultCode.SUCCESS, chemical);
    }
}
