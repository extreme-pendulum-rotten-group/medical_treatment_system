package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.PatientEducationBackground;

public interface PatientEducationBackgroundMapper extends BaseMapper<PatientEducationBackground> {
}