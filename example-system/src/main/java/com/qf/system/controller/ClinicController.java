package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.ClinicParam;
import com.qf.system.service.ClinicService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/clinic")
public class ClinicController {

    @Resource
    ClinicService clinicService;

    /**
     * 查找科室
     * @param param
     * @return
     */
    @GetMapping("/view/clc")
    public ResponseResult selectClc(ClinicParam param){
        return clinicService.selectClc(param);
    }

    /**
     * 删除
     * @param array
     * @return
     */
    @PostMapping("/delete/list")
    public ResponseResult deleteList(@RequestParam(value="array")Integer[] array) {
        List<Integer> list = Arrays.asList(array);
        return clinicService.deleteList(list);
    }

    /**
     * 编辑科室
     * @param param
     * @return
     */
    @PostMapping("/change/clc")
    public ResponseResult changeClc(ClinicParam param){
        return clinicService.changeClc(param);
    }

    /**
     * 新增
     * @param param
     * @return
     */
    @PostMapping("/add/clc")
    public ResponseResult addClc(ClinicParam param){
        return clinicService.addClc(param);
    }
}
