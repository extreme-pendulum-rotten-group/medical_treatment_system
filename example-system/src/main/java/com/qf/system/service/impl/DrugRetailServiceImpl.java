package com.qf.system.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.DrugRetail;
import com.qf.system.mapper.DrugRetailMapper;
import com.qf.system.mapper.OrderMapper;
import com.qf.system.qo.DrugRetailChemicalQo;
import com.qf.system.qo.DrugRetailCostQo;
import com.qf.system.qo.DrugRetailQo;
import com.qf.system.qo.OrderSurchargeQo;
import com.qf.system.service.DrugRetailService;
import com.qf.system.utils.MyUtils;
import com.qf.system.vo.DrugRetailChemicalVo;
import com.qf.system.vo.DrugRetailPatientVo;
import com.qf.system.vo.DrugRetailSurchargeVo;
import com.qf.system.vo.NumberVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DrugRetailServiceImpl implements DrugRetailService {
    @Resource
    DrugRetailMapper drugRetailMapper;

    @Resource
    OrderMapper orderMapper;

    @Override
    public NumberVo insert(DrugRetailQo drugRetailQo) {
        NumberVo numberVo = new NumberVo();
        int count = 0;
        DrugRetail retail = new DrugRetail();
        retail.setPatientId(drugRetailQo.getPatientId());
        retail.setOperator(drugRetailQo.getMember());
        retail.setSurchargeId(drugRetailQo.getSurchargeId());
        String number = MyUtils.getNumber("LS");
        numberVo.setNumber(number);
        retail.setRetailNumber(number);
        for (int i = 0; i < drugRetailQo.getChemicalId().size(); i++) {
            retail.setChemicalId(drugRetailQo.getChemicalId().get(i));
            retail.setChemicalMoney(drugRetailQo.getChemicalMoney().get(i));
            drugRetailMapper.insertDrugRetail(retail);
            count++;
        }
        numberVo.setCount(count);
        return numberVo;
    }

    @Override
    public List<String> findChemicalName() {
        List<String> list = drugRetailMapper.selectChemicalName();
        if (list.size() <= 0) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return list;
    }

    @Override
    public DrugRetailPatientVo findPatient(String str) {
        if ("".equals(str)) {
            throw new ServiceException(ResultCode.PARAMS_IS_INVALID);
        }
        DrugRetailPatientVo vo = drugRetailMapper.selectPatient(str);
        if (vo == null) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return vo;
    }

    @Override
    public List<DrugRetailChemicalVo> findChemical(DrugRetailChemicalQo drugRetailChemicalQo) {
        List<DrugRetailChemicalVo> vo = drugRetailMapper.selectChemical(drugRetailChemicalQo);
        if (vo == null) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return vo;
    }

    @Override
    public List<DrugRetailSurchargeVo> findSurcharge() {
        List<DrugRetailSurchargeVo> vo = drugRetailMapper.selectSurcharge();
        if (vo == null) {
            throw new ServiceException(ResultCode.ERROR);
        }
        return vo;
    }

    @Override
    public Integer addOrder(DrugRetailCostQo drugRetailCostQo) {
        NumberVo insert = insert(drugRetailCostQo.getDrugRetailQo());
        String dd = MyUtils.getNumber("DD");
        drugRetailCostQo.setOrderNumber(dd);
        Integer order = 0;
        double money = 0;
        Integer count = 0;
        if (insert.getCount() > 0) {
            OrderSurchargeQo qo = new OrderSurchargeQo();
            qo.setOrderNumber(drugRetailCostQo.getOrderNumber());
            qo.setSurchargeId(drugRetailCostQo.getDrugRetailQo().getSurchargeId());
            Integer integer = drugRetailMapper.insertSurcharge(qo);
            if (integer > 0) {
                for (int i = 0; i < drugRetailCostQo.getDrugRetailQo().getChemicalMoney().size(); i++) {
                    money += drugRetailCostQo.getDrugRetailQo().getChemicalMoney().get(i);
                    count++;
                }
                if (count == drugRetailCostQo.getDrugRetailQo().getChemicalMoney().size()) {
                    drugRetailCostQo.setOrderMeney(money);
                    drugRetailCostQo.setRetailNumber(insert.getNumber());
                    order = orderMapper.insertOrder(drugRetailCostQo);
                    if (order <= 0) {
                        throw new ServiceException(ResultCode.ERROR);
                    }
                } else {
                    throw new ServiceException(ResultCode.PARAM_IS_DEFICIENCY);
                }
            }
        }
        return order;
    }
}
