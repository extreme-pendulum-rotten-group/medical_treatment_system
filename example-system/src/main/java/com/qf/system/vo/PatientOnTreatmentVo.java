package com.qf.system.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
    * 患者表
 * @author 78458
 */
@Data
public class PatientOnTreatmentVo {

    private Long patientId;
    private String patientName;
    private String cardNumber;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date birthday;
    private Integer gender;
    private String phone;
    private String idNumber;
    private String address;

    private Integer registrationStatus;

    private String diagnose;
    private String advice;






}