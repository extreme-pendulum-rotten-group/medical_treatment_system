package com.qf.system.qo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class OrderSurchargeQo {
    /**
     * 订单号
     */
    private String orderNumber;

    /**
     * 附加费用表id
     */
    private Integer surchargeId;
}
