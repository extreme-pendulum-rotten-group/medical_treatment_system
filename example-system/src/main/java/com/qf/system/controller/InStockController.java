package com.qf.system.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.entity.InStock;
import com.qf.system.service.impl.InStockServer;
import com.qf.system.vo.InPagingVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.controller
 * @ClassName : InStockController.java
 * @createTime : 2022/3/29 11:15
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */
@RestController
public class InStockController {

    @Resource
    InStockServer inStockServer;

    /**
     * 新增入库信息
     *
     * @param inStock
     * @return
     */
    @PostMapping("/stock/in/add")
    ResponseResult addInStock(@RequestBody InStock inStock) {
        return ResponseResult.success(ResultCode.SUCCESS, inStockServer.addInStock(inStock));
    }

    /**
     * 编辑入库信息
     *
     * @param inStock
     * @return
     */
    @PutMapping("/stock/in/update")
    ResponseResult updateInStock(@RequestBody InStock inStock) {
        return ResponseResult.success(ResultCode.SUCCESS, inStockServer.updateInStock(inStock));
    }

    /**
     * 入库审核是否通过
     *
     * @param inEnable
     * @return
     */
    @PutMapping("/stock/in/review/update/{inEnable}/{inNumber}")
    ResponseResult inReview(@PathVariable("inEnable") Integer inEnable, @PathVariable("inNumber") String inNumber) {
        return ResponseResult.success(ResultCode.SUCCESS, inStockServer.inReviewEnable(inEnable, inNumber));
    }

    /**
     * 入库管理分页
     * @param inPagingVo
     * @return
     */
    @GetMapping("/in/paging")
    ResponseResult inPaging(@RequestBody InPagingVo inPagingVo) {
        return ResponseResult.success(ResultCode.SUCCESS,inStockServer.inPaging(
                inPagingVo.getPage(),inPagingVo.getSize(),inPagingVo.getParam1(),inPagingVo.getParam2(),inPagingVo.getParam3(),inPagingVo.getParam4()
        ));
    }

    /**
     * 删除入库信息
     * @param id
     * @return
     */
    @DeleteMapping("/in/delete/{id}")
    ResponseResult deleteInById(@PathVariable Integer id){
        return ResponseResult.success(ResultCode.SUCCESS,inStockServer.deleteInById(id));
    }
}
