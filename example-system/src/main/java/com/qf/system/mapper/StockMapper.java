package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Stock;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * @Project      : medical_treatment_system
 * @Package      : com.qf.system.mapper
 * @ClassName    : NfStockMapper.java
 * @createTime   : 2022/3/31 16:58
 * @version      : 1.0
 * @author       : 菜可夫斯基
 * @Email        : 3293477562@qq.com
 * @公众号        : 菜可夫斯基
 * @CSDN         : weixin_44106059
 * @Description  : 
 */
@Mapper
public interface StockMapper extends BaseMapper<Stock> {
    List<Stock> stockPaging(@Param("info") Integer info,@Param("size") Integer size,@Param("type") String type,@Param("name") String name,@Param("number") String number,@Param("producer") String producer);
}