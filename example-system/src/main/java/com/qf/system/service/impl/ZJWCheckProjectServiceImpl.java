package com.qf.system.service.impl;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.mapper.ZJWCheckProjectMapper;
import com.qf.system.reqeust.CheckProjectOnPrescriptionQo;
import com.qf.system.service.ZJWCheckProjectService;
import com.qf.system.vo.CheckProjectOnPrescriptionVo;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ZJW
 * @version 1.0.0
 * @Description TODO
 * @ClassName CheckProjectServiceImpl.java
 * @createTime 2022年03月30日 20:21:00
 */
@Service
public class ZJWCheckProjectServiceImpl implements ZJWCheckProjectService {

    @Resource
    ZJWCheckProjectMapper zjwCheckProjectMapper;
    @Override
    public ResponseResult selectAllCheckProjectOnPrescription() {
        List<CheckProjectOnPrescriptionVo> list= zjwCheckProjectMapper.selectAllCheckProjectOnPrescription();
        if (ObjectUtils.isEmpty(list)){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(list);
    }

    @Override
    public ResponseResult selectCheckProjectOnPrescriptionBySomething(CheckProjectOnPrescriptionQo checkProjectOnPrescriptionQo) {
        List<CheckProjectOnPrescriptionVo> list= zjwCheckProjectMapper.selectCheckProjectOnPrescriptionBySomething(checkProjectOnPrescriptionQo);
        if (ObjectUtils.isEmpty(list)){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(list);
    }
}
