package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 角色表
 */
@Data
@TableName(value = "lhr_role")
public class Role {
    /**
     * 主键
     */
    @TableId(value = "role_id", type = IdType.INPUT)
    private Integer roleId;

    /**
     * 角色名
     */
    @TableField(value = "`name`")
    private String name;

    /**
     * 角色说明
     */
    @TableField(value = "decl")
    private String decl;

    /**
     * 是否启用 1 启用 0 禁用
     */
    @TableField(value = "`status`")
    private Integer status;

    public static final String COL_ROLE_ID = "role_id";

    public static final String COL_NAME = "name";

    public static final String COL_DECL = "decl";

    public static final String COL_STATUS = "status";
}