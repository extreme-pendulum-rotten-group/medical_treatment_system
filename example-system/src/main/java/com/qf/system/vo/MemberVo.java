package com.qf.system.vo;

import com.qf.system.entity.Member;
import lombok.Data;

import java.util.List;

@Data
public class MemberVo extends Member {
    private String departmentName;

    private String positionName;

    private String clinicName;

    private List<RoleVo> roles;
}
