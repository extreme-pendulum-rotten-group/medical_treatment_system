package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Order;
import com.qf.system.qo.DrugRetailCostQo;
import com.qf.system.qo.OrderHomeQo;
import com.qf.system.qo.OrderQo;
import com.qf.system.vo.OrderChemicalVo;
import com.qf.system.vo.OrderDetailVo;
import com.qf.system.vo.OrderVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper extends BaseMapper<Order> {

    Integer insertOrder(@Param("qo") DrugRetailCostQo drugRetailCostQo);

    /**
     * 查询订单首页
     * @param orderHomeQo
     * @return
     */
    List<OrderVo> selectOrder(@Param("qo") OrderHomeQo orderHomeQo);

    /**
     * 修改付款状态
     */
    Integer update(@Param("number") String orderNumber ,@Param("onset") Integer onset,@Param("end") Integer end);

    /**
     * 删除订单
     */
    Integer deleteOrder(@Param("number") String orderNumber);

    /**
     * 查询详细
     */
    OrderDetailVo selectOrderDetail(@Param("number") String orderNumber);

    /**
     * 查询药品
     */
    List<OrderChemicalVo> selectOrderChemicalVo(@Param("number") String orderNumber);
}