package com.qf.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@TableName(value = "lhr_member")
public class Member {
    /**
     * 用户id
     */
    //@NotNull
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    /**
     * 姓名
     */
    //@NotBlank
    @TableField(value = "username")
    private String username;

    /**
     * 年龄
     */
    @TableField(value = "age")
    private Integer age;

    /**
     * 性别 - 1：男；2：女
     */
    //@NotBlank
    @TableField(value = "sex")
    private String sex;

    /**
     * 手机号码
     */
    //@NotBlank
    //@Pattern(regexp = "^(1[3-9][0-9])\\d{8}", message = "手机号格式不对")
    @TableField(value = "phone")
    private String phone;

    /**
     * 电子邮箱
     */
    @TableField(value = "email")
    private String email;

    /**
     * 证件号
     */
    @TableField(value = "document_number")
    private Integer documentNumber;

    /**
     * 角色
     */
    //@NotNull
    @TableField(value = "`position`")
    private Integer position;

    /**
     * 详细地址
     */
    @TableField(value = "site_particular")
    private String siteParticular;

    /**
     * 所属科室
     */
    //@NotNull
    @TableField(value = "department")
    private Integer department;

    /**
     * 权限id
     */
    //@NotNull
    @TableField(value = "role_id")
    private Integer roleId;

    /**
     * 密码
     */
    //@NotBlank
    //@Length(min = 2, max = 16)
    @TableField(value = "`password`")
    private String password;

    /**
     * 创建时间
     */
    //@NotNull
    @TableField(value = "establish")
    private String establish;

    /**
     * 状态(1是启用，0是禁用)
     */
    //@NotNull
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 是否删除(1是正常，0是删除)
     */
    //@NotNull
    @TableField(value = "is_del")
    private Integer isDel;

    /**
     * 创建人员
     */
    //@NotNull
    @TableField(value = "create_personnel")
    private String createPersonnel;

    /**
     * 所属诊所
     */
    //@NotNull
    @TableField(value = "clinic")
    private Integer clinic;

    public static final String COL_ID = "id";

    public static final String COL_USERNAME = "username";

    public static final String COL_AGE = "age";

    public static final String COL_SEX = "sex";

    public static final String COL_PHONE = "phone";

    public static final String COL_EMAIL = "email";

    public static final String COL_DOCUMENT_NUMBER = "document_number";

    public static final String COL_POSITION = "position";

    public static final String COL_SITE_PARTICULAR = "site_particular";

    public static final String COL_DEPARTMENT = "department";

    public static final String COL_ROLE_ID = "role_id";

    public static final String COL_PASSWORD = "password";

    public static final String COL_ESTABLISH = "establish";

    public static final String COL_STATUS = "status";

    public static final String COL_IS_DEL = "is_del";

    public static final String COL_CREATE_PERSONNEL = "create_personnel";

    public static final String COL_CLINIC = "clinic";
}