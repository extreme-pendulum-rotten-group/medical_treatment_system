package com.qf.system.entity;

import java.util.Date;
import lombok.Data;

@Data
public class DrugRetail {
    /**
     * 主键id
     */
    private Integer id;

    /**
     * 零售单号
     */
    private String retailNumber;

    /**
     * 患者信息
     */
    private Integer patientId;

    /**
     * 药品
     */
    private Integer chemicalId;

    /**
     * 药品单价
     */
    private Double chemicalMoney;

    /**
     * 附加费id
     */
    private Integer surchargeId;

    /**
     * 操作人 连接员工表主键id
     */
    private Integer operator;

    /**
     * 是否删除 1-删除 0-不删除
     */
    private Integer isDetele;

    /**
     * 操作时间
     */
    private Date operationTime;
}

