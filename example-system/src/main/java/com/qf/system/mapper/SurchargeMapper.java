package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.Surcharge;
import com.qf.system.reqeust.SurchargeParam;import com.qf.system.vo.SurchargeVo;import org.apache.ibatis.annotations.Mapper;import java.util.List;

@Mapper
public interface SurchargeMapper extends BaseMapper<Surcharge> {
    Integer deleteByList(List<Integer> list);

    //Integer changeById(SurchargeParam param);

    Integer addCheck(SurchargeParam param);

    List<SurchargeVo> selectSurcharge(SurchargeParam param);
}