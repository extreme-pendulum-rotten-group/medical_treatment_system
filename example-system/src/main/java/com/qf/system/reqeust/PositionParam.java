package com.qf.system.reqeust;

import com.qf.system.entity.Position;
import lombok.Data;

@Data
public class PositionParam extends Position {
}
