package com.qf.system.service.impl;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.system.mapper.ZJWChemicalMapper;
import com.qf.system.reqeust.ChemicalOnReceptionQo;
import com.qf.system.service.ZJWChemicalService;
import com.qf.system.vo.ChemicalOnReceptionVo;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ZJW
 * @version 1.0.0
 * @Description TODO
 * @ClassName ChemicalServiceImpl.java
 * @createTime 2022年03月30日 19:07:00
 */
@Service
public class ZJWChemicalServiceImpl implements ZJWChemicalService {

    @Resource
    ZJWChemicalMapper zjwChemicalMapper;
    @Override
    public ResponseResult selectAllChemicalAndStock() {
      List<ChemicalOnReceptionVo> list= zjwChemicalMapper.selectAllChemicalAndStock();
        if (ObjectUtils.isEmpty(list)){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(list);
    }

    @Override
    public ResponseResult selectChemicalBySomething(ChemicalOnReceptionQo chemicalQo) {
        List<ChemicalOnReceptionVo> list= zjwChemicalMapper.selectChemicalBySomething(chemicalQo);
        if (ObjectUtils.isEmpty(list)){
            return ResponseResult.error(ResultCode.ERROR);
        }
        return ResponseResult.success(list);
    }
}
