package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.ClinicParam;

import java.util.List;

public interface ClinicService {
    ResponseResult selectClc(ClinicParam param);

    ResponseResult deleteList(List<Integer> list);

    ResponseResult changeClc(ClinicParam param);

    ResponseResult addClc(ClinicParam param);
}
