package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.PrescriptionSurcharge;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PrescriptionSurchargeMapper extends BaseMapper<PrescriptionSurcharge> {
    int insertPrescriptionAndSurcharge(PrescriptionSurcharge ps);
}