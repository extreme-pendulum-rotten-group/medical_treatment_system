package com.qf.system.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.system.reqeust.CheckProjectOnPrescriptionQo;
import org.springframework.stereotype.Service;

@Service
public interface ZJWCheckProjectService {
    ResponseResult selectAllCheckProjectOnPrescription();

    ResponseResult selectCheckProjectOnPrescriptionBySomething(CheckProjectOnPrescriptionQo checkProjectOnPrescriptionQo);
}
