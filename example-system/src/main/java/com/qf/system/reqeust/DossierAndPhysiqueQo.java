package com.qf.system.reqeust;

import com.baomidou.mybatisplus.annotation.TableName;
import com.qf.system.entity.Physique;
import lombok.Data;

@Data
@TableName(value = "dossier")
public class DossierAndPhysiqueQo {
    private Long id;
    private String appeal;
    private String presentHistory;
    private String previousHistory;
    private String allergy;
    private String person;
    private String family;
    private String accessoryCheck;
    private String treatmentOptions;
    private String remarks;
    private String cardNumber;




    private String temperature;
    private String breathe;
    private String sphygmus;
    private String bloodPressure;
    private String height;
    private String weight;
    private String bloodGlucose;
    private String bloodFat;
}