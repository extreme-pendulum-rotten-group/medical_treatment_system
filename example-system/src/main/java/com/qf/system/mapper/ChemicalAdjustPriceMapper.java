package com.qf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.system.entity.ChemicalAdjustPrice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : 菜可夫斯基
 * @version : 1.0
 * @Project : medical_treatment_system
 * @Package : com.qf.system.mapper
 * @ClassName : ChemicalAdjustPriceMapper.java
 * @createTime : 2022/3/30 18:19
 * @Email : 3293477562@qq.com
 * @公众号 : 菜可夫斯基
 * @CSDN : weixin_44106059
 * @Description :
 */
@Mapper
public interface ChemicalAdjustPriceMapper extends BaseMapper<ChemicalAdjustPrice> {
    Integer adjustPrice(@Param("currentPrice") BigDecimal currentPrice, @Param("remarks") String remarks, @Param("chemicalId") Integer chemicalId);

    List<ChemicalAdjustPrice> adjustPaging(@Param("info") Integer info, @Param("size") Integer size, @Param("type") Integer type,@Param("name") String param1,@Param("number") String param2, @Param("producer") String param3);
}